import { useState } from "react";

const useAuth= () => {
    const [user, setUser] = useState(window.sessionStorage.getItem('user'));

    //DAPAT DIGANTI sessionStorage/localStorage

    function authOperation(operationArg) { // { name: 'LOGIN/LOGOUT', user: ''}
        switch(operationArg.name) {
            case 'LOGIN':
                window.sessionStorage.setItem('user', JSON.stringify(operationArg.user));
                setUser(operationArg.user);
                break;
            case 'LOGOUT':
                window.sessionStorage.removeItem('user');
                setUser(null);
                break;
        }
    }

    return { user, authOperation };
};

export default useAuth;