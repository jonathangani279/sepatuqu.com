﻿import "./App.css";
import React, { useState } from "react";
import { Switch, Route, useHistory } from "react-router-dom";

import UserContext from "./components/UserContext";

import Header from "./components/Header";
import NavBar from "./components/NavBar";
import Login from './components/Login';
import Register from './components/Register';
import Home from "./components/Home";
import MyAccount from "./components/MyAccount";
import Catalog from "./components/Catalog";
import Detail from "./components/Catalog/Detail";
import Footer from "./components/Footer";

import Laporan from "./components/Laporan";
import Chart from "./components/Laporan/chart";
import Invoice from "./components/Invoice";

import HomeMasterBarang from "./components/HomeMasterBarang";
import ManageMasterBarang from "./components/ManageMasterBarang";
import InsertMasterBarang from "./components/InsertMasterBarang";

import Cart from "./components/Cart";
import DetailCart from "./components/Cart/DetailCart";
import Wishlist from "./components/Wishlist";
import DetailWishlist from "./components/Wishlist/DetailWishlist";
import ShippingInformation from "./components/ShippingInformation";
import Nota from "./components/Nota";

//TODO Web Template Studio: Add routes for your new pages here.
const App = () => {
	const [ keyword, setKeyword ]= useState();
	const [ startSearch, setStartSearch ]= useState(0);
	const history= useHistory();

	function handleKeyword(keyword) {
		setKeyword(keyword);
		setStartSearch(0);
	}

	function handleStartSearch(key) {
		if (key === 13) {
			setStartSearch(1);
			history.push('/catalog');
		}
	}

    return (
		<React.Fragment>
			<UserContext>
				<Header onKeyword={ handleKeyword } 
						onStartSearch= { handleStartSearch } />
				<NavBar />
			</UserContext>
			
			<Switch>
				{/* Routing ubah2 disini */}
				<Route exact path = "/" component = { Home } />
				<Route exact path = "/home" component = { Home } />
				<Route exact path = "/login" component = { Login } />
				<Route exact path = "/register" component = { Register } />	
				
				<Route exact path = "/laporan" component = { Laporan } />
				<Route exact path = "/chart" component = { Chart } />
				<Route exact path = "/invoice" component = { Invoice } />
				
				<Route exact path = "/HomeMasterBarang/:cari/:carikategori" component = { HomeMasterBarang } />
				<Route exact path = "/ManageMasterBarang/:cari/:carikategori" component = { ManageMasterBarang } />
				<Route exact path = "/InsertMasterBarang" component = { InsertMasterBarang } />
				
				{ /* GLOBAL USER CONTEXT */ }
				<UserContext>
					<Route exact path = "/catalog" render = { () => <Catalog keyword={ keyword } 
																			 startSearch= { startSearch } /> } />
					<Route exact path = "/catalog/:kategori" component = { Catalog } />
					<Route exact path = "/catalog/:kategori/:id_sneaker" component = { Detail } />
					<Route exact path = "/myAccount" component = { MyAccount } />
					<Route exact path = "/cart" component = { Cart } />
					<Route exact path = "/cart/:id_cart" component = { DetailCart } />
					<Route exact path = "/wishlist" component = { Wishlist } />
					<Route exact path = "/wishlist/:id_wishlist" component = { DetailWishlist } />
					<Route exact path = "/shipping" component = { ShippingInformation } />	
					<Route exact path = "/nota" component = { Nota } />						
				</UserContext>
			</Switch>
			<Footer />
		</React.Fragment>
    );
}

export default App;
