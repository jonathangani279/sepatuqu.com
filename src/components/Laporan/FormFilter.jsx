import React, { useState } from 'react';
import useFormInput from "./../../libraries/useFormInput"

const FormFilter = ({ onAddItem }) => {
    const tglFrom = useFormInput("");
    const tglUntil = useFormInput("");
    //const sortPrice = useFormInput("asc");

    const [sortPrice, setSortPrice] = useState("asc");

    const handleSubmit = (e) => {
        e.preventDefault();
        onAddItem({ tglFrom : tglFrom.value, tglUntil : tglUntil.value, sortPrice : sortPrice });
    }

    return ( 
        <form onSubmit={handleSubmit}>
            From : <input type="date" { ...tglFrom } /><br/><br/>
            Until : <input type="date" { ...tglUntil } /><br/><br/>

            Price : &nbsp;&nbsp;
            <input type="radio" id="asc" name="urutanPrice" value="asc" checked={ sortPrice==="asc" } onChange={ () => setSortPrice("asc") }/>
            &nbsp;Ascending
            &nbsp;&nbsp;&nbsp;
            <input type="radio" id="desc" name="urutanPrice" value="desc" checked={ sortPrice==="desc" } onChange={ () => setSortPrice("desc") }/>
            &nbsp;Descending<br/><br/>


            <button type="submit" style={{fontSize:"0.7vw", textAlign:"center"}} className="btn btn-primary">&nbsp; Submit &nbsp;</button>
            <br/><br/><br/>
        </form>
    );
}
 
export default FormFilter;