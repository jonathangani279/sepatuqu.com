import React, { useState, useEffect } from "react";
import { MDBDataTableV5 } from 'mdbreact';
import Modal from 'react-modal';

const Posts = ({dataSetTable, dataLaporanHeader, dataLaporanDetail, dataTable, idTransPopup, isModalInvoice, isModalDetail, onCloseDetail, onCloseInvoice }) => {
  // const [isModalDetail, setIsModalDetail] = useState(isModalDetailParent);
  // const [isModalInvoice, setIsModalInvoice] = useState(isModalInvoiceParent);

  //const [idTransPopup,setidTransPopup] = useState(0);
  
  //console.log(dataTable);
  //const [ dataTable, setDataTable ]= useState();
    
    //console.log(dataLaporanHeader);
    // function getTransactions() {
    //   try {
    //       console.log("sesuatu");
    //       dataLaporanHeader = dataLaporanHeader.map((item, index) => ({
    //           row: index+1,
    //           id_trans: `#${item.id_trans}`,
    //           tgl_beli: item.tanggal_beli,
    //           jumlah: item.jumlah_beli,
    //           total: "Rp." + item.total_harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),

    //           pembeli: item.nama_pembeli,
    //           penerima: item.nama_penerima,
    //           alamat: item.alamat_pengiriman,
    //           kodepos: item.kode_pos,
    //           notelp: item.no_telp,
    //           kota: item.nama_kota,
    //           action: <button onClick={e => { handleShowPopupDetail(item.id_trans) }} 
    //                   style={{backgroundColor:"rgb(180,180,180)", border:"0px solid black", borderRadius:"15px", color:"white", height:"25px"}}>
    //                   &nbsp; Detail &nbsp;
    //                   </button>,
    //           invoice: <button onClick={e => { handleShowPopupInvoice(item.id_trans) }} 
    //                   style={{backgroundColor:"rgb(180,180,180)", border:"0px solid black", borderRadius:"15px", color:"white", height:"25px"}}>
    //                   &nbsp; Invoice &nbsp;
    //                   </button>
    //       })); 
          

    //       setDataTable({
    //           columns:  [
    //               { label: '#', field: 'row' },
    //               { label: 'NO. TRANS', field: 'id_trans' },
    //               { label: 'DATE', field: 'tgl_beli' },
    //               { label: 'JUMLAH', field: 'jumlah' },
    //               { label: 'TOTAL', field: 'total' },
    //               { label: 'PEMBELI', field: 'pembeli' },
    //               { label: 'PENERIMA', field: 'penerima' },
    //               { label: 'ALAMAT', field: 'alamat' },
    //               { label: 'KODEPOS', field: 'kodepos' },
    //               { label: 'NOTELP', field: 'notelp' },
    //               { label: 'KOTA', field: 'kota' },
    //               { label: 'ACTION', field: 'action' },
    //               { label: 'INVOICE', field: 'invoice' }
    //           ],
    //           rows: dataLaporanHeader
    //       });
    //   } catch(error) {
    //       console.error(error);
    //   }
    // }
    
    

    function handleClosePopupDetail() {
      //console.log(id_trans + " !!!!");
      // setIsModalDetail(true);
      // setidTransPopup(id_trans);
      onCloseDetail();
    }
    function handleClosePopupInvoice() {
      //console.log(id_trans + " !!!!");
      // setIsModalInvoice(true);
      // setidTransPopup(id_trans);
      onCloseInvoice();
    }

    useEffect(() => {
        //getTransactions();
    });

    return (
        <>
          { dataTable && dataTable.rows.length ?     
                <MDBDataTableV5 responsive
                                striped
                                searchBottom={ false }
                                data={ dataTable } />
            : 'No orders history.' }

            {/* MENU POPUP DETAIL*/}
            <Modal isOpen={isModalDetail} onRequestClose={() => handleClosePopupDetail() } style={{overlay:{backgroundColor:'gray'},content:{color:'black'}}}>
              <h1 style={{textAlign:"center",fontFamily: "'Comfortaa', cursive"}}>Laporan Detail</h1><br/>
              {
                dataLaporanHeader.map(item => {
                  if(item.id_trans == idTransPopup)
                  {
                    let date = new Date(item.tanggal_beli);
                    date.setDate(date.getDate()-1);
                    console.log(date);
                    return (
                      <div> 
                        ID Transaksi        <label style={{textIndent:"41.5px"}}>: &nbsp; {item.id_trans}</label> <br/>
                        Tanggal Transaksi   <label style={{textIndent:"0px"}}>: &nbsp; {date.toDateString()}</label> <br/>
                        Jumlah Transaksi    <label style={{textIndent:"4.5px"}}>: &nbsp; {item.jumlah_beli}</label> <br/>
                        Total Transaksi     <label style={{textIndent:"22px"}}>: &nbsp; Rp.{item.total_harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</label> <br/><br/>
    
                        Nama Pembeli        <label style={{textIndent:"27.5px"}}>: &nbsp; {item.nama_pembeli}</label> <br/>
                        Nama Penerima       <label style={{textIndent:"17px"}}>: &nbsp; {item.nama_penerima}</label> <br/> 
                        Alamat              <label style={{textIndent:"77.5px"}}>: &nbsp; {item.alamat_pengiriman}</label> <br/>
                        Kode Pos            <label style={{textIndent:"63px"}}>: &nbsp; {item.kode_pos}</label> <br/> 
                        No Telp             <label style={{textIndent:"73.5px"}}>: &nbsp; {item.no_telp}</label> <br/> 
                        Kota                <label style={{textIndent:"95px"}}>: &nbsp; {item.nama_kota}</label> <br/>
                        {
                            dataLaporanDetail.map(itemDetail => {
                              if(itemDetail.id_trans == idTransPopup)
                                return (
                                  <div> <hr/>
                                    Nama Item     <label style={{textIndent:"50px"}}>: {itemDetail.nama_item} ( {itemDetail.brand_item} - {itemDetail.kategori_item} )</label><br/> 
                                    Ukuran Item   <label style={{textIndent:"40.5px"}}>: {itemDetail.ukuran_item}</label> <br/> 
                                    Jumlah Item   <label style={{textIndent:"40px"}}>: {itemDetail.jumlah_item}</label> <br/> 
                                    Subtotal      <label style={{textIndent:"66px"}}>: Rp.{itemDetail.subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},- (Rp.{itemDetail.harga_item.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-/pcs)</label><br/>
                                  </div>
                                );
                            })
                        }
                      </div>
                  )};
                })
              }
              
            </Modal>


            {/* MENU POPUP INVOICE*/}
            <Modal isOpen={isModalInvoice} onRequestClose={() => handleClosePopupInvoice() }  style={{border:"2px solid black", overlay:{backgroundColor:'gray'},content:{color:'black'}}}>
            <div>
              <img src={ process.env.PUBLIC_URL+'/icons/SepatuQu2.png' } style={{position:"absolute", top:"2%", left:"88%", width:"10%"}}></img>
              <h1>PURCHASE RECEIPT</h1><br/>
              <p>
                  <i>SNEAKY</i> - Sneaker Online Store <br/>
                  Jl. Ngagel Jaya Tengah No.73-77      <br/>
                  Surabaya, Jawa Timur 60284
              </p>
              {
                dataLaporanHeader.map((item) => {
                  if(item.id_trans == idTransPopup)
                  {
                    let date = new Date(item.tanggal_beli);
                    date.setDate(date.getDate()-1);
                    return (
                      <div key={item.id_trans}>
                        
                        {/* HEADER INVOICE */}
                        <div style={{float:"left",width:"25%"}}>
                            BILL TO                           <br/>
                            {item.nama_pembeli}               <br/>
                            {item.email_pembeli}              <br/>
                        </div>
                        <div style={{float:"left",width:"25%"}}>
                            SHIP TO                           <br/>
                            {item.nama_penerima}              <br/>
                            {item.alamat_pengiriman}          <br/>
                            {item.nama_provinsi},{item.nama_kota} {item.kode_pos}
                        </div>
                        <div style={{float:"left",width:"25%"}}>
                            RECEIPT NUMBER #{item.id_trans}   <br/>
                            RECEIPT DATE                      <br/>
                            {date.toDateString()}               <br/>
                        </div>
                        <div style={{float:"left",width:"25%"}}>
                            SHIPPING TIME ESTIMATED           <br/>
                            {item.estimasi_shipping}
                        </div><p style={{clear:"both"}}/><br/><br/> 



                        {/* HEADER TABEL */}
                        <h3>ORDER DETAILS</h3><br/>

                        <hr/>
                        <div style={{float:"left",width:"24%"}}> SNEAKERS </div>
                        <div style={{float:"left",width:"13%"}}> BRAND </div>
                        <div style={{float:"left",width:"10%", textAlign:"right"}}> SIZE </div>
                        <div style={{float:"left",width:"13%", textAlign:"right"}}> QUANTITY </div>
                        <div style={{float:"left",width:"20%", textAlign:"right"}}> PRICE </div>
                        <div style={{float:"left",width:"20%", textAlign:"right"}}> SUBTOTAL </div>
                        <p style={{clear:"both"}}/>
                        <hr/>



                        {/* DETAIL PEMBELIAN */}
                        <div style={{float:"left",width:"24%"}}> 
                          {
                              dataLaporanDetail.map(itemDetail => {
                                if(itemDetail.id_trans == item.id_trans)
                                  return (
                                      <div style={{minHeight:"50px"}}>{itemDetail.nama_item}</div>
                                  );
                              })
                            }
                            <div style={{minHeight:"50px"}}/>
                        </div>
                        <div style={{float:"left",width:"13%"}}> 
                          {
                              dataLaporanDetail.map(itemDetail => {
                                if(itemDetail.id_trans == item.id_trans)
                                  return (
                                      <div style={{minHeight:"50px"}}>{itemDetail.brand_item}</div>
                                  );
                              })
                            }
                            <div style={{minHeight:"50px"}}/>
                        </div>
                        <div style={{float:"left",width:"10%", textAlign:"right"}}> 
                          {
                              dataLaporanDetail.map(itemDetail => {
                                if(itemDetail.id_trans == item.id_trans)
                                  return (
                                      <div style={{minHeight:"50px"}}>{itemDetail.ukuran_item}</div>
                                  );
                              })
                            }
                            <div style={{minHeight:"50px"}}/>
                        </div>
                        <div style={{float:"left",width:"13%", textAlign:"right"}}> 
                          {
                              dataLaporanDetail.map(itemDetail => {
                                if(itemDetail.id_trans == item.id_trans)
                                  return (
                                      <div style={{minHeight:"50px"}}>{itemDetail.jumlah_item}</div>
                                  );
                              })
                            }
                            <div style={{minHeight:"50px"}}/>
                        </div>
                        <div style={{float:"left",width:"20%", textAlign:"right"}}> 
                          {
                              dataLaporanDetail.map(itemDetail => {
                                if(itemDetail.id_trans == item.id_trans)
                                  return (
                                      <div style={{minHeight:"50px"}}>Rp.{itemDetail.harga_item.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</div>
                                  );
                              })
                            }
                            <div style={{minHeight:"50px"}}> SHIPPING COST (JNE - REGULAR) </div>
                        </div>
                        <div style={{float:"left",width:"20%", textAlign:"right"}}> 
                          {
                              dataLaporanDetail.map(itemDetail => {
                                if(itemDetail.id_trans == item.id_trans)
                                  return (
                                      <div style={{minHeight:"50px"}}>Rp.{itemDetail.subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</div>
                                  );
                              })
                            }
                            <div style={{minHeight:"50px"}}>Rp.{item.harga_shipping.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</div>
                        </div>
                        <p style={{clear:"both"}}/><hr/>


                        {/* GRAND TOTAL */}
                        <div style={{float:"left",width:"60%", textAlign:"right", minHeight:"30px"}}/>
                        <div style={{float:"left",width:"20%", textAlign:"right", minHeight:"30px"}}>GRAND TOTAL</div>
                        <div style={{float:"left",width:"20%", textAlign:"right", minHeight:"30px"}}>Rp.{(parseInt(item.total_harga) + parseInt(item.harga_shipping)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</div>
                        <p style={{clear:"both"}}/><hr/><br/><br/>

                        <br/><br/><br/><br/>
                      </div>
                    )}
                })
              }
              <center>
                  <h5>--THANK YOU FOR PURCHASING OUR ITEMS--</h5>
                  <img src={ process.env.PUBLIC_URL+'/icons/SepatuQu2.png' } style={{width:"10%", marginLeft:"auto", marginRight:"auto"}}></img><br/>
                  <i>SNEAKY</i> - Sneaker Online Store <br/>
                  @2019 <i>SNEAKY</i> <br/>
                  All Rights Reserved.
              </center><br/>
              </div>
              
            </Modal>
                
        </>
    )
};

export default Posts