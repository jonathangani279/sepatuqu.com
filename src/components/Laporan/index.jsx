import React, { useState, useEffect } from "react";
import FormFilter from './FormFilter'
//import axios from 'axios';


//resource image
import Background from './img/background.png'
import Posts from "./Posts.jsx";

const Laporan = () => {
  const [ dataLaporanHeader, setDataLaporanHeader ] = useState([]);
  const [ dataLaporanDetail, setDataLaporanDetail ] = useState([]);

  //showPopup Children
  const [isModalDetail, setIsModalDetail] = useState(false);
  const [isModalInvoice, setIsModalInvoice] = useState(false);
  const [idTransPopup,setidTransPopup] = useState(0);
  const [ dataTable, setDataTable ]= useState();
  const [ dataSetTable, setDataSetTable] = useState();

  
  async function getDataLaporanFromAPI() {
    try {
      const data = await fetch("/apiLaporan/getLaporan", { method: "GET" })
      .then(async response => {
          //alert("success");
          let a = await response.json();
          //console.log(a[0]);
          let tempTable = a[0].header.map((item, index) => {
            let date = new Date(item.tanggal_beli);
            date.setDate(date.getDate()-1);
            // var tglreal = tomorrow(item.tanggal_beli);
            // console.log(tglreal);
            return ({
                row: index+1,
                id_trans: `#${item.id_trans}`,
                tgl_beli: date.toDateString(),
                jumlah: item.jumlah_beli,
                total: "Rp." + item.total_harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
                pembeli: item.nama_pembeli,
                penerima: item.nama_penerima,
                alamat: item.alamat_pengiriman,
                kodepos: item.kode_pos,
                notelp: item.no_telp,
                kota: item.nama_kota,

                action: <button onClick={() => handleShowPopupDetail(item.id_trans) } 
                        style={{fontSize:"0.7vw", textAlign:"center"}}
                        className="btn btn-primary">
                        &nbsp; Detail &nbsp;
                        </button>,
                invoice: <button onClick={() => handleShowPopupInvoice(item.id_trans) } 
                        style={{fontSize:"0.68vw", textAlign:"center"}}
                        className="btn btn-primary">
                        &nbsp; Invoice &nbsp;
                        </button>
              }
            )}
          );

          setDataSetTable(tempTable);
          setDataLaporanHeader(a[0].header);
          setDataLaporanDetail(a[0].detail);

          setDataTable({
            columns:  [
                { label: '#', field: 'row' },
                { label: 'NO. TRANS', field: 'id_trans' },
                { label: 'DATE', field: 'tgl_beli' },
                { label: 'JUMLAH', field: 'jumlah' },
                { label: 'TOTAL', field: 'total' },
                { label: 'PEMBELI', field: 'pembeli' },
                { label: 'PENERIMA', field: 'penerima' },
                { label: 'ALAMAT', field: 'alamat' },
                { label: 'KODEPOS', field: 'kodepos' },
                { label: 'NOTELP', field: 'notelp' },
                { label: 'KOTA', field: 'kota' },
                { label: 'ACTION', field: 'action' },
                { label: 'INVOICE', field: 'invoice' }
            ],
            rows: tempTable
        });
      })
      .catch(error => alert('Error! ' + error.message));
    } catch (error) {
        console.error(error);
    }
  }

  function handleShowPopupDetail(id_trans){
    //alert("sesuatu detail");
    setidTransPopup(id_trans);
    setIsModalDetail(true);
    setIsModalInvoice(false);
  }
  function handleShowPopupInvoice(id_trans){
    //alert("sesuatu invoice");
    setidTransPopup(id_trans);
    setIsModalInvoice(true);
    setIsModalDetail(false);
  }

  async function handleAddItem(obj) {
    // console.log(obj);
    // alert("oi");
    try {
      const res = await fetch("/apiLaporan/getLaporanFilter?tglFrom=" + obj.tglFrom + "&tglUntil=" + obj.tglUntil + "&sortPrice=" + obj.sortPrice, { 
        method : "GET",
        headers : { "Content-Type" : "application/json" }
      });
      let a = await res.json();
      // console.log(res);
       console.log(a);

      let tempTable = a[0].header.map((item, index) => {
        let date = new Date(item.tanggal_beli);
        date.setDate(date.getDate()-1);
        return({
          row: index+1,
          id_trans: `#${item.id_trans}`,
          tgl_beli: date.toDateString(),
          jumlah: item.jumlah_beli,
          total: "Rp." + item.total_harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.'),
          pembeli: item.nama_pembeli,
          penerima: item.nama_penerima,
          alamat: item.alamat_pengiriman,
          kodepos: item.kode_pos,
          notelp: item.no_telp,
          kota: item.nama_kota,

          action: <button onClick={() => handleShowPopupDetail(item.id_trans) } 
                      style={{fontSize:"0.7vw", textAlign:"center"}}
                      className="btn btn-primary">
                      &nbsp; Detail &nbsp;
                      </button>,
          invoice: <button onClick={() => handleShowPopupInvoice(item.id_trans) }
                  style={{fontSize:"0.68vw", textAlign:"center"}}
                  className="btn btn-primary">
                  &nbsp; Invoice &nbsp;
                  </button>
      })
      });

      setDataSetTable(tempTable);
      setDataLaporanHeader(a[0].header);
      setDataLaporanDetail(a[0].detail);

      setDataTable({
            columns:  [
                { label: '#', field: 'row' },
                { label: 'NO. TRANS', field: 'id_trans' },
                { label: 'DATE', field: 'tgl_beli' },
                { label: 'JUMLAH', field: 'jumlah' },
                { label: 'TOTAL', field: 'total' },
                { label: 'PEMBELI', field: 'pembeli' },
                { label: 'PENERIMA', field: 'penerima' },
                { label: 'ALAMAT', field: 'alamat' },
                { label: 'KODEPOS', field: 'kodepos' },
                { label: 'NOTELP', field: 'notelp' },
                { label: 'KOTA', field: 'kota' },
                { label: 'ACTION', field: 'action' },
                { label: 'INVOICE', field: 'invoice' }
            ],
            rows: tempTable
        });

      //console.log(dataLaporanHeader);
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
      getDataLaporanFromAPI();
  }, []);


  return (
    <div className="pb-5" style={{backgroundImage: "url(" + Background + ")", backgroundSize:"cover"}}>
      <br/><br/>
      <div style={{marginLeft:"auto", marginRight:"auto", width:"1450px", position:"relative", backgroundColor:"white", border:"4px solid rgb(211,212,212)", borderRadius:"20px", padding:"70px", fontFamily: "'Comfortaa', cursive", fontSize:"13px"}}>
        <center><h1>Laporan Penjualan</h1></center><br/>
        <div>
            <div style={{minWidth:"33%", float:"left"}}>.</div>
            <div style={{minWidth:"33%", float:"left"}}> <center><FormFilter onAddItem={handleAddItem} /> </center></div>
            <div style={{minWidth:"33%", float:"left"}}>.</div>
        </div>
        <div style={{clear:"both"}}></div>
        {
          dataLaporanHeader.length ? <Posts dataSetTable={dataSetTable} dataLaporanHeader={dataLaporanHeader} dataLaporanDetail={dataLaporanDetail} dataTable={dataTable} idTransPopup={idTransPopup} isModalInvoice={isModalInvoice} isModalDetail={isModalDetail} onCloseDetail={() => setIsModalDetail(false)} onCloseInvoice={() => setIsModalInvoice(false)}/> : "kosong"
        }
        
      </div>
    </div>
  );
}
export default Laporan;