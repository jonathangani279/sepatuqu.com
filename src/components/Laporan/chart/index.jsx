import React, { useState, useEffect } from "react";
import Chart from './chart';
import ChartBar from './chartBar';
import ChartPie from './chartPie';

const Invoice= () => {
  const [ jumlahPenjualan, setJumlahPenjualan ] = useState([]);
  const [ namaProduk, setNamaProduk ] = useState([]);
  const [ chartData, setChartData ] = useState({});

  async function getDataLaporanFromAPI() {
    try {
      const data = await fetch("/apiLaporan/getMostSold", { method: "GET" });
      let temp = await data.json();
      //console.log(temp[0].nama);
      // setNamaProduk(temp[0].nama);
      // setJumlahPenjualan(temp[0].jumlah);
      setChartData({
        labels: temp[0].nama,
        datasets:[
          {
            label:'Chart Sold Shoes',
            data:temp[0].jumlah,
            backgroundColor:[
              'rgba(85, 85, 85, 0.6)',
              'rgba(237, 231, 52, 1)',
              'rgba(233, 27, 32, 1)',
              'rgba(1, 1, 50, 1)',

              'rgba(85, 85, 85, 0.6)',
              'rgba(237, 231, 52, 1)',
              'rgba(233, 27, 32, 1)',
              'rgba(1, 1, 50, 1)',

              'rgba(85, 85, 85, 0.6)',
              'rgba(237, 231, 52, 1)',
              'rgba(233, 27, 32, 1)',
              'rgba(1, 1, 50, 1)',
            ]
            
          }
        ]
    });
    } catch (error) {
        console.error(error);
    }
  }

  useEffect(() => {
    getDataLaporanFromAPI();
    
  }, []);

  return (
    <div className="pb-5">
      {/* <Chart chartData={chartData} location="Massachusetts" legendPosition="top"/> */}
      <br/><br/>
      <h1 style={{fontFamily: "'Comfortaa', cursive",textAlign: "center"}}>Best Seller Product Chart</h1>
      
      <ChartBar chartData={chartData} location="Massachusetts" legendPosition="top"/>
      <ChartPie chartData={chartData} location="Massachusetts" legendPosition="top"/>
      <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    </div>
  );  
}
export default Invoice;