import React, { useState, useEffect } from 'react';
import {Bar, Line, Pie} from 'react-chartjs-2';

const chart = ({ chartData, location, legendPosition }) => {
  const pageNumbers = [];

  return (
    <div className="chart">
        <Bar
          data={chartData}
          options={{
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Quantity Sold Product',
                        fontSize: "20",
                        fontFamily: "'Comfortaa', cursive"
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Product Name',
                        fontSize: "20",
                        fontFamily: "'Comfortaa', cursive"
                    }
                }]
            },
            title:{
              display:true,
              text:'Best Seller Product Chart',
              fontSize:25,
              fontFamily: "'Comfortaa', cursive"
            },
            legend:{
              display:true,
              position:legendPosition
            }
          }}
        /><br/><br/><br/><br/><br/><br/>

        <Line
          data={chartData}
          options={{
            
            scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  },
                  scaleLabel: {
                      display: true,
                      labelString: 'Quantity Sold Product',
                      fontSize: "20",
                      fontFamily: "'Comfortaa', cursive"
                  }
              }],
              xAxes: [{
                  scaleLabel: {
                      display: true,
                      labelString: 'Product Name',
                      fontSize: "20",
                      fontFamily: "'Comfortaa', cursive"
                  }
              }]
          },
            title:{
              display:true,
              text:'Largest Cities In '+location,
              fontSize:25,
              fontFamily: "'Comfortaa', cursive"
            },
            legend:{
              display:true,
              position:legendPosition
            }
          }}
        /><br/><br/><br/><br/><br/><br/>

        <Pie
          data={chartData}
          options={{
            
            scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  },
                  scaleLabel: {
                      display: true,
                      labelString: 'Quantity Sold Product',
                      fontSize: "20",
                      fontFamily: "'Comfortaa', cursive"
                  }
              }],
              xAxes: [{
                  scaleLabel: {
                      display: true,
                      labelString: 'Product Name',
                      fontSize: "20",
                      fontFamily: "'Comfortaa', cursive"
                  }
              }]
          },
            title:{
              display:true,
              text:'Largest Cities In '+location,
              fontSize:25,
              fontFamily: "'Comfortaa', cursive"
            },
            legend:{
              display:true,
              position:legendPosition
            }
          }}
        />
        <br/><br/>
      </div>
  );
};

export default chart;