import React from 'react';
import ReactDOM from "react-dom";
import { Context } from '../UserContext';
import scriptLoader from "react-async-script-loader";

const CLIENT = {
  	sandbox:
		"AeUGa6lOsxV2EIqUvlmJ9uejBUG_G5p9EKSWwXoD6wl_PyqELbkhA958rfblocwnddsRCuHaFwnY78oe",
	production:
		"EE4T2FQfjs9RN0sNtuWCSoMTzVsFqm8msgwZO3NTbYVAcQsJFxj4_EmlTpe-W4Z39ltpdO1v79sEJa11"
};

const CLIENT_ID = process.env.NODE_ENV === "production" ? CLIENT.production : CLIENT.sandbox;

let PayPalButton = null;

class PaypalButton extends React.Component {
	static contextType= Context;

	constructor(props) {
		super(props);
		this.state = {
			showButtons: false,
			loading: true,
			paid: false,
			usersaldo:this.props.usersaldo,
			saldotopup:this.props.saldotopup,
			userid:this.props.userid
		};

		window.React = React;
		window.ReactDOM = ReactDOM;
	}

	componentDidMount() {
		const { isScriptLoaded, isScriptLoadSucceed } = this.props;

		if (isScriptLoaded && isScriptLoadSucceed) {
			PayPalButton = window.paypal.Buttons.driver("react", { React, ReactDOM });
			this.setState({ loading: false, showButtons: true });
		}
	}
	
	componentWillReceiveProps(nextProps) {
		const { isScriptLoaded, isScriptLoadSucceed } = nextProps;

		const scriptJustLoaded = !this.state.showButtons && !this.props.isScriptLoaded && isScriptLoaded;

		if (scriptJustLoaded) {
			if (isScriptLoadSucceed) {
				PayPalButton = window.paypal.Buttons.driver("react", {
				React,
				ReactDOM
				});
				this.setState({ loading: false, showButtons: true });
			}
		}
	}

	createOrder = (data, actions) => {
		return actions.order.create({
			purchase_units: [{
				description: +"Top Up",
				amount: {
					currency_code: "USD",
					value: parseFloat(this.state.saldotopup)
				}
			}]
		});
	};

  	onApprove = (data, actions) => {
		actions.order.capture().then(async details => {
		const paymentData = {
			payerID: data.payerID,
			orderID: data.orderID
		};
		
		console.log("Payment Approved: ", paymentData);

        this.setState({ showButtons: false, paid: true });
            console.log(this.state.saldotopup);
            console.log(this.state.userid);
            
			try {
				const fetchAPI= await fetch(`/api/users/info/${this.state.userid}`, {
					method: 'PUT',
					headers: { 
						"Content-Type" : "application/json" 
					},
					body: JSON.stringify({
						saldo: parseInt(this.state.saldotopup) * 14098
					})
				});
				const res= await fetchAPI.json();

				if (res.status !== 200) {
					return alert(`Failed to save.\n${res.message}`);
				}
				
				const user= this.context;
				
				user.setUserInfo(res.editedInfo);
			} catch (error) {
				console.error(error);
			}
		});
	};

	render() {
		const { showButtons, loading, paid } = this.state;
		
		return (
			<div className="main">
				{ loading }

				{ showButtons && (
					<div>
						<div>
							<h3>
								Top Up Amount : USD {this.props.saldotopup}
							</h3>
						</div>
						<br/>
						<PayPalButton createOrder={ (data, actions) => this.createOrder(data, actions) }
									  onApprove={ (data, actions) => this.onApprove(data, actions) } />
					</div>
				) }

				{ paid && (
					<div className="main">
						<h2>
							Top Up Successfully!{" "}
							<span role="img" aria-label="emoji">
								{" "}
								😉
							</span>
						</h2>
					</div>
				) }
			</div>
		);
	}
}

export default scriptLoader(`https://www.paypal.com/sdk/js?client-id=${CLIENT_ID}`)(PaypalButton);