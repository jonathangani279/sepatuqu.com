import React from 'react';

const TitleBar = ({ title }) => {
    return (  
        <div className="mt-4"
             style={{ backgroundColor: '#EDE734' }}>
            <div className="container pt-4 pb-4">
                <h2 className="font-weight-bold"
                    style={{ color: 'rgb(1, 1, 50)' }}>
                    { title }
                </h2>
            </div>
        </div>
    );
}
 
export default TitleBar;