import React, { useState, useEffect } from 'react';
import { faEdit } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Title from './Title';
import { Modal } from 'react-bootstrap';
import useFormInput from '../../libraries/useFormInput';
import { useHistory, Link } from 'react-router-dom';
const Manage = ({ match }) => {
    const [ name, setName ]= useState();
    const [ brand, setBrand ]= useState();
    const [ kategori, setKategori ]= useState();
    const [ harga, setHarga ]= useState();
    const [ status, setStatus ]= useState();

    const [ sneakers, setSneakers ]= useState([]);
    const [ detail, setDetail ]= useState();
    const [ show, setShow ] = useState(false);
    const history= useHistory();

    function UbahUrl(e){
        if(e.target.cari.value==""){
            if(e.target.cariKategori.value==""){
                history.push(`/ManageMasterBarang/semua`);//ini redirect ke index.jsx nya admin
                return window.location.reload();
            }
            else{
                history.push(`/ManageMasterBarang/semua/${e.target.cariKategori.value}`);//ini redirect ke index.jsx nya admin
                return window.location.reload();
            }
        }
        else{
            if(e.target.cariKategori.value==""){
                history.push(`/ManageMasterBarang/${e.target.cari.value}/semua`);//ini redirect ke index.jsx nya admin
                return window.location.reload();
            }
            else{
                history.push(`/ManageMasterBarang/${e.target.cari.value}/${e.target.cariKategori.value}`);//ini redirect ke index.jsx nya admin
                return window.location.reload();
            }
        }
    }
    async function getSneaker() {
        if(match.params.cari=="semua"){
            if(match.params.carikategori=="semua"){
                const fetchAPI= await fetch(`/api/sneakers/`, {
                    method: 'GET'
                });
                const result= await fetchAPI.json();
                
                result.sneakers= result.sneakers.map(item => ({
                    id_sneaker: item.id_sneaker,
                    nama_sneaker: item.nama_sneaker,
                    gambar: item.gambar
                }));
                setSneakers(result.sneakers);
            }
            else{
                const fetchAPI= await fetch(`/api/sneakers/AdminGetSneaker/semua/${match.params.carikategori}`, {
                    method: 'GET'
                });
                const result= await fetchAPI.json();
                
                result.sneakers= result.sneakers.map(item => ({
                    id_sneaker: item.id_sneaker,
                    nama_sneaker: item.nama_sneaker,
                    gambar: item.gambar
                }));
                setSneakers(result.sneakers);
                if(result.sneakers.length==0){
                    alert("Sneaker Yang Dicari Tidak Ada!");
                }
            }
        }
        else{
            if(match.params.carikategori=="semua"){
                const fetchAPI= await fetch(`/api/sneakers/AdminGetSneaker/${match.params.cari}/semua`, {
                    method: 'GET'
                });
                const result= await fetchAPI.json();
                
                result.sneakers= result.sneakers.map(item => ({
                    id_sneaker: item.id_sneaker,
                    nama_sneaker: item.nama_sneaker,
                    gambar: item.gambar
                }));
                setSneakers(result.sneakers);
                if(result.sneakers.length==0){
                    alert("Sneaker Yang Dicari Tidak Ada!");
                }
            }
            else{
                const fetchAPI= await fetch(`/api/sneakers/AdminGetSneaker/${match.params.cari}/${match.params.carikategori}`, {
                    method: 'GET'
                });
                const result= await fetchAPI.json();
                
                result.sneakers= result.sneakers.map(item => ({
                    id_sneaker: item.id_sneaker,
                    nama_sneaker: item.nama_sneaker,
                    gambar: item.gambar
                }));
                setSneakers(result.sneakers);
                if(result.sneakers.length==0){
                    alert("Sneaker Yang Dicari Tidak Ada!");
                }
            }
        }
    }
    async function handleEdit(id_sneaker) {
        setShow(true);

        try {
            const fetchAPI= await fetch(`/api/sneakers/AdminGetDetailSneaker/${id_sneaker}`);
            const result= await fetchAPI.json();
            
            setDetail(result.detail);

            setName(result.detail[0].nama_sneaker);
            setBrand(result.detail[0].brand_sneaker);
            setKategori(result.detail[0].id_kategori);
            setHarga(result.detail[0].harga_sneaker);
            setStatus(result.detail[0].status_sneaker);
        } catch (error) {   
            console.log(error);
        }
    }
    async function handleSubmit(e) {
        e.preventDefault();
        let fields= {
            id: detail[0].id_sneaker,
            name: e.target.name.value,
            brand: e.target.brand.value,
            category: e.target.kategori.value,
            price: e.target.harga.value,
            status: e.target.status.value
        }
        if (!Object.keys(fields).every(key => fields[key] !== '')) {
            return alert('All field required.');
        }

        try {
            const fetchAPI= await fetch('/api/sneakers/AdminGetEditSneaker/', {
                method: 'PUT',
                headers: { 'Content-Type' : 'application/json' },
                body: JSON.stringify(fields)
            });
            const res= await fetchAPI.json();
            alert(res.message);
            getSneaker();
            //window.location.reload();
        } catch (error) {
            return console.error(error);
        }
    }
    function handlename(e) {setName(e.target.value);}
    function handlebrand(e) {setBrand(e.target.value);}
    function handlekategori(e) {setKategori(e.target.value);}
    function handleharga(e) {setHarga(e.target.value);}
    function handlestatus(e) {setStatus(e.target.value);}

    function handleClose() { setShow(false); }
    useEffect(() => {
        getSneaker();
    }, [match.params.cari]);
	return (
		<>	
            <Title content={ `Manage Sneaker` } />
			<div className="container mt-5"
                 style={{ fontSize: '1.25vw' }}>
                <form  onSubmit={UbahUrl} className="form-inline d-flex justify-content-center md-form form-sm active-cyan-2 mt-2">
                    <div style={{width:"100%",textAlign:"center"}}>
                        <input className="form-control form-control-sm mr-3 w-75" name="cari" type="text" placeholder="Search" aria-label="Search"></input>
                        <button style={{backgroundColor:"transparent"}} type="submit" className="fas fa-search" aria-hidden="true"></button>
                        
                        <div class="input-group mr-3 w-25" style={{marginLeft:"10.8%",marginTop:"2%"}}>
                            <div class="input-group">
                                <label class="input-group-text">Category</label>
                            </div>
                            <select class="browser-default custom-select" name="cariKategori">
                                <option selected value="semua">Choose...</option>
                                <option value="1">Men</option>
                                <option value="2">Women</option>
                                <option value="3">Kids</option>
                            </select>
                        </div>
                    </div>
                </form>
                    
                <div className="row mt-5">
                    {
                        sneakers.map((item, index) => {
                            return (
                                <div className="col-3 mb-4"
                                     key={ index }>
                                    <div className="card">
                                        <div className="card-header text-center"
                                             style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                                            <h6 className="card-text text-white">
                                                { item.nama_sneaker }
                                            </h6>
                                        </div>
                                        
                                        <Link style={{ textDecoration: 'none' }}>  
                                            <img className="card-img-top"
                                                 src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar } />
                                        </Link>

                                        <div className="card-body d-flex justify-content-center">
                                            <button className="btn btn-primary" onClick={ () => handleEdit(item.id_sneaker) }>
                                                <FontAwesomeIcon icon={ faEdit }
                                                                 size={ '1x' } />
                                                &nbsp; Edit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
            <Modal style={{ fontSize: '1.25vw' }}
                   show={ show } 
                   onHide={ handleClose }>
                { detail ? 
                    <> 
                        <Modal.Header closeButton>
                            <Modal.Title>
                                Edit Sepatu 
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                        <div className="mt-4">
                                {
                                    detail.map((item, index) => {
                                        return (
                                            <div className="card mb-4"
                                                 key={ index }>
                                                <div className="card-title"
                                                     style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                                                    <h5 className="text-center text-white p-3">
                                                        ID Sneaker #{ item.id_sneaker }
                                                    </h5>
                                                </div>

                                                <div className="card-body"
                                                     style={{ padding: '0% !important' }}>
                                                    <div className="row">
                                                        <img className="col-12"
                                                             src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar} />
                                                        
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-12 mt-auto mb-auto">
                                                            <form onSubmit={ handleSubmit }>
                                                                Name: <br/><input className="form-control" type="text" name="name" id="" value={ name } onChange={ handlename }/> <br/>
                                                                Category: (1 : Men, 2 : Women, 3 : Kids)<br/><input className="form-control" type="text" name="kategori" id="" value={ kategori } onChange={ handlekategori }/> <br/>
                                                                Brand: <br/><input className="form-control" type="text" name="brand" id="" value={ brand } onChange={ handlebrand }/><br/>
                                                                Price: <br/><input className="form-control" type="text" name="harga" id="" value={ harga } onChange={ handleharga }/><br/>
                                                                Status: (0 : Unavalaible, 1 : Avalaible)<br/><input className="form-control" type="text" name="status" id="" value={ status } onChange={ handlestatus }/>
                                                                <br/>
                                                                <center>
                                                                    <input className="btn btn-primary" type="submit" value="SAVE EDIT"/>
                                                                </center>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </Modal.Body>
                    </> 
                : '' } 
            </Modal>
		</>
	);
}
 
export default Manage;