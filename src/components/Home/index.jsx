import React, { useContext } from 'react';
import ControlledCarousel from '../ControlledCarousel';

const Home = () => {

	return (  
		<>	
			<ControlledCarousel images={ [
				'DayBreak_header.jpg',
				'UltraBoostCity_header.jpg',
				'AirForce1Peach_header.jpg',
				'PumaXHelloKitty_header.jpg'
			] } />

			<div className="container mb-5">
				<div className="d-flex justify-content-center">
					<img className="mt-5 mr-5"
						width="10%"
						style={{ opacity: 1 }}
						src={ process.env.PUBLIC_URL+'/images/brands/adidas.png'} />
					<img className="mt-5 ml-5 mr-5"
						width="10%"
						style={{ opacity: 1 }}
						src={ process.env.PUBLIC_URL+'/images/brands/nike.png'} />
					<img className="mt-5 ml-5 mr-5"
						width="10%"
						style={{ opacity: 1 }}
						src={ process.env.PUBLIC_URL+'/images/brands/puma.png'} />
					<img className="mt-5 ml-5"
						width="10%"
						style={{ opacity: 1 }}
						src={ process.env.PUBLIC_URL+'/images/brands/ageless_galaxy.png'} />
				</div>

				<hr className="mt-5" 
					style={{ backgroundColor: 'rgb(1, 1, 50)', height: '0.15vw' }} />
				
				<div className="mt-4" 
					 style={{ opacity: 0.7, fontSize: '1vw' }}>
					<h6 className="text-center mb-4">
						SepatuQu: Originals Sneakers Shoes Online Store
					</h6>
					<p className="text-justify">
						Kebutuhan sepatu sneakers berkualitas originals sudah tidak bisa dibendung lagi. 
						Kualitas yang ditawarkan dari material bahan originals akan membuat sepatu-sepatu sneakers tersebut sangat nyaman digunakan dan dipakai untuk fashion harian. 
						Para pecinta sneakers atau sneakers head selalu ingin terus menerus melengkapi koleksi sepatu mereka. 
						Memiliki sepatu sneakers limited edition bisa membuat mereka sangat senang dan didewakan oleh kolektor sepatu lainnya. 
						Untuk memenuhi kebutuhan para kolektor dan sneakerhead yang mengidamkan koleksi sepatu sneakers terbaik, SepatuQu pun hadir. 
						SepatuQu adalah toko online yang menjual sepatu sneakers originals yang menjual beragam sneakers edisi khusus atau edisi terbatas, dari beragam brand sneaker ternama dunia seperti Nike, Adidas, Puma. 
						Jadi, jika sedang mencari dan ingin membeli sepatu sneakers originals, kunjungi saja website toko online SepatuQu. 
						Temukan sneakers shoes idaman secara online Anda di sana dengan harga terbaik, produk sepatu originals pilihan. 
						SepatuQu akan segera mengirimkan barang tersebut secepatnya setelah Anda menyelesaikan pembayaran dan mengonfirmasikannya. Selain menjual sepatu sneakers originals secara online, toko online ini juga menjual pakaian originals dengan banyak pilihan agar Anda bisa tampil stylish dan fashionable di saat bersamaan. 
						SepatuQu juga menjual beragam aksesoris originals dari brand Andrrows, Relace, Umbre dan lainnya. Semua produk originals dari sepatu sneakers, originals fashion dan aksesoris tersebut tersedia untuk pria, wanita dan anak-anak. 
						Toko online sepatu sneakers originals yang cocok untuk melengkapi koleksi sepatu sneakers dan beragam kebutuhan pakaian originals anda hanya di SepatuQu.
					</p>
				</div>
				
				<hr className="mt-5" 
					style={{ backgroundColor: 'rgb(1, 1, 50)', height: '0.15vw' }} />

				<div className="mt-4" 
					 style={{ opacity: 0.7, fontSize: '1vw' }}>
					<h6 className="text-center mb-4">
						Jual Sepatu Sneakers Online Originals Terlengkap
					</h6>
					<p className="text-justify">
						Sudah ada banyak toko online yang menjual sepatu sneakers, tapi tanpa jaminan keaslian barang dan kuaitas yang tidak bisa diketahui. 
						Agar menghindari dari produk palsu, untuk itu lebih baik membeli sneakers shoes di toko online SepatuQu. 
						Tersedia beragam pilihan sneakers originals dari brand ternama dunia seperti Nike, Adidas, Puma untuk melengkapi koleksi Anda yang bisa dibeli secara online. 
						Mengapa toko online SepatuQu adalah tempat yang tepat untuk membeli sepatu sneakers originals? Karena, Hanya di SepatuQu yang menjual beragam pilihan sneakers shoes limited edition secara online yang sulit ditemukan di tempat lain. 
						Koleksi sepatu sneakers yang dijual secara online oleh SepatuQu meliputi Nike Sportswear, Adidas, Puma Select dan jenis sneakers shoes lainnya. Selain produk-produk sepatu tersebut, online store ini juga menjual pakaian fashion berkualitas untuk kebutuhan fashion harian Anda. 
						Koleksi lengkap, jaminan keaslian dan produk originals adalah yang ditawarkan oleh toko online originals fashion SepatuQu. Tunggu apa lagi? Beli sepatu sneakers online originals di SepatuQu sekarang!
					</p>
				</div>
			</div>
		</>
	);
}
 
export default Home;