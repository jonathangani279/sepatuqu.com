import React, { useState, useEffect } from 'react';
import { MDBDataTableV5 } from 'mdbreact';
import { Modal } from 'react-bootstrap';

const OrdersHistory = ({ userInfo }) => {
    const [ transaction, setTransaction ]= useState();
    const [ show, setShow ] = useState(false);
    const [ dataTable, setDataTable ]= useState();
    
    async function getTransactions() {
        try {
            const fetchAPI= await fetch(`/api/users/transactions/${userInfo.id_user}`);
            const result= await fetchAPI.json();

            result.transactions= result.transactions.map((item, index) => {
                let date = new Date(item.tgl_beli);

                date.setDate(date.getDate()-1);

                return ({
                    row: index+1,
                    id_trans: `#${item.id_trans}`,
                    id_shipment: `#${item.id_shipment}`,
                    tgl_beli: date.toDateString(),
                    jumlah: item.jumlah,
                    total: 'Rp.'+item.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')+',-',
                    action: <button className="btn btn-primary"
                                    style={{ fontSize: '1.25vw' }}
                                    onClick={ () => handleShow(item.id_trans, item.id_shipment) }>
                                DETAIL
                            </button>
                })
            }); 
            
            setDataTable({
                columns:  [
                    { label: '#', field: 'row' },
                    { label: 'NO. TRANS', field: 'id_trans' },
                    { label: 'NO. SHIPMENT', field: 'id_shipment' },
                    { label: 'DATE', field: 'tgl_beli' },
                    { label: 'AMOUNT', field: 'jumlah' },
                    { label: 'TOTAL', field: 'total' },
                    { label: '', field: 'action' }
                ],
                rows: result.transactions
            });
        } catch(error) {
            console.error(error);
        }
    }

    useEffect(() => {
        getTransactions();
    }, []);

    async function handleShow(id_trans, id_shipment) {
        setShow(true);

        try {
            const fetchAPI= await fetch(`/api/users/transactions/${id_trans}/${id_shipment}`);
            const result= await fetchAPI.json();
            
            setTransaction(result.transaction);
        } catch (error) {   
            console.log(error);
        }
    }

    function handleClose() { setShow(false); }

    return (  
        <>
            { dataTable && dataTable.rows.length ?     
                <MDBDataTableV5 responsive
                                striped
                                small
                                searchBottom={ false }
                                data={ dataTable } />
            : 'No orders history.' }
            
            { transaction ? 
                <Modal style={{ fontSize: '1.25vw' }}
                       show={ show }
                       size="lg"
                       onHide={ handleClose }>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            NO. TRANSACTION #{ transaction.id_trans }
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="d-flex justify-content-between pt-1 pb-1">
                            <span>NO. SHIPMENT</span>
                            <span>#{ transaction.id_shipment }</span>
                        </div>

                        <div className="d-flex justify-content-between pt-1 pb-1">
                            <span>ADDRESS</span>
                            <span>{ `${transaction.alamat}, ${transaction.nama_kota}` }</span>
                        </div>

                        <div className="d-flex justify-content-between pt-1 pb-1">
                            <span>ZIP CODE</span>
                            <span>{ transaction.kode_pos }</span>
                        </div>

                        <div className="d-flex justify-content-between pt-1 pb-1">
                            <span>RECEIVER NAME</span>
                            <span>{ transaction.nama_penerima }</span>
                        </div>

                        <div className="d-flex justify-content-between pt-1 pb-1">
                            <span>PHONE NUMBER</span>
                            <span>{ transaction.no_telp }</span>
                        </div>

                        <div className="mt-4">
                            <h4>Details</h4>

                            {
                                transaction.sneakers.map((item, index) => {
                                    return (
                                        <div className="card mb-4"
                                            key={ index }>
                                            <div className="card-title"
                                                 style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                                                <h5 className="text-center text-white p-3">
                                                    { item.nama_sneaker }
                                                </h5>
                                            </div>

                                            <div className="card-body"
                                                style={{ padding: '0% !important' }}>
                                                <div className="row">
                                                    <img className="col-6"
                                                        src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar_sneaker } />
                                                    
                                                    <div className="col-6 mt-auto mb-auto">
                                                        <div className="p-2">
                                                            BRAND: { item.brand_sneaker }
                                                        </div>
                                                        <div className="p-2">
                                                            CATEGORY: { item.kategori_sneaker }
                                                        </div>
                                                        <div className="p-2">
                                                            QUANTITY: { item.jumlah_sneaker }
                                                        </div>
                                                        <div className="p-2">
                                                            PRICE: Rp.{ item.harga_satuan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') }
                                                        </div>
                                                        <div className="p-2">
                                                            SUBTOTAL: Rp.{ item.subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </Modal.Body>
                </Modal>
            : '' }
        </>
    );
}
 
export default OrdersHistory;