import React, { useState } from 'react';
import useFormInput from '../../libraries/useFormInput';
import PaypalButton from '../PaypalButton';

const Balance = ({ userInfo }) => {
    const [ showPaypal, setShowPaypal ]= useState(false);

    const balance= useFormInput();
    async function handleSubmit(e) {
        e.preventDefault();
        
        if (!balance.value) {
            return alert('Balance field required.');
        }
        
        if (balance.value <= 0) {
            return alert('Amount must greater than zero.');
        }

        setShowPaypal(true);
    }
    
    function cekpay() {
        if(showPaypal) {
            return (
                <PaypalButton usersaldo={userInfo.saldo } saldotopup={balance.value} userid={userInfo.id_user} />
            );
        } else {
            return (
                <>
                    <h5>
                        Your balance: 
                        Rp.{ userInfo.saldo.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') },- <br/>
                        1 USD = 14.098 Rupiah
                    </h5>
                    <form className="mt-3"
                        onSubmit={ handleSubmit }>
                        <div className="form-row">
                            <div className="form-group col-6">
                                <input className="form-control"
                                    type="number" { ...balance }
                                    min="0" 
                                    autoFocus />
                            </div>
                            
                            <div className="form-group col-6">
                                <button className="btn btn-primary">TOP UP</button>
                            </div>
                        </div>
                    </form>
                </>
            );
        }
    }
    return (     
        cekpay()
    );
}
 
export default Balance;