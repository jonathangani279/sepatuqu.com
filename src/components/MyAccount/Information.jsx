import React, { useContext } from 'react';
import useFormInput from '../../libraries/useFormInput';
import { Context } from '../UserContext';

const Information = ({ userInfo }) => {
    const { setUserInfo }= useContext(Context);
    const nama= useFormInput(userInfo.nama);
    const current_password= useFormInput('');
    const new_password= useFormInput('');
    const confirm_password= useFormInput('');
    
    async function handleSubmit(e) {
        e.preventDefault();

        let password_fields= {
            current: current_password.value,
            new: new_password.value,
            confirm: confirm_password.value
        };

        if (Object.keys(password_fields).some(key => password_fields[key] !== '')) {
            if (!Object.keys(password_fields).every(key => password_fields[key] !== '')) {
                return alert('All password field required.');
            }

            if (current_password.value !== userInfo.password) {
                return alert('Wrong old password.');
            }

            if (new_password.value.length < 6) {
                return alert('6 character length minimum for password.');
            }

            if (new_password.value !== confirm_password.value) {
                return alert('New Password & Confirm-Password not same.');
            }
        }

        if (!nama.value) {
            return alert('Required field name.');
        }

        try {
            const fetchAPI= await fetch(`/api/users/info/${userInfo.id_user}`, {
                method: 'PUT',
                headers : { 
                    "Content-Type" : "application/json" 
                },
                body: JSON.stringify({
                    nama: nama.value,
                    new_password: new_password.value
                })
            });
            const res= await fetchAPI.json();

            if (res.status !== 200) {
                return alert(`Failed to save.\n${res.message}`);
            }

            alert(res.message);
            setUserInfo(res.editedInfo);
        } catch (error) {
            console.error(error);
        }
    }

    return ( 
        <>
            <form onSubmit={ handleSubmit }>
                <h4>ACCOUNT INFORMATION.</h4>

                <div className="form-row mt-3">
                    <div className="form-group col-6">
                        <label>NAMA</label>
                        <input className="form-control"
                               type="text" { ...nama } 
                               autoFocus />
                    </div>

                    <div className="form-group col-6">
                        <label>E-MAIL</label>
                        <input className="form-control"
                               type="text" 
                               value={ userInfo.email } disabled />
                    </div>
                </div>

                <h4 className="mt-4">CHANGE PASSWORD.</h4>

                <div className="form-row mt-3">
                    <div className="form-group col-12">
                            <label>CURRENT PASSWORD</label>
                            <input className="form-control"
                                   type="password" { ...current_password } />
                        </div>

                        <div className="form-group col-6">
                            <label>NEW PASSWORD</label>
                            <input className="form-control"
                                   type="password" { ...new_password } />
                        </div>
                        
                    <div className="form-group col-6">
                            <label>CONFIRM NEW PASSWORD</label>
                            <input className="form-control"
                                   type="password" { ...confirm_password } />
                    </div>
                </div>

                <button className="btn btn-primary"
                        type="submit">
                    SAVE
                </button>
            </form>
        </>
    );
}
 
export default Information;