import React, { useState, useContext } from 'react';
import { Link } from "react-router-dom";
import Information from './Information';
import OrdersHistory from './OrdersHistory';
import Balance from './Balance';
import { Context } from '../UserContext';
import AuthenticatedContent from '../AuthenticatedContent';

const MyAccount = () => {
    const { userInfo }= useContext(Context);
    const [ component, setComponent ]= useState();
    
    function handleTab(tab) {
        let cTemp;

        if (tab === 1) 
            cTemp= React.cloneElement(
                <Information userInfo={ userInfo } />
            );
        else if (tab === 2) 
            cTemp= React.cloneElement(
                <OrdersHistory userInfo={ userInfo } />
            );
        else if (tab === 3) 
            cTemp= React.cloneElement(
                <Balance userInfo={ userInfo } />
            );

        setComponent(cTemp);
    }

    function showComponent() {
        return (
            !component ? <Information userInfo={ userInfo } /> : component  
        );
    }

    return (
        <AuthenticatedContent>
            <div className="container mt-4 mb-5"
                 style={{ fontSize: '1.25vw' }}>
                <div className="row">
                    <div className="col-3">
                        <Link className="navbar text-dark"
                              style={{ borderBottom: '0.2vw solid rgb(1, 1, 50)' }}
                              to="#"
                              onClick= { () => handleTab(1) }>
                            Account Information
                        </Link>
                        <Link className="navbar text-dark"
                              style={{ borderBottom: '0.2vw solid rgb(1, 1, 50)' }}
                              to="#"
                              onClick= { () => handleTab(2) } >
                            My Orders History
                        </Link>
                        <Link className="navbar text-dark"
                              style={{ borderBottom: '0.2vw solid rgb(1, 1, 50)' }}
                              to="#"
                              onClick= { () => handleTab(3) } >
                            My Balance
                        </Link>
                    </div>
                    
                    <div className="col-9">
                        <div className="ml-5">
                            { userInfo ? showComponent() : '' }
                        </div>
                    </div>
                </div>
            </div>
        </AuthenticatedContent>
    );
}
 
export default MyAccount;