import React from 'react';
import useFormInput from '../../libraries/useFormInput';

const Register = () => {
    const username= useFormInput('');
    const nama= useFormInput('');
    const email= useFormInput('');
    const password= useFormInput('');
    const confirm_password= useFormInput('');

    async function handleSubmit(e) {
        e.preventDefault();

        let fields= {
            username: username.value,
            nama: nama.value,
            email: email.value,
            password: password.value,
            confirm_password: confirm_password.value
        }

        if (!Object.keys(fields).every(key => fields[key] !== '')) {
            return alert('All field required.');
        }

        if (password.value.length < 6) {
            return alert('6 character length minimum for password.');
        }

        if (password.value !== confirm_password.value) {
            return alert('Password & Confirm-Password not same.');
        }

        try {
            const fetchAPI= await fetch('/api/users/register', {
                method: 'POST',
                headers: { 'Content-Type' : 'application/json' },
                body: JSON.stringify({
                    username: username.value,
                    nama: nama.value,
                    email: email.value,
                    password: password.value,
                    saldo: 0,
                    status_verifikasi: 0
                })
            });
            const res= await fetchAPI.json();

            if (res.status !== 200) {
                return alert(`Failed to register.\n${res.message}`);
            }

            alert(res.message);
        } catch (error) {
            return console.error(error);
        }
    }

    return (  
        <>
            <div className="container mt-4 mb-5"
                 style={{ fontSize: '1.25vw' }}>
                <h1 className="font-weight-bold">REGISTER.</h1>   
                <form onSubmit={ handleSubmit }>
                    <div className="form-row">
                        <div className="form-group col-6">
                            USERNAME
                            <input className="form-control"
                                   type="text" { ...username } 
                                   autoFocus />
                        </div>
                        
                        <div className="form-group col-6">
                            NAMA
                            <input className="form-control"
                                   type="text" { ...nama } /> 
                        </div>
                        
                        <div className="form-group col-12">
                            E-MAIL
                            <input className="form-control"
                                   type="email" { ...email } />
                        </div>

                        <div className="form-group col-6">
                            PASSWORD
                            <input className="form-control"
                                   type="password" { ...password } />
                        </div>

                        <div className="form-group col-6">
                            CONFIRM-PASSWORD
                            <input className="form-control"
                                   type="password" { ...confirm_password } />
                        </div>
                    </div>

                    <button className="btn btn-primary pl-5 pr-5">REGISTER</button>
                </form>
            </div>
        </>
    );
}
 
export default Register;