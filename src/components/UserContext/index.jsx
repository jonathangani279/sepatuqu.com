import React, { createContext, useEffect, useState } from 'react';
import useAuth from '../../libraries/UserAuth';

const UserContext = ({ children }) => {
    let { user } = useAuth();
    const [ userInfo, setUserInfo ]= useState(null);

	user = JSON.parse(user);

    useEffect(() => {
        const getUserInfo = async () => {
            try {
                const fetchAPI= await fetch(`/api/users/info`, { 
                    method: 'GET',
                    headers: {
                        'x-access-token': user.token
                    } 
                });
                const res= await fetchAPI.json();
                
                setUserInfo(res.info);
            } catch (error) {
                console.error(error);
            }
        };

        if (user) {
            if (user.id === 'admin') {
                setUserInfo('admin');
            } else{ 
                getUserInfo();
            }
        }
    }, []);

    return (  
        <Context.Provider value={{ userInfo, setUserInfo }}>
            { children }
        </Context.Provider>
    );
}

export const Context= createContext();
export default UserContext;