﻿import React, { useContext } from 'react';
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Link } from "react-router-dom";
import { Context } from "../UserContext";

//TODO Web Template Studio: Add a new link in the NavBar for your page here.
// A skip link is included as an accessibility best practice. For more information visit https://www.w3.org/WAI/WCAG21/Techniques/general/G1.
const NavBar = () => {
	const { userInfo }= useContext(Context);

	function CheckRole() {
		if (userInfo === "admin") {
			return (
				<nav className="navbar navbar-expand" 
					 style={{ backgroundColor: 'rgb(0, 0, 30.2)' }}>
					<ul className="navbar-nav mx-auto">
						<li className="nav-item">
							<Link className="nav-link" 
								  style={{ color: 'white' }}
								  to="/HomeMasterBarang/semua/semua">
								HOME
							</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" 
								  style={{ color: 'white' }}
								  to="/InsertMasterBarang">
								INSERT
							</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" 
								  style={{ color: 'white' }}
								  to="/ManageMasterBarang/semua/semua">
								MANAGE
							</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" 
								  style={{ color: 'white' }}
								  to="/laporan">
								REPORT
							</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" 
								  style={{ color: 'white' }}
								  to="/chart">
								CHART
							</Link>
						</li>
					</ul>
				</nav>
			);
		} else {
			return (
				<nav className="navbar navbar-expand" 
					style={{ backgroundColor: 'rgb(0, 0, 30.2)' }}>
					<ul className="navbar-nav mx-auto">
						<li className="nav-item">
							<Link className="nav-link" 
								  style={{ color: 'white' }}
								  to="/catalog/men">
								MEN'S
							</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" 
								  style={{ color: 'white' }}
								  to="/catalog/women">
								WOMEN'S
							</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" 
								  style={{ color: 'white' }}
								  to="/catalog/kid">
								KID'S
							</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" 
								  style={{ color: 'white' }}
								  to="/cart">
								<FontAwesomeIcon icon={ faShoppingCart }
												 size={ '1x' } />
							</Link>
						</li>
					</ul>
				</nav>
			);
		}
	}

	return (
		CheckRole()
	);
}

export default NavBar;
