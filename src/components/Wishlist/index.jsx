import React, { useContext } from 'react';
import { Context } from '../UserContext';
import WishlistItem from './WishlistItem';
import AuthenticatedContent from '../AuthenticatedContent';

const Wishlist = () => {
    const { userInfo }= useContext(Context);
    
    // const [wishlist,setWishlist] = useState([]); 
    // const [comment,setComment] = useState([]);
    // const history = useHistory();
    // useEffect(function(){
    //     getWishlist();
    // },[])

    // async function handleUpdateComment(id_sneaker,index)
    // {
    //     try {
    //         var fetch_api = await fetch("/api/users/updateCommentWishlist",{
    //             method : "POST",
    //             headers : {
    //                 "Content-Type" : "application/json"
    //             }, 
    //             body: JSON.stringify({
    //                 id_user    : userInfo.id_user,
    //                 id_sneaker : id_sneaker,
    //                 comment : comment[index]
    //             })
    //         });
    //         var result = await fetch_api.json();
    //         alert(result.msg);
    //         //getWishlist();
    //     } catch (error) {
    //         console.log(error);
    //     }
    // }
    // async function handleDeleteWishlist(id_sneaker,id_wishlist)
    // {
    //     try {
    //         var fetch_api = await fetch("/api/users/deleteItemWishlist",{
    //             method : "DELETE",
    //             headers : {
    //                 "Content-Type" : "application/json"
    //             }, 
    //             body: JSON.stringify({
    //                 id_user    : userInfo.id_user,
    //                 id_sneaker : id_sneaker,
    //                 id_wishlist:id_wishlist
    //             })
    //         });
    //         var result = await fetch_api.json();
    //         alert(result.msg);
    //         setWishlist(wishlist.filter(item =>item.id_wishlist!==id_wishlist));
            
    //         // if(result.msg=="Item berhasil dihapus")
    //         // {
    //         //     getWishlist();
    //         // }
    //         // else if(result.msg=="Item berhasil dihapus.")
    //         // {
    //         //     window.location.reload();
    //         // }

    //     } catch (error) {
    //         console.log(error);
    //     }
    // }
    // async function handleDeleteAll(){
    //     try {
    //         var fetch_api = await fetch("/api/users/deleteWishlist",{
    //             method : "DELETE",
    //             headers : {
    //                 "Content-Type" : "application/json"
    //             }, 
    //             body: JSON.stringify({
    //                 id_user    : userInfo.id_user,
    //             })
    //         });
    //         var result = await fetch_api.json();
    //         alert(result.msg);
    //         setWishlist([]);
    //     } catch (error) {
    //         console.log(error);
    //     }
    // }
    // async function handleEditWishlist(id_wishlist)
    // {
    //     history.push("/wishlist/"+id_wishlist);
    // }
    // async function handleAddAllToCart(){
    //     try {
    //         var fetch_api = await fetch(`/api/users/addAllWishlist`,{
    //             method : "POST",
    //             headers : {
    //                 "Content-Type" : "application/json"
    //             }, 
    //             body: JSON.stringify({
    //                 id_user    : userInfo.id_user,
    //             })
    //         });
    //         var result = await fetch_api.json();
    //         //setWishlist(result.result);
    //         alert(result.msg);
    //         if(result.msg=="Item berhasil masuk cart")
    //         {
    //             history.push("/cart");
    //         }
    //     } catch (error) {
    //         console.log(error);
    //     }
    // }
    // function editcomentar(commentar,index)
    // {
    //     let temp=[];
    //     //console.log(temp);
    //     comment.forEach(item => {
    //         temp.push(item);
    //     });
    //     temp[index] = commentar;

    //     setComment(temp);
    //     console.log(comment[index]);
    // }
    // function setFieldCommentar(commentar,index)
    // {
    //     let temp = comment;
    //     temp[index] = commentar;
    //     setComment(temp);
    //     return temp[index];
    // }
    // async function getWishlist(){
    //     try {
    //         var fetch_api = await fetch(`/api/users/wishlist/${userInfo.id_user}`,{
    //             method : "GET",
    //             headers : {
    //                 "Content-Type" : "application/json"
    //             }, 
    //         });
    //         var result = await fetch_api.json();
    //         setWishlist(result.result);
    //         let temp = [];
    //         for (let index = 0; index < result.result.length; index++) {
    //             temp.push(result.result[index].komentar);
    //         }
    //         console.log(temp);
    //         setComment(temp);
    //         // result.result=result.result.map(item=>({
    //         //     "komentar" : item.komentar
    //         // }))
            
    //         console.log(result.result);
    //         // setComment(result.result);
    //     } catch (error) {
    //         console.log(error);
    //     }
    // }
    
    return (  
        <AuthenticatedContent>
            { userInfo ? <WishlistItem id_user = {userInfo.id_user} /> : '' }
        </AuthenticatedContent>
    )
    //     <AuthenticatedContent>
    //         <Fragment>
    //             {
    //                 wishlist.map((item,index)=>{
    //                     return(
    //                         <Fragment key={index}>
    //                             <table style={{border:"1px solid black"}}>
    //                                 <tbody>
    //                                 <tr>
    //                                     <th>Gambar Sneaker</th>
    //                                     <th>Nama Sneaker</th>
    //                                     <th>Komentar</th>
    //                                     <th>Quantity</th>
    //                                     <th>Price</th>
    //                                 </tr>
    //                                 <tr>
    //                                     <td> <img className="card-img-top"
    //                                         src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar } style={{width:"100px", height:"100px"}}></img>
    //                                     </td>
    //                                     <td>{item.nama_sneaker}</td>
                                        
    //                                     <td><input type="text" onChange={(e)=>editcomentar(e.target.value,index)} value={comment[index]?comment[index]:item.komentar/*setFieldCommentar(item.komentar,index)*/} /> <button onClick={()=>handleUpdateComment(item.id_sneaker,index)}>Update</button> </td>
    //                                     <td>{item.jumlah_sneaker}</td>
    //                                     <td>{item.harga_sneaker}</td>
    //                                     <td><button onClick={ () => handleEditWishlist(item.id_wishlist)}>Edit Wishlist</button></td>
    //                                     <td><button onClick={ () => handleDeleteWishlist(item.id_sneaker,item.id_wishlist)}>Delete Item</button></td>
    //                                 </tr>
    //                                 </tbody>
    //                             </table>
    //                         </Fragment>
    //                     )
    //                 })
    //             }
    //             {wishlist.length!=0 ? <button onClick={ () => handleDeleteAll()}>Delete All</button>:"" }
    //             {wishlist.length!=0 ? <button onClick={ () => handleAddAllToCart()}>Add All To Cart</button>:"" }
    //         </Fragment>
    //     </AuthenticatedContent>
        
    // );
}
 
export default Wishlist;