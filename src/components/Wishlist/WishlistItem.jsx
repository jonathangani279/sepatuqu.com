import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import TitleBar from '../TitleBar';

const WihslistItem = ({ id_user }) => {
    const [ wishlist, setWishlist ] = useState([]); 
    const [ comment, setComment ] = useState([]);
    const history = useHistory();
    
    useEffect(function(){
        getWishlist();
    }, []);

    async function getWishlist(){
        try {
            var fetch_api = await fetch(`/api/users/wishlist/${id_user}`,{
                method : "GET",
                headers : {
                    "Content-Type" : "application/json"
                }, 
            });
            var result = await fetch_api.json();
            setWishlist(result.result);
            let temp = [];
            for (let index = 0; index < result.result.length; index++) {
                temp.push(result.result[index].komentar);
            }
            console.log(temp);
            setComment(temp);
            // result.result=result.result.map(item=>({
            //     "komentar" : item.komentar
            // }))
            
            console.log(result.result);
            // setComment(result.result);
        } catch (error) {
            console.log(error);
        }
    }

    async function handleUpdateComment(id_sneaker,index)
    {
        try {
            var fetch_api = await fetch("/api/users/updateCommentWishlist",{
                method : "POST",
                headers : {
                    "Content-Type" : "application/json"
                }, 
                body: JSON.stringify({
                    id_user    : id_user,
                    id_sneaker : id_sneaker,
                    comment : comment[index]
                })
            });
            var result = await fetch_api.json();
            alert(result.msg);
            //getWishlist();
        } catch (error) {
            console.log(error);
        }
    }

    async function handleDeleteWishlist(id_sneaker,id_wishlist)
    {
        try {
            var fetch_api = await fetch("/api/users/deleteItemWishlist",{
                method : "DELETE",
                headers : {
                    "Content-Type" : "application/json"
                }, 
                body: JSON.stringify({
                    id_user    : id_user,
                    id_sneaker : id_sneaker,
                    id_wishlist:id_wishlist
                })
            });
            var result = await fetch_api.json();
            alert(result.msg);
            setWishlist(wishlist.filter(item =>item.id_wishlist!==id_wishlist));
            
            // if(result.msg=="Item berhasil dihapus")
            // {
            //     getWishlist();
            // }
            // else if(result.msg=="Item berhasil dihapus.")
            // {
            //     window.location.reload();
            // }

        } catch (error) {
            console.log(error);
        }
    }

    async function handleDeleteAll(){
        try {
            var fetch_api = await fetch("/api/users/deleteWishlist",{
                method : "DELETE",
                headers : {
                    "Content-Type" : "application/json"
                }, 
                body: JSON.stringify({
                    id_user    : id_user,
                })
            });
            var result = await fetch_api.json();
            alert(result.msg);
            setWishlist([]);
        } catch (error) {
            console.log(error);
        }
    }

    async function handleEditWishlist(id_wishlist)
    {
        history.push("/wishlist/"+id_wishlist);
    }

    async function handleAddAllToCart(){
        try {
            var fetch_api = await fetch(`/api/users/addAllWishlist`,{
                method : "POST",
                headers : {
                    "Content-Type" : "application/json"
                }, 
                body: JSON.stringify({
                    id_user    : id_user,
                })
            });
            var result = await fetch_api.json();
            //setWishlist(result.result);
            alert(result.msg);
            if(result.msg=="Item berhasil masuk cart")
            {
                history.push("/cart");
            }
        } catch (error) {
            console.log(error);
        }
    }

    function editcomentar(commentar,index)
    {
        let temp=[];
        //console.log(temp);
        comment.forEach(item => {
            temp.push(item);
        });
        temp[index] = commentar;

        setComment(temp);
        console.log(comment[index]);
    }

    function showWishlists() {
        return (
            <>
                <table className="table-responsive">
                    <thead className="text-center"
                        style={{ 
                                backgroundColor: 'rgb(1, 1, 50)', 
                                color: 'white'
                        }}> 
                        <tr>
                            <th className="p-3">#NO.</th>
                            <th></th>
                            <th>SNEAKER NAME</th>
                            <th>NOTE</th>
                            <th>QUANTITY</th>
                            <th>PRICE</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody className="text-center">
                        {
                            wishlist.map((item, index) => {
                                return (
                                    <tr key={ index }>
                                        <td className="p-3"> 
                                            #{ item.id_wishlist }
                                        </td>
                                        <td className="p-3"> 
                                            <img className="img-fluid img-thumbnail"
                                                 width="180vw"
                                                 style={{ 
                                                    border: '0.15vw solid rgb(1, 1, 50)', 
                                                    borderRadius: '3%', 
                                                    boxShadow: 'inset 0 1px 1px rgba(1, 1, 50, 0.075), 0 0 8px rgba(1, 1, 50, 0.5)' 
                                                 }}
                                                 src={ process.env.PUBLIC_URL+'/images/sneakers/' + item.gambar } />
                                        </td>
                                        <td className="p-3">
                                            { item.nama_sneaker }
                                        </td>
                                        <td className="p-3">
                                            <input className="form-control w-100"
                                                   type="text" 
                                                   onChange={ (e) => editcomentar(e.target.value,index) } 
                                                   value={ comment[index] ? comment[index] : item.komentar } /> 
                                            <button className="btn btn-primary mt-3"
                                                    onClick={ () => handleUpdateComment(item.id_sneaker,index) }>
                                                UPDATE
                                            </button> 
                                        </td>
                                        <td className="p-3">
                                            { item.jumlah_sneaker }
                                        </td>
                                        <td className="p-3">
                                            Rp.{ item.harga_sneaker.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') },-
                                        </td>
                                        <td className="p-3">
                                            <button className="btn btn-primary"
                                                    onClick={ () => handleEditWishlist(item.id_wishlist) }>
                                                EDIT
                                            </button>
                                        </td>
                                        <td className="p-3">
                                            <button className="btn btn-primary"
                                                    onClick={ () => handleDeleteWishlist(item.id_sneaker, item.id_wishlist) }>
                                                REMOVE
                                            </button>
                                        </td>
                                    </tr>
                                );
                            })
                        }
                    </tbody>
                </table>
                <div className="d-flex mt-4">
                    { wishlist.length!=0 ? 
                        <button className="btn btn-primary mr-2"
                                onClick={ () => handleDeleteAll() }>
                            EMPTY WISHLIST
                        </button> 
                    : '' }
                    { wishlist.length!=0 ? 
                        <button className="btn btn-primary"
                                onClick={ () => handleAddAllToCart()}>
                            ADD ALL TO CART
                        </button> 
                    : '' }  
                </div>
            </>
        );
    }

    return (  
        <>
            <TitleBar title={ 'MY WISHLIST.' } />
            <div className="container mt-5 mb-5"
                 style={{ fontSize: '1.25vw' }}>
                { wishlist.length ? showWishlists() : 'No wishlists yet.' }
            </div>
        </>
    );
}
 
export default WihslistItem;