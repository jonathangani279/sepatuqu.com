import React, { useEffect, useState } from 'react';
import TitleBar from '../TitleBar';
import PageNavigation from '../PageNavigation';
import DetailSneaker from '../DetailSneaker';

const DetailWishlist = ({match}) => {
    const [ detail, setDetail ] = useState();

    useEffect(()=>{
        getDetailWishlist();      
    }, []);

    async function getDetailWishlist(){
        try {
            var fetch_api = await fetch(`/api/users/getWishlist/${match.params.id_wishlist}`);
            var result = await fetch_api.json();
            console.log(result);
            setDetail(result.result);
          } catch (error) {
              console.log(error);
          }
    }

    
    function showDetail() {
        return (
            <>
                <TitleBar title={ detail.nama_sneaker.toUpperCase()+'.' } />
                <div className="container mt-5">
                    <PageNavigation pages={ [
                        { name: 'HOME', path: '/' },
                        { name: "WISHLIST", path: `/wishlist` },
                        { name: detail.nama_sneaker.toUpperCase() }
                    ] } />
                    <DetailSneaker detail={ detail } 
                    from = {"wishlist"}
                    />
                </div>
            </>
        )
    }

    return ( 
        <>
            { detail ? showDetail() : '' }
        </>
    );
}
 
export default DetailWishlist;