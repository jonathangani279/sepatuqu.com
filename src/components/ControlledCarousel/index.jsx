import React, { useState }from 'react';
import Carousel from 'react-bootstrap/Carousel'

const ControlledCarousel = ({ images }) => {
    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex) => {
        setIndex(selectedIndex);
    };

    return (  
        <Carousel activeIndex={ index }
                  indicators={ false }
                  interval={ 3000 }
                  onSelect={ handleSelect }>
            {
                images.map((item, index) => {
                    return (
                        <Carousel.Item key={ index }>
                            <img className="img-fluid"
                                 src={ process.env.PUBLIC_URL+'/images/sneakers/'+item}
                            />
                        </Carousel.Item>
                    )
                })
            }
        </Carousel>
    );
}
 
export default ControlledCarousel;