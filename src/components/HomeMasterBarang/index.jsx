import React, { useState, useEffect } from 'react';
import { faEdit } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Title from './Title';
import { Modal } from 'react-bootstrap';
import useFormInput from '../../libraries/useFormInput';
import { useHistory, Link } from 'react-router-dom';
const Admin = ({ match }) => {
    const [ size_add, setSize_add ]= useState();
    const [ stock_add, setStock_add ]= useState();
    const [ stock_update, setStock_update ]= useState();
    const [ size_update, setSize_update ]= useState();
    
    const [ sneakers, setSneakers ]= useState([]);
    const [ detail, setDetail ]= useState();
    const [ ukuranSneaker, setUkuranSneaker ]= useState();
    const [ show, setShow ] = useState(false);
    const history= useHistory();

    function UbahUrl(e){
        if(e.target.cari.value==""){
            if(e.target.cariKategori.value==""){
                history.push(`/HomeMasterBarang/semua`);//ini redirect ke index.jsx nya admin
                return window.location.reload();
            }
            else{
                history.push(`/HomeMasterBarang/semua/${e.target.cariKategori.value}`);//ini redirect ke index.jsx nya admin
                return window.location.reload();
            }
        }
        else{
            if(e.target.cariKategori.value==""){
                history.push(`/HomeMasterBarang/${e.target.cari.value}/semua`);//ini redirect ke index.jsx nya admin
                return window.location.reload();
            }
            else{
                history.push(`/HomeMasterBarang/${e.target.cari.value}/${e.target.cariKategori.value}`);//ini redirect ke index.jsx nya admin
                return window.location.reload();
            }
        }
    }
    async function getSneaker() {
        if(match.params.cari=="semua"){
            if(match.params.carikategori=="semua"){
                const fetchAPI= await fetch(`/api/sneakers/`, {
                    method: 'GET'
                });
                const result= await fetchAPI.json();
                
                result.sneakers= result.sneakers.map(item => ({
                    id_sneaker: item.id_sneaker,
                    nama_sneaker: item.nama_sneaker,
                    gambar: item.gambar
                }));
                setSneakers(result.sneakers);
            }
            else{
                const fetchAPI= await fetch(`/api/sneakers/AdminGetSneaker/semua/${match.params.carikategori}`, {
                    method: 'GET'
                });
                const result= await fetchAPI.json();
                
                result.sneakers= result.sneakers.map(item => ({
                    id_sneaker: item.id_sneaker,
                    nama_sneaker: item.nama_sneaker,
                    gambar: item.gambar
                }));

                setSneakers(result.sneakers);
                if(result.sneakers.length==0){
                    alert("Sneaker Yang Dicari Tidak Ada!");
                }
            }
        }
        else{
            if(match.params.carikategori=="semua"){
                const fetchAPI= await fetch(`/api/sneakers/AdminGetSneaker/${match.params.cari}/semua`, {
                    method: 'GET'
                });
                const result= await fetchAPI.json();
                
                result.sneakers= result.sneakers.map(item => ({
                    id_sneaker: item.id_sneaker,
                    nama_sneaker: item.nama_sneaker,
                    gambar: item.gambar
                }));
                setSneakers(result.sneakers);
                if(result.sneakers.length==0){
                    alert("Sneaker Yang Dicari Tidak Ada!");
                }
            }
            else{
                const fetchAPI= await fetch(`/api/sneakers/AdminGetSneaker/${match.params.cari}/${match.params.carikategori}`, {
                    method: 'GET'
                });
                const result= await fetchAPI.json();
                
                result.sneakers= result.sneakers.map(item => ({
                    id_sneaker: item.id_sneaker,
                    nama_sneaker: item.nama_sneaker,
                    gambar: item.gambar
                }));
                setSneakers(result.sneakers);
                if(result.sneakers.length==0){
                    alert("Sneaker Yang Dicari Tidak Ada!");
                }
            }
        }
    }

    async function refreshModal(id_sneaker) {
        try {
            const fetchAPI= await fetch(`/api/sneakers/AdminGetDetailSneaker/${id_sneaker}`);
            const result= await fetchAPI.json();

            const fetchAPI2= await fetch(`/api/ukuran/${id_sneaker}`);
            const result2= await fetchAPI2.json();

            result2.ukuran= result2.ukuran.map(item => ({
                id_sneaker: item.id_sneaker,
                ukuran_sneaker: item.ukuran_sneaker,
                stok_sneaker: item.stok_sneaker
            }));
            setUkuranSneaker(result2.ukuran.sort(sortByProperty("ukuran_sneaker")));
            setDetail(result.detail);
            
        } catch (error) {   
            console.log(error);
        }
    }   

    function handleEdit(id_sneaker) {
        setShow(true);
        refreshModal(id_sneaker);
    }
    async function handleSubmit_add(e) {
        e.preventDefault();
        let fields= {
            id: detail[0].id_sneaker,
            size: e.target.size_add.value,
            stock: e.target.stock_add.value
        }
        if (!Object.keys(fields).every(key => fields[key] !== '')) {
            return alert('All field required.');
        }
        let total_size_sebelumnya=-1;
        ukuranSneaker.map((item, index) => {
            if(item.id_sneaker==fields.id){
                if(item.ukuran_sneaker==fields.size){
                    total_size_sebelumnya=item.stok_sneaker;
                }
            }
        });
        
        if(total_size_sebelumnya!=-1){
            fields.stock=parseInt(fields.stock)+parseInt(total_size_sebelumnya);
            try {
                const fetchAPI= await fetch('/api/ukuran/AdminUpdateStock/', {
                    method: 'PUT',
                    headers: { 'Content-Type' : 'application/json' },
                    body: JSON.stringify(fields)
                });
                const res= await fetchAPI.json();
                alert(res.message);
                
                refreshModal(fields.id);
                //window.location.reload();
            } catch (error) {
                return console.error(error);
            }
        }
        else{
            try {
                const fetchAPI= await fetch('/api/ukuran/AdminAddSize/', {
                    method: 'POST',
                    headers: { 'Content-Type' : 'application/json' },
                    body: JSON.stringify(fields)
                });
                const res= await fetchAPI.json();
                alert(res.message);
                
                refreshModal(fields.id);
                //window.location.reload();
            } catch (error) {
                return console.error(error);
            }
        }
    }
    async function handleSubmit_update(e) {
        e.preventDefault();
        let fields= {
            id: detail[0].id_sneaker,
            size: e.target.size_update.value,
            stock: e.target.stock_update.value
        }
        if (!Object.keys(fields).every(key => fields[key] !== '')) {
            return alert('All field required.');
        }
        try {
            const fetchAPI= await fetch('/api/ukuran/AdminUpdateStock/', {
                method: 'PUT',
                headers: { 'Content-Type' : 'application/json' },
                body: JSON.stringify(fields)
            });
            const res= await fetchAPI.json();
            alert(res.message);
            
            refreshModal(fields.id);
            //window.location.reload();
        } catch (error) {
            return console.error(error);
        }
    }
    function handleChangeSize(e){
        ukuranSneaker.map((item, index) => {
            if(item.ukuran_sneaker==e.target.value){
                setStock_update(item.stok_sneaker);
            }
        });
        setSize_update(e.target.value);
    }
    function sortByProperty(property){  
        return function(a,b){  
           if(a[property] > b[property])  
              return 1;  
           else if(a[property] < b[property])  
              return -1;  
       
           return 0;  
        }  
     }
    function handlesize_add(e) {setSize_add(e.target.value);}
    function handlestock_add(e) {setStock_add(e.target.value);}
    function handlestock_update(e) {setStock_update(e.target.value);}

    function handleClose() { setShow(false); }

    useEffect(() => {
        getSneaker();
    }, [match.params.cari]);

	return (  
		<>	
        <Title content={ `Home` } />
			<div className="container mt-5"
                 style={{ fontSize: '1.25vw' }}>
                <form  onSubmit={UbahUrl} className="form-inline d-flex justify-content-center md-form form-sm active-cyan-2 mt-2">
                    <div style={{width:"100%",textAlign:"center"}}>
                        <input className="form-control form-control-sm mr-3 w-75" name="cari" type="text" placeholder="Search" aria-label="Search"></input>
                        <button style={{backgroundColor:"transparent"}} type="submit" className="fas fa-search" aria-hidden="true"></button>
                        
                        <div class="input-group mr-3 w-25" style={{marginLeft:"10.8%",marginTop:"2%"}}>
                            <div class="input-group">
                                <label class="input-group-text">Category</label>
                            </div>
                            <select class="browser-default custom-select" name="cariKategori">
                                <option selected value="semua">Choose...</option>
                                <option value="1">Men</option>
                                <option value="2">Women</option>
                                <option value="3">Kids</option>
                            </select>
                        </div>
                    </div>
                </form>
                    
                <div className="row mt-5">
                    {
                        sneakers.map((item, index) => {
                            return (
                                <div className="col-3 mb-4"
                                     key={ index }>
                                    <div className="card">
                                        <div className="card-header text-center"
                                             style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                                            <h6 className="card-text text-white">
                                                { item.nama_sneaker }
                                            </h6>
                                        </div>
                                        
                                        <Link style={{ textDecoration: 'none' }}>  
                                            <img className="card-img-top"
                                                 src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar } />
                                        </Link>

                                        <div className="card-body d-flex justify-content-center">
                                            <button className="btn btn-primary" onClick={ () => handleEdit(item.id_sneaker) }>
                                                <FontAwesomeIcon icon={ faEdit }
                                                                 size={ '1x' } />
                                                &nbsp; Edit Size
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
            <Modal size="lg" style={{ fontSize: '1.25vw' }}
                   show={ show } 
                   onHide={ handleClose }>
                { detail ? 
                    <> 
                        <Modal.Header closeButton>
                            <Modal.Title>
                                Edit Size Sepatu 
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                        <div className="mt-4">
                                {
                                    detail.map((item, index) => {
                                        return (
                                            <div className="card mb-4"
                                                 key={ index }>
                                                <div className="card-title"
                                                     style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                                                    <h5 className="text-center text-white p-3">
                                                        { item.nama_sneaker }
                                                    </h5>
                                                </div>

                                                <div className="card-body"
                                                     style={{ padding: '0% !important' }}>
                                                    <div className="row">
                                                        <img className="col-12"
                                                             src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar} />
                                                    </div>
                                                    <div className="row">
                                                        <div className="col-lg-6">
                                                            <div className="card mb-4">
                                                                <div className="card-title"
                                                                    style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                                                                    <h5 className="text-center text-white p-3">
                                                                        Add Size :
                                                                    </h5>
                                                                </div>

                                                                <div className="card-body" style={{ padding: '0% !important' }}>
                                                                    <form onSubmit={ handleSubmit_add }>
                                                                        Size: <br/><input className="form-control" type="text" name="size_add" id="" value={ size_add } onChange={ handlesize_add }/> <br/>
                                                                        Stock: <br/><input className="form-control" type="text" name="stock_add" id="" value={ stock_add } onChange={ handlestock_add }/> <br/>
                                                                        
                                                                        <center>
                                                                            <input className="btn btn-primary" type="submit" value="ADD"/>
                                                                        </center>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-lg-6">
                                                            <div className="card mb-4">
                                                                <div className="card-title"
                                                                    style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                                                                    <h5 className="text-center text-white p-3">
                                                                    Update Size :
                                                                    </h5>
                                                                </div>

                                                                <div className="card-body" style={{ padding: '0% !important' }}>
                                                                    <form onSubmit={ handleSubmit_update }>
                                                                        Size : 
                                                                        <select style={{marginBottom:"10.5%"}} class="browser-default custom-select" name="size_update" id="" value={ size_update }  onChange={handleChangeSize}>
                                                                            <option selected value="">Choose...</option>
                                                                            {
                                                                                ukuranSneaker.map((item, index) => {
                                                                                    return(
                                                                                        <option value={item.ukuran_sneaker}>{item.ukuran_sneaker}</option>
                                                                                    );
                                                                                })
                                                                            }
                                                                        </select> 
                                                                        Stock: <br/><input className="form-control" type="text" name="stock_update" id="" value={ stock_update } onChange={ handlestock_update }/> <br/>
                                                                        
                                                                        <center>
                                                                            <input className="btn btn-primary" type="submit" value="UPDATE"/>
                                                                        </center>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </Modal.Body>
                    </> 
                : '' } 
            </Modal>
		</>
	);
}
 
export default Admin;