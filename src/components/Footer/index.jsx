﻿import React from "react";
import { faFacebookF, faTwitter, faInstagram } from "@fortawesome/free-brands-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

const Footer = () => {
	return (
		<footer 
				style={{ backgroundColor: 'rgb(0, 0, 30.2)' }}>
			<div className="container"
				 style={{ padding: '0%', fontSize: '1.25vw' }}>
				<div className="row">
					<div className="col"
						 style={{ padding: '0%' }}>
						<img width="55%"
							 src={ process.env.PUBLIC_URL+'/icons/SepatuQu2.png' }/>
					</div>

					<div className="col"
						 style={{ padding: '2.5%' }}>
						<ul className="list-unstyled ml-5">
							<li>
								<a className="text-white" href="">About Us</a>
							</li>
							<li>
								<a className="text-white" href="">Privacy Police</a>
							</li>
							<li>
								<a className="text-white" href="">Terms & Conditions</a>
							</li>
							<li>
								<a className="text-white" href="">Contact Us</a>
							</li>
							<li>
								<a className="text-white" href="">Sitemap</a>
							</li>
						</ul>
					</div>

					<div className="col"
						 style={{ padding: '2.5%' }}>
						<span className="text-white">Follow Us.</span>
						<div className="d-md-flex mt-3">
							<a className="text-white"
							   href="">
								<FontAwesomeIcon icon={ faFacebookF }
												 size={ '2x' } />
							</a>
							<a className="text-white"
							   href="">
								<FontAwesomeIcon icon={ faTwitter }
												 size={ '2x' } 
												 className="ml-4 mr-4"/>
							</a>
							<a className="text-white"
							   href="">
								<FontAwesomeIcon icon={ faInstagram }
											 	 size={ '2x' } />
							</a>
						</div>
					</div>
				</div>
			</div>

			<div style={{ backgroundColor: 'rgb(1, 1, 50)', fontSize: '1.25vw'}}>
				<div className="container text-white pt-3 pb-3">
					© 2019 SepatuQu. All Rights Reserved.
				</div>
			</div>
		</footer>
	);
}

export default Footer;