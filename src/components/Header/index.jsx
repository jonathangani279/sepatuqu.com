import React, { useContext } from 'react';
import { faUserAlt, faStar } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import useAuth from '../../libraries/UserAuth';
import { Link } from "react-router-dom";
import { useHistory } from 'react-router-dom';
import { Context } from "../UserContext";

const Header = ({ onKeyword, onStartSearch }) => {
    const { authOperation } = useAuth();
    const { userInfo }= useContext(Context);
    const history= useHistory();

    function handleLogout() {
        authOperation({ name: 'LOGOUT' });
		history.push('/');
		window.location.reload();
    }

    function handleKeyword(e) {
        onKeyword(e.target.value);
    }

    function handleStartSearch(e) {
        onStartSearch(e.keyCode);
    }

    function LogoCheckRole(){
        if (userInfo) {
            if (userInfo === "admin") {
                return (
                    <Link to="/HomeMasterBarang/semua/semua">
                        <img width="150vw" 
                             style={{ border: '0'}} 
                             src={ process.env.PUBLIC_URL+'/icons/SepatuQu.png' }/>
                    </Link>
                );
            }
            else {
                return (
                    <Link to="/">
                        <img width="50%" 
                             style={{ border: '0'}} 
                             src={ process.env.PUBLIC_URL+'/icons/SepatuQu.png' }/>
                    </Link>
                );
            }
        } else {
            return (
                <Link to="/">
                    <img width="50%" 
                         style={{ border: '0'}} 
                         src={ process.env.PUBLIC_URL+'/icons/SepatuQu.png' }/>
                </Link>
            );
        }
    }

    function CheckRole() {
        if (userInfo) {
            if (userInfo === "admin") {
                return (
                    <>
                        <span className="ml-auto mr-2">
                            Hello, Admin!
                        </span>
                        <Link style={{ marginTop: '-0.75%' }}
                              to="#">
                            <button className="btn btn-primary"
                                    style={{ fontSize: '1.25vw' }}
                                    onClick={ handleLogout }>
                                Logout
                            </button>
                        </Link> 
                    </>
                );
            } else {
                return (
                    <>
                        <input className="form-control w-50"
                               type="text" 
                               placeholder="Search entire products here" 
                               onChange={ handleKeyword } 
                               onKeyDown={ handleStartSearch } />
                        <Link className="ml-auto mr-auto"
                              style={{ color: 'rgb(1, 1, 70)' }}
                              to="/wishlist">
                            <FontAwesomeIcon icon={ faStar }
                                             size={ '1x' } />
                            Wishlist
                        </Link>
                        <Link style={{ color: 'rgb(1, 1, 70)' }}
                              to="/myAccount">
                            My Account
                        </Link> &nbsp; &nbsp;
                        <Link style={{ marginTop: '-0.75%' }}
                              to="#">
                            <button className="btn btn-primary"
                                    style={{ fontSize: '1.25vw' }}
                                    onClick={ handleLogout }>
                                Logout
                            </button>
                        </Link> 
                    </>
                );
            }
        } else {
            return (
                <>
                    <input className="form-control w-50"
                           type="text" 
                           placeholder="Search entire products here" 
                           onChange={ handleKeyword } 
                           onKeyDown={ handleStartSearch } />
                    <Link className="ml-auto mr-auto"
                          style={{ color: 'rgb(1, 1, 70)' }}
                          to="/wishlist">
                        <FontAwesomeIcon icon={ faStar }
                                         size={ '1x' } />
                        Wishlist
                    </Link>
                    <FontAwesomeIcon style={{ 
                                        marginTop: '-0.5%', 
                                        marginRight: '0.5%',
                                     }}
                                     icon={ faUserAlt }
                                     size={ '2x' } /> &nbsp;
                    <Link style={{ color: 'rgb(1, 1, 70)' }}
                          to="/register">
                        Register
                    </Link> &nbsp; or &nbsp;
                    <Link style={{ color: 'rgb(1, 1, 70)' }}
                          to="/login">
                        Sign In
                    </Link> 
                </>
            );
        }
    }
    
    return (  
        <>
            <header className="container"
                    style={{ padding: '0%', fontSize: '1.25vw' }}>
                <div className="row">
                    <div className="col-3"
                         style={{ padding: '0%' }}>
                        { LogoCheckRole() }
                    </div>
                    <div className="col-9 d-flex justify-content-between mt-5"
                         style={{ padding: '0%' }}>
                        { CheckRole() }
                    </div>
                </div>
            </header>
        </>
    );
}

export default Header;