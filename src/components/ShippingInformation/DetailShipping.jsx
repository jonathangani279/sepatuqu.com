import React, { Fragment, useEffect, useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import TitleBar from '../TitleBar';

const DetailShipping = ({userInfo,setUserInfo}) => {
    const history= useHistory();
    const [ kota, setKota ] = useState([]);
    const [ provinsi, setProvinsi ] = useState([]);
    const [ tampungkota, setTampungKota ] = useState([]);
    
    const [ firstname, setFirstname ] = useState([]);  
    const [ lastname, setLastname ] = useState([]);
    const [ adress, setAdress ] = useState([]);
    const [ telephone, setTelephone ] = useState([]);
    const [ postal, setPostal ] = useState([]);
    const [ pilihkota, setPilihKota ] = useState([]);
    const [ pilihprovinsi, setPilihProvinsi ] = useState([]);
    
    const [totalcart,setTotalCart] = useState();
    const [totalshipment,setTotalShipment] = useState(64000);
    const [totalharga,setTotalHarga] = useState();

    useEffect(function(){
        getState();
    }, []);

    // async function getStruk(){
    //     try {
    //         let fetchapi = await fetch(`/api/users/getStruk/${userInfo.id_user}`,{
    //             method:"GET",
    //         })
    //         let result = await fetchapi.json();
    //         // console.log(result.msg);
           
    //     } catch (error) {
    //         console.log(error);
    //     }
    // }

    async function getState()
    {
        try {
            var fetch_api = await fetch(`/api/users/getKota`,{
                method : "GET",
                headers : {
                    "Content-Type" : "application/json"
                }, 
            });
            var result = await fetch_api.json();
            setKota(result.result);
            setTampungKota(result.result);
            setProvinsi(result.provinsi);

            //console.log(result.result);
        } catch (error) {
            console.log(error);
        }

        try {
            var fetch_api = await fetch(`/api/users/getHarga/${userInfo.id_user}`,{
                method : "GET",
                headers : {
                    "Content-Type" : "application/json"
                }, 
            });
            var result = await fetch_api.json();
            if(result.msg!="")
            {
                setTotalCart(result.msg);
                setTotalHarga(result.msg+64000);
            }
            //console.log(result.result);
        } catch (error) {
            console.log(error);
        }

    }
    
    async function handlePay(e) {
        e.preventDefault();

        //alert(firstname+"-"+lastname+"-"+adress+"-"+pilihprovinsi+"-"+pilihkota+"-"+postal+"-"+telephone);
        if(firstname==""||lastname==""||adress==""||pilihprovinsi==""||pilihkota==""||telephone==""||postal=="")
        {
            alert('Semua field harus diisi');
        }
        else
        {
            try {
                var fetch_api = await fetch(`/api/users/pay`,{
                    method : "POST",
                    headers : {
                        "Content-Type" : "application/json"
                    },
                    body: JSON.stringify({
                        nama_penerima    : firstname+" "+lastname,
                        alamat : adress,
                        kode_pos:postal,
                        no_telp:telephone,
                        id_kota:pilihkota,
                        id_user:userInfo.id_user,
                        saldo : userInfo.saldo
                    }) 
                });
                var result = await fetch_api.json();
                alert(result.msg);
                if(result.msg=="Saldo Tidak Cukup")
                {
                    history.push("/cart");
                }
                else if(result.user=="stok")
                {
                    history.push("/cart");
                }
                else{
                    console.log(result.user[0]);
                    setUserInfo(result.user[0]);
                    history.push("/nota");
                }
                
            } catch (error) {
                console.log(error);
            }
        }
    }

    async function handleChangeState(e){
        setPilihProvinsi(e);
        
        let tampung=[];
        for (let index = 0; index < tampungkota.length; index++) {
            if(e==tampungkota[index].id_provinsi)
            {
                tampung.push({
                    "id_kota" : tampungkota[index].id_kota,
                    "nama_kota" : tampungkota[index].nama_kota
                })
            }
        }
        for (let index = 0; index < provinsi.length; index++) {
            if(e==provinsi[index].id_provinsi){
                setTotalShipment(provinsi[index].harga_shipping);
                setTotalHarga(provinsi[index].harga_shipping+totalcart);
                break;
            }
        }
        setKota(tampung);
    }

    function showDetailHarga(){
        return(
        <>
            <TitleBar title={ 'SHIPPING INFORMATION.' } />
                <div className="container mt-4 mb-5"
                    style={{ fontSize: '1.25vw' }}>
                    <form onSubmit={ handlePay }>
                        <div className="form-row">
                            <div className="form-group col-6">
                                <label>FIRST NAME</label>
                                <input className="form-control"
                                    type="text" 
                                    onChange={ (e) => setFirstname(e.target.value) }
                                    autoFocus />
                            </div>
                            
                            <div className="form-group col-6">
                                <label>LAST NAME</label>
                                <input className="form-control"
                                    type="text"  
                                    onChange={ (e) => setLastname(e.target.value) } />
                            </div>
                        </div>

                        <div className="form-row">
                            <div className="form-group col-12">
                                <label>ADDRESS</label>
                                <input className="form-control" 
                                    type="text" 
                                    onChange={ (e) => setAdress(e.target.value) } />
                            </div>
                        </div>

                        <div className="form-row">
                            <div className="form-group col-6">
                                <label>STATE / PROVINCE</label>
                                <select className="form-control"
                                        onClick={ (e) => handleChangeState(e.target.value) }>
                                    {
                                        provinsi.map((item, index) => {
                                            return(
                                                <Fragment key={ index }>
                                                    <option value={ item.id_provinsi }>
                                                        { item.nama_provinsi }
                                                    </option>
                                                </Fragment>
                                            )
                                        })
                                    }
                                </select>
                            </div>

                            <div className="form-group col-6">
                                <label>CITY</label>
                                <select className="form-control"
                                        onClick={ (e) => setPilihKota(e.target.value) } >
                                    {
                                        kota.map((item, index)=>{
                                            return(
                                                <Fragment key={ index }>
                                                    <option value={ item.id_kota }>
                                                        { item.nama_kota }
                                                    </option>
                                                </Fragment>
                                            )
                                        })
                                    }
                                </select>
                            </div>
                        </div>

                        <div className="form-row">
                            <div className="form-group col-6">
                                <label>TELEPHONE</label>
                                <input className="form-control"
                                    onChange={ (e) => setTelephone(e.target.value) } />
                            </div>

                            <div className="form-group col-6">
                                <label>POSTAL CODE</label>
                                <input className="form-control"
                                    onChange={ (e) => setPostal(e.target.value)} />
                            </div>
                        </div>

                        <div className="pr-3" style={{textAlign:"right"}}>
                        <label><b>BALANCE: </b></label> <b> Rp {parseInt(userInfo.saldo).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b></div>
                        
                        <br/>

                        <div className="pr-3" style={{textAlign:"right"}}>
                            <label><b>TOTAL CART: </b></label> <b>Rp {parseInt(totalcart).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b></div>
                        <div className="pr-3" style={{textAlign:"right"}}>
                            <label><b>SHIPMENT FEE: </b></label> <b>Rp {parseInt(totalshipment).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b>
                        </div>
                        
                        <div className="d-flex">
                            <hr className="w-100" style={{ backgroundColor: 'rgb(1, 1, 50)', height: '0.05vw' }}/>
                            <span className="ml-1" style={{ marginTop: '0.25%' }}>+</span>
                        </div>

                        <div className="pr-3" style={{textAlign:"right"}}>
                            <label><b>GRAND TOTAL:</b> </label> <b> Rp {parseInt(totalharga).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</b> 
                        </div>
                        
                        {userInfo.saldo-totalharga>0?<div style={{textAlign:"right"}}><button className="btn btn-primary mr-3 mt-3">CONTINUE</button></div>:<div style={{textAlign:"right"}}><button className="btn btn-primary mr-3 mt-3" disabled>CONTINUE</button></div>}
                    </form>
                </div>                            
            </>
        )
    }

    return (  
        <>
           { showDetailHarga() }
        </>
    );
}
 
export default DetailShipping;