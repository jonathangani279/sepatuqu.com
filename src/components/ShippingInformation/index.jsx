import React, { useContext } from 'react';
import { Context } from '../UserContext';
import AuthenticatedContent from '../AuthenticatedContent';
import DetailShipping from './DetailShipping';

const ShippingInformation = () => {
    const { userInfo,setUserInfo}= useContext(Context);
    // const history= useHistory();

    // const [ kota, setKota ] = useState([]);
    // const [ provinsi, setProvinsi ] = useState([]);
    // const [ tampungkota, setTampungKota ] = useState([]);
    
    // const [ firstname, setFirstname ] = useState([]);  
    // const [ lastname, setLastname ] = useState([]);
    // const [ adress, setAdress ] = useState([]);
    // const [ telephone, setTelephone ] = useState([]);
    // const [ postal, setPostal ] = useState([]);
    // const [ pilihkota, setPilihKota ] = useState([]);
    // const [ pilihprovinsi, setPilihProvinsi ] = useState([]);
    
    // const [totalcart,setTotalCart] = useState();
    // const [totalshipment,setTotalShipment] = useState(64000);
    // const [totalharga,setTotalHarga] = useState();

    // useEffect(function(){
    //     getState();
    // }, []);

    // // async function getStruk(){
    // //     try {
    // //         let fetchapi = await fetch(`/api/users/getStruk/${userInfo.id_user}`,{
    // //             method:"GET",
    // //         })
    // //         let result = await fetchapi.json();
    // //         // console.log(result.msg);
           
    // //     } catch (error) {
    // //         console.log(error);
    // //     }
    // // }

    // async function getState()
    // {
    //     try {
    //         var fetch_api = await fetch(`/api/users/getKota`,{
    //             method : "GET",
    //             headers : {
    //                 "Content-Type" : "application/json"
    //             }, 
    //         });
    //         var result = await fetch_api.json();
    //         setKota(result.result);
    //         setTampungKota(result.result);
    //         setProvinsi(result.provinsi);

    //         //console.log(result.result);
    //     } catch (error) {
    //         console.log(error);
    //     }

    //     try {
    //         var fetch_api = await fetch(`/api/users/getHarga/${userInfo.id_user}`,{
    //             method : "GET",
    //             headers : {
    //                 "Content-Type" : "application/json"
    //             }, 
    //         });
    //         var result = await fetch_api.json();
    //         if(result.msg!="")
    //         {
    //             setTotalCart(result.msg);
    //             setTotalHarga(result.msg+64000);
    //         }
    //         //console.log(result.result);
    //     } catch (error) {
    //         console.log(error);
    //     }

    // }
    
    // async function handlePay(e) {
    //     e.preventDefault();

    //     //alert(firstname+"-"+lastname+"-"+adress+"-"+pilihprovinsi+"-"+pilihkota+"-"+postal+"-"+telephone);
    //     if(firstname==""||lastname==""||adress==""||pilihprovinsi==""||pilihkota==""||telephone==""||postal=="")
    //     {
    //         alert('Semua field harus diisi');
    //     }
    //     else
    //     {
    //         try {
    //             var fetch_api = await fetch(`/api/users/pay`,{
    //                 method : "POST",
    //                 headers : {
    //                     "Content-Type" : "application/json"
    //                 },
    //                 body: JSON.stringify({
    //                     nama_penerima    : firstname+" "+lastname,
    //                     alamat : adress,
    //                     kode_pos:postal,
    //                     no_telp:telephone,
    //                     id_kota:pilihkota,
    //                     id_user:userInfo.id_user,
    //                     saldo : userInfo.saldo
    //                 }) 
    //             });
    //             var result = await fetch_api.json();
    //             alert(result.msg);
    //             if(result.msg=="Saldo Tidak Cukup")
    //             {
    //                 history.push("/cart");
    //             }
    //             else if(result.user=="stok")
    //             {
    //                 history.push("/cart");
    //             }
    //             else{
    //                 console.log(result.user[0]);
    //                 setUserInfo(result.user[0]);
    //                 history.push("/nota");
    //             }
                
    //         } catch (error) {
    //             console.log(error);
    //         }
    //     }
    // }

    // async function handleChangeState(e){
    //     setPilihProvinsi(e);
        
    //     let tampung=[];
    //     for (let index = 0; index < tampungkota.length; index++) {
    //         if(e==tampungkota[index].id_provinsi)
    //         {
    //             tampung.push({
    //                 "id_kota" : tampungkota[index].id_kota,
    //                 "nama_kota" : tampungkota[index].nama_kota
    //             })
    //         }
    //     }
    //     for (let index = 0; index < provinsi.length; index++) {
    //         if(e==provinsi[index].id_provinsi){
    //             setTotalShipment(provinsi[index].harga_shipping);
    //             setTotalHarga(provinsi[index].harga_shipping+totalcart);
    //             break;
    //         }
    //     }
    //     setKota(tampung);
    // }
   
    return (  
        <AuthenticatedContent>
            {userInfo?<DetailShipping userInfo={userInfo} setUserInfo = {setUserInfo}/>:""}
        </AuthenticatedContent>
    );
}
 
export default ShippingInformation;