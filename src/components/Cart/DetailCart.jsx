import React, { useEffect, useState } from 'react';
import DetailSneaker from '../DetailSneaker';
import PageNavigation from '../PageNavigation';
import TitleBar from '../TitleBar';

const DetailCart = ({ match }) => {
    const [detail, setDetail] = useState();

    useEffect(() => {
        getDetailSneaker();      
    }, []);

    async function getDetailSneaker(){
        try {
            var fetch_api = await fetch(`/api/users/getCart/${match.params.id_cart}`);
            var result = await fetch_api.json();
            console.log(result);
            setDetail(result.result);
          } catch (error) {
              console.log(error);
          }
    }

    function showDetail() {
        return (
            <>
                <TitleBar title={ detail.nama_sneaker.toUpperCase()+'.' } />
                <div className="container mt-5">
                    <PageNavigation pages={ [
                        { name: 'HOME', path: '/' },
                        { name: "CART", path: `/cart` },
                        { name: detail.nama_sneaker.toUpperCase() }
                    ] } />
                    <DetailSneaker detail={ detail } 
                                   from = {"cart"} />
                </div>
            </>
        )
    }

    return ( 
        <>
            { detail ? showDetail() : '' }
        </>
    );
}
 
export default DetailCart;