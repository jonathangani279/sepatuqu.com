import React, { useContext } from 'react';
import AuthenticatedContent from '../AuthenticatedContent';
import { Context } from '../UserContext';
import CartItem from './CartItem';

const Cart = () => {
    const { userInfo }= useContext(Context);
    
    // const [cart,setCart] = useState([]); 
    // const [deleted,setDeleted] = useState(false);
    // const [deletedAll,setDeletedAll] = useState(false);
    // const history = useHistory();
    // useEffect(function(){
    //     getCart();
    // },[deleted?deleted:"",deletedAll?deletedAll:""])

    // async function handleDeleteAll(){
    //     // var pertanyaan = confirm("Hapus semua data?");
    //     // if(pertanyaan==true)
    //     // {
    //     try {
    //         var fetch_api = await fetch("/api/users/deleteCart",{
    //             method : "DELETE",
    //             headers : {
    //                 "Content-Type" : "application/json"
    //             }, 
    //             body: JSON.stringify({
    //                 id_user    : userInfo.id_user,
    //             })
    //         });
    //         var result = await fetch_api.json();
    //         alert(result.msg);
    //         setDeletedAll(false);
    //         setDeletedAll(true);
    //         setCart([]);
    //         } catch (error) {
    //             console.log(error);
    //         }
    //     // }

    // }
    // async function handleDeleteItem(id_sneaker,id_cart)
    // {
    //     try {
    //         var fetch_api = await fetch("/api/users/cart",{
    //             method : "DELETE",
    //             headers : {
    //                 "Content-Type" : "application/json"
    //             }, 
    //             body: JSON.stringify({
    //               id_user    : userInfo.id_user,
    //               id_sneaker : id_sneaker,
    //               id_cart:id_cart
    //             })
    //         });
    //         var result = await fetch_api.json();
    //         alert(result.msg);
    //         setDeleted(false);
    //         setDeleted(true);
    //         setCart(cart.filter(item =>item.id_cart!==id_cart));
    //       } catch (error) {
    //           console.log(error);
    //       }
    // }
    // async function handleEditItem(id_cart)
    // {
    //     history.push("/cart/"+id_cart);
    // }
    // async function getCart(){
    //     try {
    //         var fetch_api = await fetch(`/api/users/cart/${userInfo.id_user}`,{
    //             method : "GET",
    //             headers : {
    //                 "Content-Type" : "application/json"
    //             }, 
    //         });
    //         var result = await fetch_api.json();
    //         setCart(result.result);
    //         console.log(cart);
    //     } catch (error) {
    //         console.log(error);
    //     }
    // } 
    // function showCart()
    // {   return(
    //     <Fragment>
    //             {   
    //                 cart.map((item,index)=>{
    //                     return (
    //                         <Fragment key={index}>
    //                             <table>
    //                                 <tbody>
    //                                 <tr>
    //                                     <th>Id Sneaker</th>
    //                                     <th>Nama Sneaker</th>
    //                                     <th>Gambar Sneaker</th>
    //                                     <th>Ukuran Sneaker</th>
    //                                     <th>Jumlah Sneaker</th>
    //                                     <th>Harga Satuan</th>
    //                                     <th>Subtotal</th>
    //                                 </tr>
                                    
    //                                 <tr>
    //                                     <td>{item.id_sneaker}</td>
    //                                     <td>{item.nama_sneaker}</td>
    //                                     <td> <img className="card-img-top"
    //                                         src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar } style={{width:"100px", height:"200px"}}></img>
    //                                     </td>
    //                                     <td>{item.ukuran_sneaker}</td>
    //                                     <td>{item.jumlah_sneaker}</td>
    //                                     <td>{item.harga_satuan}</td>
    //                                     <td>{item.subtotal}</td>
    //                                     <td><button onClick={()=>handleEditItem(item.id_cart)}>Edit Item</button></td>
    //                                     <td><button onClick={ () => handleDeleteItem(item.id_sneaker,item.id_cart)}>Delete Item</button></td>
    //                                 </tr>
    //                                 </tbody>
    //                             </table>
    //                             {/* <div>
    //                                 {item.id_sneaker}<br></br>
    //                             </div>
    //                             <div>
    //                                 <img className="card-img-top"
    //                                     src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar } style={{width:"100px", height:"200px"}}></img>
    //                             </div>     */}
    //                         </Fragment>     
    //                     )
    //                 })
    //             }
    //             {cart.length!=0 ? <button onClick={ () => handleDeleteAll()} >Delete All</button>:"" }
    //             {/* {cart.length!=0 ? 
    //                 <button onClick={ () => handleCheckOut()}>Check Out</button>
    //                 :"" } */}
    //             {cart.length!=0 ?
    //               <Link style={{ textDecoration: 'none' }}
    //                                           to={ `/shipping`}> <button>Check Out</button>  
    //             </Link> 
    //             :""}

    //         </Fragment>)
    // }

    return (  
        <AuthenticatedContent>
            { userInfo ? <CartItem id_user = { userInfo.id_user } /> : '' }
        </AuthenticatedContent>
    );
}
 

export default Cart;