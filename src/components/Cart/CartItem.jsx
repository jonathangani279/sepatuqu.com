import React, { useEffect, useState } from 'react';
import { Link,useHistory } from "react-router-dom";
import TitleBar from '../TitleBar';

const CartItem = ({id_user}) => {
    const [ cart, setCart ] = useState([]); 
    const history = useHistory();
    const [total,setTotal] = useState("");
    useEffect(function () {
        getCart();
    }, []);
    
    async function getCart(){
        try {
            var fetch_api = await fetch(`/api/users/cart/${id_user}`,{
                method : "GET",
                headers : {
                    "Content-Type" : "application/json"
                }, 
            });
            var result = await fetch_api.json();
            setCart(result.result);
            setTotal(result.total);
            console.log(cart);
        } catch (error) {
            console.log(error);
        }
    } 

    async function handleDeleteAll(){
        // var pertanyaan = confirm("Hapus semua data?");
        // if(pertanyaan==true)
        // {
        
        try {
            var fetch_api = await fetch("/api/users/deleteCart",{
                method : "DELETE",
                headers : {
                    "Content-Type" : "application/json"
                }, 
                body: JSON.stringify({
                    id_user    : id_user,
                })
            });
            var result = await fetch_api.json();
            alert(result.msg);
            setCart([]);
        } catch (error) {
            console.log(error);
        }
        // }
    }

    async function handleDeleteItem(id_sneaker,id_cart)
    {
        var cariharga = cart.find(item=>item.id_cart===id_cart);
        var totalbaru = total - cariharga.subtotal;
        setTotal(totalbaru);
        try {
            var fetch_api = await fetch("/api/users/cart",{
                method : "DELETE",
                headers : {
                    "Content-Type" : "application/json"
                }, 
                body: JSON.stringify({
                  id_user    : id_user,
                  id_sneaker : id_sneaker,
                  id_cart:id_cart
                })
            });
            var result = await fetch_api.json();
            alert(result.msg);

            setCart(cart.filter(item =>item.id_cart!==id_cart));
          } catch (error) {
              console.log(error);
          }
    }

    async function handleEditItem(id_cart)
    {
        history.push("/cart/"+id_cart);
    }

    function showCarts() {
        return (
            <>
                <table className="table-responsive">
                    <thead className="text-center"
                           style={{ 
                                backgroundColor: 'rgb(1, 1, 50)', 
                                color: 'white'
                            }}>
                        <tr>
                            <th className="p-3">#NO.</th>
                            <th></th>
                            <th>SNEAKER NAME</th>
                            <th>SIZE</th>
                            <th>QUANTITY</th>
                            <th>PRICE</th>
                            <th>SUBTOTAL</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody className="text-center">
                        {
                            cart.map((item, index) => {
                                return (
                                    <tr key={ index }>
                                        <td className="p-3">
                                            #{ item.id_sneaker }
                                        </td>
                                        <td className="p-3"> 
                                            <img className="img-fluid img-thumbnail"
                                                 style={{ 
                                                    border: '0.15vw solid rgb(1, 1, 50)', 
                                                    borderRadius: '3%', 
                                                    boxShadow: 'inset 0 1px 1px rgba(1, 1, 50, 0.075), 0 0 8px rgba(1, 1, 50, 0.5)' 
                                                 }}
                                                 width="180vw"
                                                 src={ process.env.PUBLIC_URL+'/images/sneakers/' + item.gambar } />
                                        </td>
                                        <td className="p-3">
                                            { item.nama_sneaker }
                                        </td>
                                        <td className="p-3">
                                            { item.ukuran_sneaker }
                                        </td>
                                        <td className="p-3">
                                            { item.jumlah_sneaker }
                                        </td>
                                        <td className="p-3">
                                            Rp.{ item.harga_satuan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') },-
                                        </td>
                                        <td className="p-3">
                                            Rp.{ item.subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') },-
                                        </td>
                                        <td className="p-3">
                                            <button className="btn btn-primary"
                                                    onClick={ () => handleEditItem(item.id_cart)}>
                                                EDIT
                                            </button>
                                        </td>
                                        <td className="p-3">
                                            <button className="btn btn-primary"
                                                    onClick={ () => handleDeleteItem(item.id_sneaker, item.id_cart)}>
                                                REMOVE
                                            </button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
                <div className="mt-4">
                    <span>
                        <b>GRAND TOTAL : Rp.{total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},- </b>
                    </span>
                    <div className="d-flex mt-2">
                        { cart.length != 0 ? 
                            <button className="btn btn-primary mr-2"
                                    onClick={ () => handleDeleteAll() }>
                                EMPTY CART
                            </button> 
                        : '' }
                        { cart.length != 0 ?
                            <Link style={{ textDecoration: 'none' }}
                                  to={ `/shipping`}> 
                                <button className="btn btn-primary">
                                    CHECK OUT
                                </button>  
                            </Link> 
                        : '' }
                    </div>
                </div>
            </>
        );
    }

    return (
        <>
            <TitleBar title={ 'SHOPPING CART.' } />
            <div className="container mt-5 mb-5"
                 style={{ fontSize: '1.25vw' }}>
                { cart.length ? showCarts() : 'No cart yet.' }
            </div>
        </>
    )
}
 
export default CartItem;