import React, { Fragment, useState, useEffect } from 'react';
import { Link,useHistory } from "react-router-dom";

const DetailNota = ({id_user}) => {
    const [ infoHeader, setInfoHeader ] = useState();
    const [ infoDetails, setInfoDetails ] = useState([]);
    const [ infoShipment, setInfoShipment ] = useState();
    const [ totalsemua,setTotalSemua] = useState();
    const [ totalshipping,setTotalShipping] = useState();
    const [ totaltanpaship,setTotalTanpaShip] = useState();
    

    const history = useHistory();
    useEffect(() => {
        getStruk();
    }, []);

    async function getStruk(){
        try {
            let fetchapi = await fetch(`/api/users/getStruk/${id_user}`,{
                method:"GET",
            })
            let result = await fetchapi.json();
            // console.log(result.msg);
            setInfoHeader(result.msg.headers);
            setInfoDetails(result.msg.details);
            setInfoShipment(result.msg.shipment);

            setTotalSemua(result.msg.totalsemua);
            setTotalShipping(result.msg.totalshipping);
            setTotalTanpaShip(result.msg.totaltanpaship);

            //history.push("/home");
        } catch (error) {
            console.log(error);
        }
    }

    console.log(infoDetails);
    console.log(infoHeader);
    console.log(infoShipment);
    
    function showstruk(){
        return(
            <div className="p-2">
            {/* <table className="p-5" style={{border:"1px solid black",width:"100%"}}> */}
            <h1 style={{textAlign:"center"}}>PURCHASE RECEIPT</h1>
            <div style={{textAlign:"center"}}><img style={{ 
                border: '0.15vw solid rgb(1, 1, 50)', 
                borderRadius: '3%', 
                boxShadow: 'inset 0 1px 1px rgba(1, 1, 50, 0.075), 0 0 8px rgba(1, 1, 50, 0.5)' 
                }}
                width="100vw"
                src={ process.env.PUBLIC_URL+'/icons/SepatuQu2.png' } />
            </div>
            <div style={{textAlign:"center"}}>SNEAKY - Sneaker Online Store</div>
            <div style={{textAlign:"center"}}>Jl. Ngagel Jaya Tengah No.73-77</div>
            <div style={{textAlign:"center"}}>Surabaya, Jawa Timur 60284</div>
            <br/>
            <table style={{width:"100%",border:"1px solid black"}}>
            <thead>
                <td className="pl-5 pt-2">BILL TO</td>
                <td className="pl-5 pt-2">SHIP TO</td>
                <td className="pl-5 pt-2">RECEIPT NUMBER#{infoHeader.id_trans}</td>
                <td className="pl-5 pt-2">SHIPPING TIME ESTIMATED</td>
            </thead>
            <tbody>
                <tr>
                    <td className="pl-5">{infoHeader.username}</td>
                    <td className="pl-5">{infoShipment.nama_penerima}</td>
                    <td className="pl-5">RECEIPT DATE</td>
                    <td className="pl-5">{infoShipment.estimasi_shipping}</td>
                </tr>
                <tr>
                    <td className="pl-5"><em>{infoHeader.email}</em></td>
                    <td className="pl-5">{infoShipment.alamat}</td>
                    <td className="pl-5">{infoHeader.tgl_beli}</td>
                    <td className="pl-5"></td>
                </tr>
                <tr>
                    <td className="pl-5 pb-2"></td>
                    <td className="pl-5 pb-2"> {infoShipment.nama_kota},{infoShipment.nama_provinsi},{infoShipment.kode_pos}</td>
                    <td className="pl-5 pb-2"> </td>
                    <td className="pl-5 pb-2"></td>
                </tr>
            </tbody>
            </table>
            <br/>
            <h3 style={{textAlign:"center"}}>ORDER DETAILS</h3>
            <br/>
            {/* <table style="width:100%;border:1px solid black;padding: 2.5% 2.5% 2.5% 2.5%"> */}
            <table style={{width:"100%",border:"1px solid #010132"}}>
                <thead>
                    {/* <tr style="border-top: 2px solid #3949AB; border-bottom: 2px solid #3949AB;"> */}
                    <tr style={{borderTop:"2px solid #010132",borderBottom:"2px solid #010132"}}>
                        <th className="pl-5">SNEAKER</th>
                        <th className="pl-2">BRAND</th>
                        <th style={{"text-align":"right"}}>SIZE</th>
                        <th style={{"text-align":"right"}}>QUANTITY</th>
                        <th className="text-center" >PRICE</th>
                        <th className="text-right pr-5">TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                {
                infoDetails.map((item,index)=>{
                        return(
                            <tr key={index}>    
                                <td className="pl-5">{item.nama_sneaker}</td>
                                <td className="pl-2">{item.brand_sneaker}</td>
                                <td style={{textAlign:"right",marginRight:"100px"}}>{item.ukuran_sneaker}</td>
                                <td style={{textAlign:"right",marginRight:"100px"}}>{item.jumlah_sneaker} Item</td>
                                <td className="text-center" >Rp.{item.harga_satuan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},- </td>
                                <td className="text-right pr-5" >Rp.{item.subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</td>
                            </tr>
                        )
                    })
                }
                    <tr style={{marginTop:"2%"}}>
                        <td colSpan="4" className="mt-5"></td>
                        <td className="text-right">SUBTOTAL</td>
                        <td className="text-right pr-5" style={{textAlign: "right",paddingLeft:"10px"}}>Rp.{totaltanpaship},-</td>
                    </tr>
                    <tr>
                        <td colSpan="4"></td>
                        <td className="text-right">SHIPPING COST (JNE - REGULER)</td>
                        <td className="text-right pr-5" style={{textAlign: "right"}}>Rp.{totalshipping},-</td>
                    </tr>
                    
                    <tr style={{borderTop:"2px solid #010132",borderBottom:"2px solid #010132"}} >
                        <td colSpan="4"></td>
                        <td className="text-right">GRAND TOTAL</td>
                        <td className="text-right pr-5">Rp.{totalsemua},-</td>
                    </tr>
                
                </tbody>
                

            </table>
            <br/>
            <div style={{width:"100%",textAlign:"right"}}>
                <Link className="ml-auto" style={{ textDecoration: 'none' }}
                        to={ `/cart`}> 
                    <button className="btn btn-primary">
                        FINISH
                    </button>  
                </Link> 
            
            </div>
            {/* <Fragment>
                {
                    infoDetails.map((item,index)=>{
                        return(
                            <Fragment key={index}>
                                <table>
                                    <tbody>
                                        <tr>
                                        <th>Details</th>
                                        </tr>
                                        <tr>
                                            <td>{item.id_trans}</td>
                                            <td>{item.brand_sneaker}</td>
                                            <td>{item.gambar}</td>
                                            <td>{item.harga_satuan}</td>
                                            <td>{item.id_sneaker}</td>
                                            <td>{item.nama_sneaker}</td>
                                            <td>{item.ukuran_sneaker}</td>
                                            <td>{item.jumlah_sneaker}</td>
                                            <td>{item.subtotal}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Fragment>   
                        )
                    })
                } */}
    
                {/* {
                    infoHeader.map((item,index)=>{
                        return(
                            <Fragment key={index}>
                                <table>
                                    <tbody>
                                        <tr>
                                        <th>Headers</th>
                                        </tr>
                                        <tr>
                                            <td>Username{item.id_user}</td>
                                            <td>Nama{item.nama}</td>
                                            <td>Email{item.email}</td>
                                            <td>{item.harga_satuan}</td>
                                            <td>{item.tgl_beli}</td>
                                            <td>Subtotal{item.total}</td>
                                            <td>{item.username}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Fragment>   
                        )
                    })
                } */}
    
                {/* {
                    infoShipment.map((item,index)=>{
                        return(
                            <Fragment key={index}>
                                <table>
                                    <tbody>
                                        <tr>
                                        <th>Shipment</th>
                                        </tr>
                                        <tr>
                                            <td>Alamat{item.alamat}</td>
                                            <td>{item.estimasi_shipping}</td>
                                            <td>Harga Shipping{item.harga_shipping}</td>
                                            <td>{item.nama_kota}</td>
                                            <td>{item.nama_provinsi}</td>
                                            <td>Kode Pos{item.kode_pos}</td>
                                            <td>Penerima{item.nama_penerima}</td>
                                            <td>Nomer Telepon{item.no_telp}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Fragment>   
                        )
                    })
    
                } */}
            {/* </Fragment> */}
            {/* </table> */}
            </div>
        )
    }

    return(
        <div className="p-3">
           {infoHeader&&infoShipment&&infoDetails?showstruk():""}
        </div>
    )


    
}
 
export default DetailNota;