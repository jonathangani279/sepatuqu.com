import React, { useContext } from 'react';
import { Context } from '../UserContext';
import DetailNota from './DetailNota';
import AuthenticatedContent from '../AuthenticatedContent';

const Nota = () => {
    const { userInfo } = useContext(Context);

    // const [infoHeader,setInfoHeader] = useState([]);
    // const [infoDetails,setInfoDetails] = useState([]);
    // const [infoShipment,setInfoShipment] = useState([]);
    // useEffect(()=>{
    //     getStruk();
    // },[])
    // console.log(userInfo);
    // async function getStruk(){
    //     try {
    //         let fetchapi = await fetch(`/api/users/getStruk/${userInfo.id_user}`,{
    //             method:"GET",
    //         })
    //         let result = await fetchapi.json();
    //         // console.log(result.msg);
    //         setInfoHeader(result.msg.headers);
    //         setInfoDetails(result.msg.details);
    //         setInfoShipment(result.msg.shipment);
    //         console.log(result.msg);
    //     } catch (error) {
    //         console.log(error);
    //     }
    // }

    
    // function showStruk()
    // {
    //     console.log(infoDetails);
    //     console.log(infoHeader);
    //     console.log(infoShipment);
    //     return(
    //         <Fragment>
                
    //             {infoDetails.map((item,index)=>{
    //                 return(
    //                     <Fragment key={index}>
    //                         <table>
    //                             <tbody>
    //                                 <tr>
    //                                 <th>Details</th>
    //                                 </tr>
    //                                 <tr>
    //                                     <td>{item.id_trans}</td>
    //                                     <td>{item.brand_sneaker}</td>
    //                                     <td>{item.gambar}</td>
    //                                     <td>{item.harga_satuan}</td>
    //                                     <td>{item.id_sneaker}</td>
    //                                     <td>{item.nama_sneaker}</td>
    //                                     <td>{item.ukuran_sneaker}</td>
    //                                     <td>{item.jumlah_sneaker}</td>
    //                                     <td>{item.subtotal}</td>
    //                                 </tr>
    //                             </tbody>
    //                         </table>
    //                     </Fragment>   
    //                 )
    //             })}

    //             {infoHeader.map((item,index)=>{
    //                 return(
    //                     <Fragment key={index}>
    //                         <table>
    //                             <tbody>
    //                                 <tr>
    //                                 <th>Headers</th>
    //                                 </tr>
    //                                 <tr>
    //                                     <td>Username{item.id_user}</td>
    //                                     <td>Nama{item.nama}</td>
    //                                     <td>Email{item.email}</td>
    //                                     <td>{item.harga_satuan}</td>
    //                                     <td>{item.tgl_beli}</td>
    //                                     <td>Subtotal{item.total}</td>
    //                                     <td>{item.username}</td>
    //                                 </tr>
    //                             </tbody>
    //                         </table>
    //                     </Fragment>   
    //                 )
    //             })}

    //             {infoShipment.map((item,index)=>{
    //                 return(
    //                     <Fragment key={index}>
    //                         <table>
    //                             <tbody>
    //                                 <tr>
    //                                 <th>Shipment</th>
    //                                 </tr>
    //                                 <tr>
    //                                     <td>Alamat{item.alamat}</td>
    //                                     <td>{item.estimasi_shipping}</td>
    //                                     <td>Harga Shipping{item.harga_shipping}</td>
    //                                     <td>{item.nama_kota}</td>
    //                                     <td>{item.nama_provinsi}</td>
    //                                     <td>Kode Pos{item.kode_pos}</td>
    //                                     <td>Penerima{item.nama_penerima}</td>
    //                                     <td>Nomer Telepon{item.no_telp}</td>
                                        
    //                                 </tr>
    //                             </tbody>
    //                         </table>
    //                     </Fragment>   
    //                 )
    //             })}

    //         </Fragment>
    //     )
        
    // }

    return ( 
        <AuthenticatedContent>
            { userInfo ? <DetailNota id_user={ userInfo.id_user } />: '' }
        </AuthenticatedContent>
    );
}
 
export default Nota;