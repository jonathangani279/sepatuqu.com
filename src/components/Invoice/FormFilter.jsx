import React, { useState,useEffect } from 'react';
import useFormInput from "./../../libraries/useFormInput"

const FormFilter = ({ onAddItem }) => {
    const tglFrom = useFormInput("");
    const tglUntil = useFormInput("");
    const sortPrice = useFormInput("asc");

    const [ emailList, setEmailList ] = useState([]);
    const [ emailSelected, setEmailSelected ] = useState("jonathangani279@gmail.com");

    async function getDataEmailFromAPI() {
        try {
          const data = await fetch("/apiLaporan/getEmail", { method: "GET" })
          .then(async response => {
              //alert("success");
              let a = await response.json();
              setEmailList(a[0].email);
          })
          .catch(error => alert('Error! ' + error.message));
        } catch (error) {
            console.error(error);
        }
    }

    useEffect(() => {
        getDataEmailFromAPI();
        //console.log(emailList);
      }, []);

    const handleSubmit = (e) => {
        e.preventDefault();
        onAddItem({ emailFilter : emailSelected});
    }
    const handleChangeEmail = (e) => {
        e.preventDefault();
        setEmailSelected(e.target.value);
    }

    return ( 
        <form onSubmit={handleSubmit}>
            <center>
                Email : &nbsp;
                <select onChange={handleChangeEmail}>
                    {
                        emailList.map(temp => {
                            return (
                                <option value={temp.email}>{temp.email}</option>
                            );
                        })
                    }
                </select>

                &nbsp;&nbsp;
                <button type="submit" style={{backgroundColor:"rgb(180,180,180)", border:"0px solid black", borderRadius:"15px", color:"white", height:"30px"}}>&nbsp; Submit &nbsp;</button>
            </center>
            <br/>
        </form>
    );
}
 
export default FormFilter;