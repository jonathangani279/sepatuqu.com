import React from 'react';

//resource image
import Logo from './img/logo.jpg'

const Footer = ({ postsperPage, totalPosts, paginate }) => {
    const pageNumbers = [];
  
    for (let i = 1; i <= Math.ceil(totalPosts / postsperPage); i++) {
      pageNumbers.push(i);
    }
  
    return (
      <div>
            <center>
                <h5>--THANK YOU FOR PURCHASING OUR ITEMS--</h5>
                <img src={Logo} style={{width:"10%", marginLeft:"auto", marginRight:"auto"}}></img><br/>
                <i>SNEAKY</i> - Sneaker Online Store <br/>
                @2019 <i>SNEAKY</i> <br/>
                All Rights Reserved.
            </center><br/>
      </div>
    );
  };
  
  export default Footer;