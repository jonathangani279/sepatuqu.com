import React, { useState, useEffect } from "react";
import axios from 'axios';

// design tampilan
import Header from './Header'
import Footer from './Footer'

// pagination dan form
import Posts from './Posts'
import Pagination from './Pagination'
import FormFilter from './FormFilter'

//resource image
import Logo from './img/logo.jpg'

const Invoice= () => {
  const [ dataLaporanHeader, setDataLaporanHeader ] = useState([]);
  const [ dataLaporanDetail, setDataLaporanDetail ] = useState([]);

  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsperPage] = useState(1);

  async function getDataLaporanFromAPI() {
    try {
      const data = await fetch("/apiLaporan/getInvoice", { method: "GET" })
      .then(async response => {
          //alert("success");
          let a = await response.json();
          setDataLaporanHeader(a[0].header);
          setDataLaporanDetail(a[0].detail);
      })
      .catch(error => alert('Error! ' + error.message));
    } catch (error) {
        console.error(error);
    }
  }

  async function handleAddItem(obj) {
    try {
      const res = await fetch("/apiLaporan/getInvoiceFilter", { 
        method : "POST",
        headers : { "Content-Type" : "application/json" },
        body : JSON.stringify(obj)
      });
      let a = await res.json();
      setDataLaporanHeader(a[0].header);
      setDataLaporanDetail(a[0].detail);
      //console.log(dataLaporanHeader);
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    const fetchPosts = async () => {
      setLoading(true);
      getDataLaporanFromAPI();
      setLoading(false);
    }
    fetchPosts();
  }, []);

  //GET CURRENT POST
  const indexOfLastPost = currentPage * postsperPage;
  const indexOfFirstPost = indexOfLastPost - postsperPage;
  const currentPosts = dataLaporanHeader.slice(indexOfFirstPost, indexOfLastPost);

  //CHANGING PAGE
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  return (
    <div>
      <br/>
      <FormFilter onAddItem={handleAddItem} />
      <div style={{marginLeft:"auto", marginRight:"auto", width:"1300px", position:"relative", backgroundColor:"white", border:"2px solid black", padding:"20px"}}>
        <img src={Logo} style={{position:"absolute", top:"2%", left:"88%", width:"10%"}}></img>
        <h1>PURCHASE RECEIPT</h1><br/>

        <Header/><br/>
          {/* <OrderDetail/> */}
        <Posts dataLaporanHeader={currentPosts} dataLaporanDetail={dataLaporanDetail} loading={loading}/>
        <Footer/>
        
        <Pagination postsperPage={postsperPage} totalPosts={dataLaporanHeader.length} paginate={paginate} />
        

      </div>
      <br/>
    </div>
    
  );  
}
export default Invoice;