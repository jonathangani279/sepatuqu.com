import React from 'react';

const Header = ({ postsperPage, totalPosts, paginate }) => {
    const pageNumbers = [];
  
    for (let i = 1; i <= Math.ceil(totalPosts / postsperPage); i++) {
      pageNumbers.push(i);
    }
  
    return (
      <div>
            <p>
                <i>SNEAKY</i> - Sneaker Online Store <br/>
                Jl. Ngagel Jaya Tengah No.73-77      <br/>
                Surabaya, Jawa Timur 60284
            </p>
      </div>
    );
  };
  
  export default Header;