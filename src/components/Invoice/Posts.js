import React from "react";

const Posts = ({ dataLaporanHeader, dataLaporanDetail, loading}) => {
    if(loading) {
        return <h2>Loading...</h2>;
    }
    return (
        <div>
        {
          dataLaporanHeader.map((item) => {
            return (
              <div key={item.id_trans}>
                
                {/* HEADER INVOICE */}
                <div style={{float:"left",width:"25%"}}>
                    BILL TO                           <br/>
                    {item.nama_pembeli}               <br/>
                    {item.email_pembeli}              <br/>
                </div>
                <div style={{float:"left",width:"25%"}}>
                    SHIP TO                           <br/>
                    {item.nama_penerima}              <br/>
                    {item.alamat_pengiriman}          <br/>
                    {item.nama_provinsi},{item.nama_kota} {item.kode_pos}
                </div>
                <div style={{float:"left",width:"25%"}}>
                    RECEIPT NUMBER #{item.id_trans}   <br/>
                    RECEIPT DATE                      <br/>
                    {item.tanggal_beli}               <br/>
                </div>
                <div style={{float:"left",width:"25%"}}>
                    SHIPPING TIME ESTIMATED           <br/>
                    {item.estimasi_shipping}
                </div><p style={{clear:"both"}}/><br/><br/> 



                {/* HEADER TABEL */}
                <h3>ORDER DETAILS</h3><br/>

                <hr/>
                <div style={{float:"left",width:"24%"}}> SNEAKERS </div>
                <div style={{float:"left",width:"13%"}}> BRAND </div>
                <div style={{float:"left",width:"10%", textAlign:"right"}}> SIZE </div>
                <div style={{float:"left",width:"13%", textAlign:"right"}}> QUANTITY </div>
                <div style={{float:"left",width:"20%", textAlign:"right"}}> PRICE </div>
                <div style={{float:"left",width:"20%", textAlign:"right"}}> SUBTOTAL </div>
                <p style={{clear:"both"}}/>
                <hr/>



                {/* DETAIL PEMBELIAN */}
                <div style={{float:"left",width:"24%"}}> 
                   {
                      dataLaporanDetail.map(itemDetail => {
                        if(itemDetail.id_trans == item.id_trans)
                          return (
                              <div style={{minHeight:"50px"}}>{itemDetail.nama_item}</div>
                          );
                      })
                    }
                    <div style={{minHeight:"50px"}}/>
                </div>
                <div style={{float:"left",width:"13%"}}> 
                   {
                      dataLaporanDetail.map(itemDetail => {
                        if(itemDetail.id_trans == item.id_trans)
                          return (
                              <div style={{minHeight:"50px"}}>{itemDetail.brand_item}</div>
                          );
                      })
                    }
                    <div style={{minHeight:"50px"}}/>
                </div>
                <div style={{float:"left",width:"10%", textAlign:"right"}}> 
                   {
                      dataLaporanDetail.map(itemDetail => {
                        if(itemDetail.id_trans == item.id_trans)
                          return (
                              <div style={{minHeight:"50px"}}>{itemDetail.ukuran_item}</div>
                          );
                      })
                    }
                    <div style={{minHeight:"50px"}}/>
                </div>
                <div style={{float:"left",width:"13%", textAlign:"right"}}> 
                   {
                      dataLaporanDetail.map(itemDetail => {
                        if(itemDetail.id_trans == item.id_trans)
                          return (
                              <div style={{minHeight:"50px"}}>{itemDetail.jumlah_item}</div>
                          );
                      })
                    }
                    <div style={{minHeight:"50px"}}/>
                </div>
                <div style={{float:"left",width:"20%", textAlign:"right"}}> 
                   {
                      dataLaporanDetail.map(itemDetail => {
                        if(itemDetail.id_trans == item.id_trans)
                          return (
                              <div style={{minHeight:"50px"}}>Rp.{itemDetail.harga_item.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</div>
                          );
                      })
                    }
                    <div style={{minHeight:"50px"}}> SHIPPING COST (JNE - REGULAR) </div>
                </div>
                <div style={{float:"left",width:"20%", textAlign:"right"}}> 
                   {
                      dataLaporanDetail.map(itemDetail => {
                        if(itemDetail.id_trans == item.id_trans)
                          return (
                              <div style={{minHeight:"50px"}}>Rp.{itemDetail.subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</div>
                          );
                      })
                    }
                    <div style={{minHeight:"50px"}}>Rp.{item.harga_shipping.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</div>
                </div>
                <p style={{clear:"both"}}/><hr/>


                {/* GRAND TOTAL */}
                <div style={{float:"left",width:"60%", textAlign:"right", minHeight:"30px"}}/>
                <div style={{float:"left",width:"20%", textAlign:"right", minHeight:"30px"}}>GRAND TOTAL</div>
                <div style={{float:"left",width:"20%", textAlign:"right", minHeight:"30px"}}>Rp.{(parseInt(item.total_harga) + parseInt(item.harga_shipping)).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</div>
                <p style={{clear:"both"}}/><hr/><br/><br/>

                <br/><br/><br/><br/>
              </div>
            )
          })
        }
        </div>
    )
};

export default Posts