import React, { useState, useContext }from 'react';
import Magnifier from "react-magnifier";
import { useHistory } from 'react-router-dom';
import { faShoppingCart, faStar } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Context } from '../UserContext';
import AuthenticatedContent from '../AuthenticatedContent';

const DetailSneaker = ({ detail, from }) => {
    const [ size, setSize ]= useState(detail.ukuran_sneaker ? detail.ukuran_sneaker : 0);
    const [ amount, setAmount ]= useState(detail.jumlah_sneaker ? detail.jumlah_sneaker : 0);
    const { userInfo }= useContext(Context);
    const history= useHistory();
    
    async function handleCart(e,id_cart) {
        e.preventDefault();
        if (from === 'cart') {
            if(detail.id_sneaker==undefined||size==0||amount==0)
            {
                alert("Semua Field harus diisi");
            }
            else{
                try {
                    var fetch_api = await fetch("/api/users/updateCart",{
                        method : "POST",
                        headers : {
                            "Content-Type" : "application/json"
                        }, 
                        body: JSON.stringify({
                          id_user    : userInfo.id_user,
                          id_sneaker : detail.id_sneaker,
                          ukuran_sneaker : size,
                          jumlah_sneaker : amount,
                          harga_sneaker: detail.harga_sneaker,
                          id_cart : id_cart
                        })
                    });
                    var result = await fetch_api.json();
                    alert(result.msg);
                    history.push("/cart");
                  } catch (error) {
                      console.log(error);
                  }
            }
           
        } else {
            if(userInfo)
            {
                if(detail.id_sneaker==undefined||size==0||amount==0)
                {
                    alert("Semua Field harus diisi");
                }
                else if(from==undefined)
                {
                    try {
                        var fetch_api = await fetch("/api/users/cart",{
                            method : "POST",
                            headers : {
                                "Content-Type" : "application/json"
                            }, 
                            body: JSON.stringify({
                              id_user    : userInfo.id_user,
                              id_sneaker : detail.id_sneaker,
                              ukuran_sneaker : size,
                              jumlah_sneaker : amount,
                              harga_sneaker: detail.harga_sneaker
                            })
                        });
                        var result = await fetch_api.json();
                        alert(result.msg);
                      } catch (error) {
                          console.log(error);
                      }
                }
                else
                {
                    try {
                        var fetch_api = await fetch("/api/users/cart?from="+detail.id_wishlist,{
                            method : "POST",
                            headers : {
                                "Content-Type" : "application/json"
                            }, 
                            body: JSON.stringify({
                              id_user    : userInfo.id_user,
                              id_sneaker : detail.id_sneaker,
                              ukuran_sneaker : size,
                              jumlah_sneaker : amount,
                              harga_sneaker: detail.harga_sneaker
                            })
                        });
                        var result = await fetch_api.json();
                        alert(result.msg);
                      } catch (error) {
                          console.log(error);
                      }
                }
                
            }
            else
            {
                history.push('/login');
            }
        }
        
        
         

        // alert(`CART: ${detail.id_sneaker} - ${size} - ${amount}`);
    }

    async function handleWishlist(e,id_wishlist) {
        e.preventDefault();
        if (from === 'wishlist') {
            if(size==0&&amount==0)
            {
                try {
                    var fetch_api = await fetch("/api/users/updatewishlist",{
                        method : "POST",
                        headers : {
                            "Content-Type" : "application/json"
                        }, 
                        body: JSON.stringify({
                          id_user    : userInfo.id_user,
                          id_sneaker : detail.id_sneaker,
                          id_wishlist: id_wishlist
                        })
                    });
                    var result = await fetch_api.json();
                    alert(result.msg);
                  } catch (error) {
                      console.log(error);
                  }
            }
            else if(size!=0&&amount==0)
            {
                try {
                    var fetch_api = await fetch("/api/users/updatewishlist",{
                        method : "POST",
                        headers : {
                            "Content-Type" : "application/json"
                        }, 
                        body: JSON.stringify({
                          id_user    : userInfo.id_user,
                          id_sneaker : detail.id_sneaker,
                          size : size,
                          id_wishlist: id_wishlist
                        })
                    });
                    var result = await fetch_api.json();
                    alert(result.msg);
                  } catch (error) {
                      console.log(error);
                  }
            }
            else if(size==0&&amount!=0)
            {
                try {
                    var fetch_api = await fetch("/api/users/updatewishlist",{
                        method : "POST",
                        headers : {
                            "Content-Type" : "application/json"
                        }, 
                        body: JSON.stringify({
                          id_user    : userInfo.id_user,
                          id_sneaker : detail.id_sneaker,
                          amount : amount,
                          id_wishlist: id_wishlist
                        })
                    });
                    var result = await fetch_api.json();
                    alert(result.msg);
                  } catch (error) {
                      console.log(error);
                  }
            }
            else{
                try {
                    var fetch_api = await fetch("/api/users/updatewishlist",{
                        method : "POST",
                        headers : {
                            "Content-Type" : "application/json"
                        }, 
                        body: JSON.stringify({
                          id_user    : userInfo.id_user,
                          id_sneaker : detail.id_sneaker,
                          size:size,
                          amount : amount,
                          id_wishlist: id_wishlist
                        })
                    });
                    var result = await fetch_api.json();
                    alert(result.msg);
                } catch (error) {
                    console.log(error);
                }
            }
            history.push("/wishlist");
        } else {
            if(size==0&&amount==0)
            {
                try {
                    var fetch_api = await fetch("/api/users/wishlist",{
                        method : "POST",
                        headers : {
                            "Content-Type" : "application/json"
                        }, 
                        body: JSON.stringify({
                          id_user    : userInfo.id_user,
                          id_sneaker : detail.id_sneaker
                        })
                    });
                    var result = await fetch_api.json();
                    alert(result.msg);
                  } catch (error) {
                      console.log(error);
                  }
            }
            else if(size!=0&&amount==0)
            {
                try {
                    var fetch_api = await fetch("/api/users/wishlist",{
                        method : "POST",
                        headers : {
                            "Content-Type" : "application/json"
                        }, 
                        body: JSON.stringify({
                          id_user    : userInfo.id_user,
                          id_sneaker : detail.id_sneaker,
                          size : size
                        })
                    });
                    var result = await fetch_api.json();
                    alert(result.msg);
                  } catch (error) {
                      console.log(error);
                  }
            }
            else if(size==0&&amount!=0)
            {
                try {
                    var fetch_api = await fetch("/api/users/wishlist",{
                        method : "POST",
                        headers : {
                            "Content-Type" : "application/json"
                        }, 
                        body: JSON.stringify({
                          id_user    : userInfo.id_user,
                          id_sneaker : detail.id_sneaker,
                          amount : amount
                        })
                    });
                    var result = await fetch_api.json();
                    alert(result.msg);
                  } catch (error) {
                      console.log(error);
                  }
            }
            else{
                try {
                    var fetch_api = await fetch("/api/users/wishlist",{
                        method : "POST",
                        headers : {
                            "Content-Type" : "application/json"
                        }, 
                        body: JSON.stringify({
                          id_user    : userInfo.id_user,
                          id_sneaker : detail.id_sneaker,
                          size:size,
                          amount : amount
                        })
                    });
                    var result = await fetch_api.json();
                    alert(result.msg);
                } catch (error) {
                    console.log(error);
                }
            }
        }
        
        
        //alert(detail.ukuran_sneaker+"-"+detail.jumlah_sneaker);
        
        

        //alert(`WISHLIST: ${detail.id_sneaker} - ${size} - ${amount}`);
    }

    return (  
        <AuthenticatedContent>
            <div className="row mt-4 mb-5">
                <div className="col-5">
                    <Magnifier className="img-fluid"
                               style={{ 
                                    border: '0.15vw solid rgb(1, 1, 50)', 
                                    borderRadius: '3%', 
                                    boxShadow: 'inset 0 1px 1px rgba(1, 1, 50, 0.075), 0 0 8px rgba(1, 1, 50, 0.5)' 
                               }}
                               src={ process.env.PUBLIC_URL+'/images/sneakers/'+detail.gambar } />
                </div>
                
                <div className="col-5 ml-5"
                     style={{ fontSize: '1.25vw', color: 'rgb(1, 1, 50)' }}>
                    <div className="d-flex justify-content-between mb-3">
                        <span className="font-weight-bold">
                            IDR { detail.harga_sneaker.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') },-
                        </span>
                        <span className="font-weight-bold">
                            STATUS: { detail.status_sneaker === 1 ? 
                            <span style={{ color: 'green' }}>AVAILABLE</span> 
                        : <span style={{ color: 'red' }}>NOT AVAILABLE</span> }
                        </span>
                    </div>
                    
                    <span className="font-weight-bold">
                        BRAND: { detail.brand_sneaker }
                    </span>
                    
                    <form className="mt-3">
                        <div className="font-weight-bold">SIZE & FIT</div>

                        <div className="form-row mt-2">
                            <div className="form-group col-12">
                                {
                                    detail.ukurans.map((item, index) => {
                                        return (
                                            <div className="custom-control custom-radio custom-control-inline"
                                                 key={ index }>
                                                <input className="custom-control-input"
                                                       id={ `custom-radio${ index }` }
                                                       type="radio"
                                                       value={ item.ukuran_sneaker }
                                                       checked={ size === item.ukuran_sneaker } 
                                                       onChange={ () => setSize(item.ukuran_sneaker) } 
                                                       disabled={ detail.status_sneaker === 1 ? false : true } />
                                                <label className="custom-control-label"
                                                       htmlFor={ `custom-radio${ index }` } >
                                                    { item.ukuran_sneaker }
                                                </label>
                                            </div>
                                        )
                                    })
                                }
                            </div>

                            <div className="form-group col-12">
                                <input className="form-control"
                                       type="number"
                                       min="0" 
                                       value = {amount}
                                       autoFocus 
                                       onChange={ (e) => setAmount(e.target.value) } 
                                       disabled={ detail.status_sneaker === 1 ? false : true } />
                            </div>
                            
                            <div className="form-group col-12">
                                <button className="btn btn-primary w-100"
                                        onClick={ (e)=>handleCart(e,detail.id_cart?detail.id_cart:"")}
                                        disabled={ detail.status_sneaker === 1 ? false : true }>
                                    <FontAwesomeIcon icon={ faShoppingCart }
                                                     size={ '1x' } />
                                    &nbsp; { from === 'cart' ? 'UPDATE CART' : 'ADD TO CART'} 
                                </button>
                            </div>
                            <div className="form-group col-12">
                                <button className="btn btn-primary w-100"
                                        onClick={(e)=> handleWishlist(e,detail.id_wishlist?detail.id_wishlist:"") }>
                                    <FontAwesomeIcon icon={ faStar }
                                                     size={ '1x' } />
                                    &nbsp; { from === 'wishlist' ? 'UPDATE WISHLIST' : 'ADD TO WISHLIST'} 
                                </button>
                            </div>
                        </div>
                    </form>
                </div> 
            </div>
        </AuthenticatedContent>
    );
}
 
export default DetailSneaker;