import React, { useState, useEffect,useContext } from 'react';
import { Link,useHistory } from 'react-router-dom';
import { faShoppingCart, faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PageNavigation from '../PageNavigation';
import TitleBar from '../TitleBar';
import { Context } from '../UserContext';

const Catalog = ({ match, keyword, startSearch }) => {
    const [ sneakers, setSneakers ]= useState([]);
    const { userInfo }= useContext(Context);
    const history= useHistory();

    async function getSneaker() {
        let fetchAPI, result;

        if (match) {
            fetchAPI= await fetch(`/api/sneakers/?kategori=${match.params.kategori}`);
            result= await fetchAPI.json();
        }
        
        if (keyword && startSearch && startSearch === 1) {
            fetchAPI= await fetch(`/api/sneakers/?keyword=${keyword}`);
            result= await fetchAPI.json();
        }

        if (result) {
            result.sneakers= result.sneakers.map(item => ({
                id_sneaker: item.id_sneaker,
                nama_sneaker: item.nama_sneaker,
                kategori_sneaker: item.nama_kategori.toLowerCase(),
                gambar_sneaker: item.gambar
            }));

            setSneakers(result.sneakers);
        }
    }

    useEffect(() => {
        getSneaker();
    }, [ match ? match.params.kategori : startSearch ]);
    
    async function handleAddToWishlist(id_sneaker) {
        if(userInfo)
        {
            try {
                var fetch_api = await fetch("/api/users/wishlist",{
                    method : "POST",
                    headers : {
                        "Content-Type" : "application/json"
                    }, 
                    body: JSON.stringify({
                      id_user    : userInfo.id_user,
                      id_sneaker : id_sneaker
                    })
                });
                var result = await fetch_api.json();
                alert(result.msg);
              } catch (error) {
                  console.log(error);
              }
        }
        else{
            history.push('/login');
        }
        
    }

    function showSneakers() {
        return (
            <>
                <div className="row mt-4">
                    {
                        sneakers.map((item, index) => {
                            return (
                                <div className="col-3 mb-4"
                                     key={ index }>
                                    <div className="card">
                                        <div className="card-header text-center"
                                             style={{ backgroundColor: 'rgb(1, 1, 50)' }}>
                                            <h6 className="card-text text-white">
                                                { item.nama_sneaker }
                                            </h6>
                                        </div>
                                        
                                        <Link style={{ textDecoration: 'none' }}
                                              to={ `/catalog/${item.kategori_sneaker}/${item.id_sneaker}`}>  
                                            <img className="card-img-top"
                                                 src={ process.env.PUBLIC_URL+'/images/sneakers/'+item.gambar_sneaker } />
                                        </Link>
                                        
                                        <div className="card-body d-flex justify-content-between">
                                            <Link style={{ textDecoration: 'none' }}
                                                  to={ `/catalog/${item.kategori_sneaker}/${item.id_sneaker}`}>
                                                <button className="btn btn-primary">
                                                    <FontAwesomeIcon icon={ faShoppingCart }
                                                                     size={ '1x' } />
                                                    &nbsp; SHOP NOW
                                                </button>
                                            </Link>
                                            
                                            <button className="btn btn-primary"
                                                    onClick={ () => handleAddToWishlist(item.id_sneaker) }>
                                                <FontAwesomeIcon icon={ faStar }
                                                                 size={ '1x' } />
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </>
        )
    }

    return (  
        <>
            <TitleBar title={ match ? match.params.kategori.toUpperCase()+`'S.` : `SEARCH RESULT'S.` } />
            <div className="container mt-5 mb-5"
                 style={{ fontSize: '1.25vw' }}>
                <PageNavigation pages={ [
                    { name: 'HOME', path: '/' },
                    { name: match ? match.params.kategori.toUpperCase() : 'SEARCH' }
                ] } />
                { sneakers.length ? showSneakers() : 'No sneakers found.' }
            </div>
        </>
    );
}   
 
export default Catalog;