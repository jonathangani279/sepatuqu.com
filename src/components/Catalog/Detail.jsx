import React, { useState, useEffect } from 'react';
import TitleBar from '../TitleBar';
import PageNavigation from '../PageNavigation';
import DetailSneaker from '../DetailSneaker';

const Detail = ({ match }) => {
    const [ detail, setDetail ]= useState();
    
    async function getDetail() {
        const fetchAPI= await fetch(`/api/sneakers/${match.params.id_sneaker}`);
        const result= await fetchAPI.json();

        setDetail(result.detail);
    }
    
    useEffect(() => {
        getDetail();
    }, []);

    function showDetail() {
        return (
            <>
                <TitleBar title={ detail.nama_sneaker.toUpperCase()+'.' } />
                <div className="container mt-5">
                    <PageNavigation pages={ [
                        { name: 'HOME', path: '/' },
                        { name: match.params.kategori.toUpperCase(), path: `/catalog/${match.params.kategori}` },
                        { name: detail.nama_sneaker.toUpperCase() }
                    ] } />
                    <DetailSneaker detail={ detail } />
                </div>
            </>
        )
    }

    return ( 
        <>
            { detail ? showDetail() : '' }
        </>
    );
}

export default Detail;