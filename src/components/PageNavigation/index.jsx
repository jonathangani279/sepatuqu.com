import React from 'react';
import { Link } from 'react-router-dom';

const PageNavigation = ({ pages }) => {

    return (  
        <nav className="font-weight-bold">
            { 
                pages.map((item, index) => {
                    return (
                        <span key={ index }>
                            {
                                index < pages.length - 1 ?
                                    <>
                                        <Link style={{ color: 'rgb(1, 1, 50)' }}
                                              to={ item.path }>
                                            { item.name } 
                                        </Link>&nbsp;>&nbsp;
                                    </>
                                :
                                    <span style={{ color: 'rgb(1, 1, 50)' }}>
                                                { item.name }
                                    </span>
                            }
                        </span>
                    )
                })
            }
        </nav>
    );
}
 
export default PageNavigation;