import React, { useState, useEffect } from 'react';
import axios from 'axios'; 
import { Link } from 'react-router-dom';
import { faEdit } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import Title from './Title';
import { Modal } from 'react-bootstrap';
import useFormInput from '../../libraries/useFormInput';
const InsertMasterBarang = () => {
    const name= useFormInput('');
    const kategori= useFormInput('');
    const brand= useFormInput('');
    const harga= useFormInput('');
    const Status= useFormInput('');
    const [ nameImage, setNameImage ]= useState('');
    const [ Image, setImage ]= useState('');
    
    async function handleSubmit(e) {
        e.preventDefault();
        let fields= {
            name: name.value,
            kategori: kategori.value,
            brand: brand.value,
            harga: harga.value,
            Status: Status.value,
            nameImage:nameImage
        }
        if (!Object.keys(fields).every(key => fields[key] !== '')) {
            return alert('All field required.');
        }
        else{
            const formData = new FormData();
            formData.append('myImage',Image);
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            };
            axios.post("/api/sneakers/upload/",formData,config)
                .then((response) => {
                    alert("The file is successfully uploaded");
                }).catch((error) => {
            });
        }
        try {
            const fetchAPI= await fetch('/api/sneakers/AdminInsertSneaker/', {
                method: 'POST',
                headers: { 'Content-Type' : 'application/json' },
                body: JSON.stringify(fields)
            });
            const res= await fetchAPI.json();
            alert(res.message);
            //window.location.reload();
        } catch (error) {
            return console.error(error);
        }
    }
    function onFileChange(e){ 
        setImage(e.target.files[0]); 
        setNameImage(e.target.files[0].name); 
    }; 
	return (  
		<>	
            <Title content={ `Insert Sneaker` } />
			<div className="container mt-4 mb-5"
                 style={{ fontSize: '1.25vw' }}> 
                <form onSubmit={ handleSubmit }>
                    <div className="form-row">
                        <div className="form-group col-12">
                            Name:
                            <input className="form-control" type="text" { ...name } />
                        </div>
                        <div className="form-group col-12">
                            Category:
                            <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <label className="input-group-text" >Options</label>
                            </div>
                            <select className="custom-select" { ...kategori }>
                                <option selected value="">Choose...</option>
                                <option value="1">Men</option>
                                <option value="2">Women</option>
                                <option value="3">Kids</option>
                            </select>
                            </div>
                        </div>
                        <div className="form-group col-12">
                            Brand:
                            <input className="form-control" type="text" { ...brand } />
                        </div>
                        <div className="form-group col-12">
                            Price:
                            <input className="form-control" type="text" { ...harga } />
                        </div>
                        <div className="form-group col-12">
                            Status:
                            <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <label className="input-group-text">Options</label>
                            </div>
                            <select className="custom-select" { ...Status }>
                                <option selected value="">Choose...</option>
                                <option value="0">Unavalaible</option>
                                <option value="1">Avalaible</option>
                            </select>
                            </div>
                        </div>
                        <div className="form-group col-12">
                            Image:
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">Upload</span>
                                </div>
                                <div className="custom-file">
                                    <input type="file" className="custom-file-input" name="myImage" onChange={onFileChange}></input>
                                    <label className="custom-file-label" >{nameImage}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input className="btn btn-primary pl-5 pr-5" type="submit" value="Insert Sneaker"/>
                </form>
            </div>
		</>
	);
}
 
export default InsertMasterBarang;