import { useHistory } from "react-router";
import useAuth from "../../libraries/UserAuth";

const AuthenticatedContent = ({ children }) => {
	const { user } = useAuth();
	const history= useHistory();

	if (!user) {
		history.push('/login');
	}

	return children;
}
 
export default AuthenticatedContent;