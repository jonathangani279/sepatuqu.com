import React from 'react';
import { useHistory, Link } from 'react-router-dom';
import { faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import useFormInput from '../../libraries/useFormInput';
import useAuth from '../../libraries/UserAuth';
import "./login.css";

const Login = () => {
    const username= useFormInput('');
    const password= useFormInput('');
    
    const { authOperation } = useAuth();
    const history= useHistory();

    async function handleSubmit(e) {
        e.preventDefault();

        let fields= {
            username: username.value,
            password: password.value
        };

        if (!Object.keys(fields).every(key => fields[key] !== '')) {
            return alert('All field required.');
        }

        //LOGIN ADMIN
        if (username.value === 'admin' && password.value === 'admin') {
            authOperation({ name: 'LOGIN', user: {id:"admin"} }); //harus object
            history.push('/HomeMasterBarang/semua/semua');//ini redirect ke index.jsx nya admin
            return window.location.reload();
        }
        
        try {
            const fetchAPI= await fetch('/api/users/login', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    username: username.value,
                    password: password.value
                })
            });
            const res= await fetchAPI.json();

            if (res.status !== 200) {
                return alert(`Login failed.\n${res.message}`);
            }

            alert(res.message);
            authOperation({ name: 'LOGIN', user: res.userLogged });
            history.push('/home');
            window.location.reload();
        } catch (error) {
            console.error(error);
        }
    }

    return (
        <>  
            <div className="container mt-4"
                 style={{ fontSize: '1.25vw' }}>
                <div className="row">
                    <div className="col-6">
                        <h1 className="font-weight-bold">LOG IN.</h1>
                        
                        <form onSubmit={ handleSubmit }>
                            <div class  Name="form-group">
                                <label>USERNAME</label> 
                                <input className="form-control"
                                       type="text" { ...username } 
                                       autoFocus />
                            </div>
                            
                            <div className="form-group mt-2">
                                <label>PASSWORD</label>
                                <input className="form-control"
                                       type="password" { ...password } />
                            </div>

                            <button className="btn btn-primary w-25">LOGIN</button>
                        </form>
                    </div>
                    
                    <div className="col-6">
                        <div className="ml-5">
                            <h1 className="font-weight-bold">JOIN US. GET COMFY.</h1>
                            <ul className="mt-3">
                                <li>Save Multiple Item</li>
                                <li>Faster Checkout</li>
                                <li>Easy Shipping Method</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div className="mt-5"
                style={{ backgroundColor: '#EDE734' }}>
                <div className="container d-flex pt-5 pl-5 pb-5">
                    <h2 className="font-weight-bold">
                        STAY WITH US IN <br/>
                        SepatuQu!
                    </h2>

                    <Link to="/register"
                          style={{ width: '15%', marginLeft: '20%' }}>
                        <button className="btn btn-primary w-75 h-75">
                            SIGN UP &nbsp;
                            <FontAwesomeIcon icon={ faAngleDoubleRight }
                                             size={ '1x' } />
                        </button>
                    </Link>
                </div>
            </div>
        </>
    );
}
 
export default Login;