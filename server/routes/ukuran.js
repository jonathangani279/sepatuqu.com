const express = require("express");
const config = require('./config');
const mysql= require('mysql');
const router= express.Router();
const pool= mysql.createPool(config.database);

const getConn= () => {
    try {   
        return new Promise((resolve, reject) => {
            pool.getConnection((err, conn) => {
                if (err) reject(err);
                else resolve(conn);
            });
        });
    } catch (error) {   
        console.log(error);
    }   
};

const executeQuery= (conn, query) => {
    try {
        return new Promise((resolve, reject) => {
            conn.query(query, (err, res) => {
                if (err) reject(err);
                else resolve(res);
            });
        });
    } catch (error) {   
        console.log(error);
    }
};

router.get('/:id_sneaker', async (req, res) => {
    let conn= await getConn();
    let query= await executeQuery(conn, `
        SELECT *
        FROM ukuran 
        where id_sneaker = ${req.params.id_sneaker}
    `);

    conn.release();

    return res.status(200).json({
        status: 200,
        ukuran: query
    });
});

router.post('/AdminAddSize/', async (req, res) => {
    let id=req.body.id;
    let size=req.body.size;
    let stock=req.body.stock;
    let conn= await getConn();
    let query= await executeQuery(conn, `
        Insert
        INTO ukuran
        Values(
            ${id},
            ${size},
            ${stock},
            ${null},
            ${null}
        )
    `);

    conn.release();

    if (query.affetedRows === 0) {
        return res.status(500).json({
            status: 500,
            message: 'Internal server error. Please try again.'
        });
    }
    else{
        return res.status(200).json({
            status: 200,
            message: 'Insert Ukuran Success.'
        });
    }
});

router.put('/AdminUpdateStock/', async (req, res) => {
    let id=req.body.id;
    let size=req.body.size;
    let stock=req.body.stock;
    let conn= await getConn();
    let query= await executeQuery(conn, `
        UPDATE ukuran
        SET stok_sneaker=${stock}
        WHERE id_sneaker = ${id} 
        AND
        ukuran_sneaker=${size}
    `);

    conn.release();

    if (query.affetedRows === 0) {
        return res.status(500).json({
            status: 500,
            message: 'Internal server error. Please try again.'
        });
    }
    else{
        return res.status(200).json({
            status: 200,
            message: 'update stok Ukuran Success.'
        });
    }
});

module.exports= router;