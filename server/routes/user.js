const express = require("express");
const jwt= require('jwt-simple');
const config = require('./config');
const mysql= require('mysql');
const router= express.Router();
const pool= mysql.createPool(config.database);
var nodemailer = require('nodemailer');

const getConn= () => {
    try {   
        return new Promise((resolve, reject) => {
            pool.getConnection((err, conn) => {
                if (err) reject(err);
                else resolve(conn);
            });
        });
    } catch (error) {   
        console.log(error);
    }   
};

const executeQuery= (conn, query) => {
    try {
        return new Promise((resolve, reject) => {
            conn.query(query, (err, res) => {
                if (err) reject(err);
                else resolve(res);
            });
        });
    } catch (error) {   
        console.log(error);
    }
};

router.post('/register', async (req, res) => {
    const newUser= req.body;

    let conn= await getConn();
    let query= await executeQuery(conn, `
        SELECT *
        FROM user
        WHERE username = '${newUser.username}'
    `);

    if (query.length) {
        return res.status(400).json({
            status: 400,
            message: 'Username already used.'
        });
    }

    conn.release();

    conn= await getConn();
    query= await executeQuery(conn, `
        INSERT INTO user
        VALUES (
            null,
            '${newUser.username}',
            '${newUser.nama}',
            '${newUser.email}',
            '${newUser.password}',
            ${newUser.saldo},
            ${newUser.status_verifikasi},
            null,
            null
        )
    `);
    
    conn.release();

    if (query.affetedRows === 0) {
        return res.status(500).json({
            status: 500,
            message: 'Internal server error. Please try again.'
        });
    }

    return res.status(200).json({
        status: 200,
        message: 'Register success.'
    });
});

router.post('/login', async (req, res) => {
    const loginUser= req.body;

    let conn= await getConn();
    let query= await executeQuery(conn, `
        SELECT *
        FROM user
        WHERE username = '${loginUser.username}' AND
              password = '${loginUser.password}'
    `);

    conn.release();

    if (!query.length) {
        return res.status(400).json({
            status: 400,
            message: 'Wrong username or password.'
        });
    }

    const token= jwt.encode({ id_user: query[0].id_user }, 'user');

    return res.status(200).json({
        status: 200,
        message: 'Login success.',
        userLogged: {
            token: token
        }
    });
});

router.get('/info', async (req, res) => {
    const token= req.header('x-access-token');
    const payload= jwt.decode(token, 'user');

    let conn= await getConn();
    let query= await executeQuery(conn, `
        SELECT *
        FROM user
        WHERE id_user = ${payload.id_user}
    `);

    return res.status(200).json({
        status: 200,
        info: query[0]
    });
});

router.put('/info/:id_user', async (req, res) => {
    const newData= req.body;
    
    let change_name= '',
        change_password= '',
        change_balance= '';

    if (newData.nama) {
        change_name= `nama = '${newData.nama}'`;
    }

    if (newData.new_password) {
        change_password= `, password = '${newData.new_password}'`;
    }

    if (newData.saldo) {
        change_name= `saldo = saldo + ${newData.saldo}`;
    }

    let conn= await getConn();
    let query= await executeQuery(conn, `
        UPDATE user
        SET ${change_name} ${change_password} ${change_balance}
        WHERE id_user = ${req.params.id_user}
    `);

    conn.release();

    if (query.affetedRows === 0) {
        return res.status(500).json({
            status: 500,
            message: 'Internal server error. Please try again.'
        });
    }

    conn= await getConn();
    query= await executeQuery(conn, `
        SELECT *
        FROM user
        WHERE id_user = ${req.params.id_user}
    `);

    conn.release();

    return res.status(200).json({
        status: 200,
        message: 'Save success.',
        editedInfo: query[0]
    });
});

router.post('/addAllWishlist',async function(req,res){
    let conn = await getConn();
    let id_user = req.body.id_user;
    let query = `select * from wishlist where id_user =${id_user}`;
    let hasil = await executeQuery(conn,query);
    conn.release();
    conn = await getConn();
    let ada = false;
    if(hasil.length>0)
    {
        for (let index = 0; index < hasil.length; index++) {
            let querystatus = `select * from sneaker where id_sneaker = ${hasil[index].id_sneaker}`;
            let hasilstatus = await executeQuery(conn,querystatus);
            //console.log(hasilstatus);
            conn.release();
            conn = await getConn();
            if(hasil[index].ukuran_sneaker==0||hasil[index].jumlah_sneaker==0||hasilstatus[0].status_sneaker==0)
            {
                console.log("sini");
                ada = true;
            }
        }

        if(ada==true)
        {
            return res.status(400).json({
                "msg" : "Semua harus ada ukuran"
            })
        }
        else{
            for (let index = 0; index < hasil.length; index++) {
                let query3 = `select harga_sneaker from sneaker where id_sneaker = ${hasil[index].id_sneaker}`;
                let hasil3 = await executeQuery(conn,query3);
                conn.release();
                conn = await getConn();
                var subtotal = parseInt(hasil3[0].harga_sneaker * hasil[index].jumlah_sneaker);
                let query2 = `insert into cart values(null,${id_user},${hasil[index].id_sneaker},
                    ${hasil[index].jumlah_sneaker},${hasil[index].ukuran_sneaker},${hasil3[0].harga_sneaker},${subtotal},null,null)`;
                let hasil2 = await executeQuery(conn,query2);
                conn.release();
                conn = await getConn();
            }
            let query4 = `delete from wishlist where id_user = ${id_user}`;
            let hasil4 = await executeQuery(conn,query4);
            conn.release();
            return res.status(200).json({
                "msg" : "Item berhasil masuk cart"
            })
        }
    }
});

router.post("/updateCommentWishlist",async function(req,res){
    let conn = await getConn();
    let id_sneaker = req.body.id_sneaker;
    let id_user = req.body.id_user;
    let comment = req.body.comment;
    let query = `update wishlist set komentar = "${comment}" where id_sneaker=${id_sneaker} and id_user=${id_user}`;
    let hasil = await executeQuery(conn,query);
    conn.release();
    if(hasil.affectedRows>0)
    {
        return res.status(200).json({
            "msg" : "Update Comment Sukses" 
        })
    }
    else{
        return res.status(200).json({
            "msg" : "Update Comment Gagal" 
        })
    }
});

router.post("/updatewishlist",async function(req,res){
    let conn = await getConn();
    let id_sneaker = req.body.id_sneaker;
    var id_wishlist = req.body.id_wishlist;
    let id_user = req.body.id_user;
    var size = req.body.size;
    var amount = req.body.amount;
    if(size==undefined&&amount==undefined)
    {
        let query2 = `update wishlist set ukuran_sneaker = 0, jumlah_sneaker =0 where id_wishlist =${id_wishlist}`;
        let hasil2 = await executeQuery(conn,query2);
        conn.release();
        if(hasil2.affectedRows>0)
        {
            return res.status(200).json({
                "msg" : "Item Berhasil Diupdate" 
            })
        }
        else{
            return res.status(404).json({
                "msg" : "Item Gagal Diupdate" 
            })
        }
    }
    else if(size!=undefined&&amount==undefined)
    {
        let query2 = `update wishlist set ukuran_sneaker = ${size}, jumlah_sneaker =0 where id_wishlist =${id_wishlist}`;
        let hasil2 = await executeQuery(conn,query2);
        conn.release();
        if(hasil2.affectedRows>0)
        {
            return res.status(200).json({
                "msg" : "Item Berhasil Diupdate" 
            })
        }
        else{
            return res.status(404).json({
                "msg" : "Item Gagal Diupdate" 
            })
        }
    }
    else if(size==undefined&&amount!=undefined)
    {
        let query2 = `update wishlist set ukuran_sneaker = 0, jumlah_sneaker =${amount} where id_wishlist =${id_wishlist}`;
        let hasil2 = await executeQuery(conn,query2);
        conn.release();
        if(hasil2.affectedRows>0)
        {
            return res.status(200).json({
                "msg" : "Item Berhasil Diupdate" 
            })
        }
        else{
            return res.status(404).json({
                "msg" : "Item Gagal Diupdate" 
            })
        }
    }
    else{
        let query2 = `update wishlist set ukuran_sneaker = ${size}, jumlah_sneaker =${amount} where id_wishlist =${id_wishlist}`;
        let hasil2 = await executeQuery(conn,query2);
        conn.release();
        if(hasil2.affectedRows>0)
        {
            return res.status(200).json({
                "msg" : "Item Berhasil Diupdate" 
            })
        }
        else{
            return res.status(404).json({
                "msg" : "Item Gagal Diupdate" 
            })
        }
    }
});

router.post("/wishlist",async function(req,res){
    let conn = await getConn();
    let id_sneaker = req.body.id_sneaker;
    let id_user = req.body.id_user;
    var size = req.body.size;
    var amount = req.body.amount;
    if(size==undefined&&amount==undefined)
    {
        let query2 = `insert into wishlist values(null,${id_user},${id_sneaker},1,0.00,"",null,null)`;
        let hasil2 = await executeQuery(conn,query2);
        conn.release();
        if(hasil2.affectedRows>0)
        {
            return res.status(200).json({
                "msg" : "Item masuk wishlist" 
            })
        }
        else{
            return res.status(404).json({
                "msg" : "Item gagal masuk wishlist" 
            })
        }
    }
    else if(size!=undefined&&amount==undefined)
    {
        let query2 = `insert into wishlist values(null,${id_user},${id_sneaker},1,${size},"",null,null)`;
        let hasil2 = await executeQuery(conn,query2);
        conn.release();
        if(hasil2.affectedRows>0)
        {
            return res.status(200).json({
                "msg" : "Item masuk wishlist" 
            })
        }
        else{
            return res.status(404).json({
                "msg" : "Item gagal masuk wishlist" 
            })
        }
    }
    else if(size==undefined&&amount!=undefined)
    {
        let query2 = `insert into wishlist values(null,${id_user},${id_sneaker},${amount},0.00,"",null,null)`;
        let hasil2 = await executeQuery(conn,query2);
        conn.release();
        if(hasil2.affectedRows>0)
        {
            return res.status(200).json({
                "msg" : "Item masuk wishlist" 
            })
        }
        else{
            return res.status(404).json({
                "msg" : "Item gagal masuk wishlist" 
            })
        }
    }
    else{
        let query2 = `insert into wishlist values(null,${id_user},${id_sneaker},${amount},${size},"",null,null)`;
        let hasil2 = await executeQuery(conn,query2);
        conn.release();
        if(hasil2.affectedRows>0)
        {
            return res.status(200).json({
                "msg" : "Item masuk wishlist" 
            })
        }
        else{
            return res.status(404).json({
                "msg" : "Item gagal masuk wishlist" 
            })
        }
    }
});

router.get("/getWishlist/:id_wishlist",async function(req,res){
    let conn = await getConn();
    var id_wishlist = req.params.id_wishlist;
    let query = `select * from wishlist w,sneaker s where w.id_wishlist = ${id_wishlist} and s.id_sneaker = w.id_sneaker`;
    let hasil = await executeQuery(conn,query);
    conn.release();

    let result = hasil[0];
    console.log(hasil);
    if(hasil.length>0)
    {
        conn = await getConn();
        let query2 = `select * from ukuran where id_sneaker = ${hasil[0].id_sneaker} order by ukuran_sneaker asc`;
        let hasil2 = await executeQuery(conn,query2);
        conn.release();
        result.ukurans = hasil2;
        return res.status(200).json({
            "result" : result
        })
    }
});

router.get("/wishlist/:id_user",async function(req,res){
    let conn = await getConn();
    var id_user = req.params.id_user;
    let query = `select * from wishlist w,sneaker s where w.id_user = ${id_user} and s.id_sneaker = w.id_sneaker`;
    let hasil = await executeQuery(conn,query);
    conn.release();
    let result = [];
    if(hasil.length>0)
    {
        for (let index = 0; index < hasil.length; index++) {
            result.push({
                "id_wishlist" :hasil[index].id_wishlist,
                "id_sneaker" : hasil[index].id_sneaker,
                "nama_sneaker" : hasil[index].nama_sneaker,
                "jumlah_sneaker" : hasil[index].jumlah_sneaker,
                "ukuran_sneaker" : hasil[index].ukuran_sneaker,
                "komentar" : hasil[index].komentar,
                "harga_sneaker" : hasil[index].harga_sneaker,
                "gambar" : hasil[index].gambar
            });
        }
        return res.status(200).json({
            "result" : result
        })
    }
    else
    {

    }
});

router.delete("/deleteItemWishlist",async function(req,res){
    let conn = await getConn();
    var id_user = req.body.id_user;
    var id_sneaker = req.body.id_sneaker;
    var id_wishlist = req.body.id_wishlist;
    let query = `delete from wishlist where id_wishlist = ${id_wishlist}`;
    let hasil = await executeQuery(conn,query);
    conn.release();
    conn = await getConn();
    let query2 = `select count(id_sneaker) as total from wishlist where id_user = ${id_user}`;
    let hasil2 = await executeQuery(conn,query2);
    conn.release();
    console.log(hasil2);
    if(hasil)
    {
        if(hasil2[0].total==0)
        {
            return res.status(200).json({
                "msg" : "Item Berhasil Dihapus." 
            })
        }
        else{
            return res.status(200).json({
                "msg" : "Item Berhasil Dihapus" 
            })
        }

    } 
    else
    {
        return res.status(404).json({
            "msg" : "Item Gagal Dihapus" 
        })
    }
});

router.delete("/deleteWishlist",async function(req,res){
    let conn = await getConn();
    var id_user = req.body.id_user;
    let query = `delete from wishlist where id_user = ${id_user}`;
    let hasil = await executeQuery(conn,query);
    conn.release();
    if(hasil)
    {
        return res.status(200).json({
            "msg" : "Semua Item Berhasil Dihapus" 
        })
    } 
    else
    {
        return res.status(404).json({
            "msg" : "Item Gagal Dihapus" 
        })
    }
});

router.delete("/deletecart",async function(req,res){
    let conn = await getConn();
    var id_user = req.body.id_user;
    let query = `delete from cart where id_user = ${id_user}`;
    let hasil = await executeQuery(conn,query);
    conn.release();
    if(hasil)
    {
        return res.status(200).json({
            "msg" : "Semua Item Berhasil Dihapus" 
        })
    } 
    else
    {
        return res.status(404).json({
            "msg" : "Item Gagal Dihapus" 
        })
    }
});

router.delete("/cart",async function(req,res){
    let conn = await getConn();
    var id_user = req.body.id_user;
    var id_sneaker = req.body.id_sneaker;
    var id_cart = req.body.id_cart; 
    let query = `delete from cart where id_cart = ${id_cart}`;
    let hasil = await executeQuery(conn,query);
    conn.release();
    if(hasil.affectedRows>0)
    {
        return res.status(200).json({
            "msg" : "Item Berhasil Dihapus" 
        })
    } 
    else
    {
        return res.status(404).json({
            "msg" : "Item Gagal Dihapus" 
        })
    }
});

router.get("/cart/:id_user",async function(req,res){
    let conn = await getConn();
    var id_user = req.params.id_user;
    let query = `select * from cart c,sneaker s where c.id_user = ${id_user} and c.id_sneaker = s.id_sneaker`;
    let hasil = await executeQuery(conn,query);
    conn.release();
    let result = [];
    var grandtotal = 0;
    if(hasil.length>0)
    {
        for (let index = 0; index < hasil.length; index++) {
            grandtotal += parseInt(hasil[index].subtotal);
            result.push({
                "id_cart" : hasil[index].id_cart,
                "id_sneaker" : hasil[index].id_sneaker,
                "nama_sneaker" : hasil[index].nama_sneaker,
                "jumlah_sneaker" : hasil[index].jumlah_sneaker,
                "ukuran_sneaker" : hasil[index].ukuran_sneaker,
                "harga_satuan" : hasil[index].harga_satuan,
                "subtotal" : hasil[index].subtotal,
                "gambar" : hasil[index].gambar
            });
        }
        return res.status(200).json({
            "result" : result,
            "total" : grandtotal
        })
    }
    else
    {

    }
});

router.post('/updateCart',async function(req,res){
    let conn = await getConn();
    console.log(req.body);
    var id_sneaker = req.body.id_sneaker;
    var id_user = req.body.id_user;
    var id_cart = req.body.id_cart;
    var jumlah_sneaker = req.body.jumlah_sneaker;
    var ukuran_sneaker = req.body.ukuran_sneaker;
    var harga_satuan = req.body.harga_sneaker;
    var subtotal = parseInt(jumlah_sneaker*harga_satuan);
    let query = `update cart set jumlah_sneaker = ${jumlah_sneaker}, ukuran_sneaker = ${ukuran_sneaker}, subtotal = ${subtotal} where id_cart = ${id_cart}`; 
    let hasil = await executeQuery(conn,query);
    conn.release();
    if(hasil.affectedRows>0)
    {
        res.status(200).json({
            "msg" : "Sukses Update"
        })
    }
});

router.get('/getCart/:id_cart',async function(req,res){
    let conn= await getConn();
    var id_cart = req.params.id_cart;
    let query = `select * from sneaker s,cart c where c.id_cart = ${id_cart} and s.id_sneaker = c.id_sneaker`;
    let hasil = await executeQuery(conn,query);
    conn.release();
    conn = await getConn();
    let result = hasil[0];
    console.log(hasil);
    if(hasil.length>0)
    {
        let query2 = `select * from ukuran where id_sneaker = ${hasil[0].id_sneaker} order by ukuran_sneaker asc`;
        let hasil2 = await executeQuery(conn,query2);
        conn.release();
        result.ukurans = hasil2;
        return res.status(200).json({
            "result" : result
        })
    }
});

router.post('/cart',async function(req,res){
    let conn = await getConn();
    var from = req.query.from;
    var id_sneaker = req.body.id_sneaker;
    var id_user = req.body.id_user;
    var jumlah_sneaker = req.body.jumlah_sneaker;
    var ukuran_sneaker = req.body.ukuran_sneaker;
    var harga_satuan = req.body.harga_sneaker;
    var subtotal = parseInt(jumlah_sneaker*harga_satuan);

    let querysneaker = `select * from ukuran where id_sneaker = ${id_sneaker} and ukuran_sneaker = ${ukuran_sneaker}`;
    let hasilquerysneaker = await executeQuery(conn,querysneaker);
    if(hasilquerysneaker[0].stok_sneaker<jumlah_sneaker)
    {
        return res.status(404).json({
            "msg" : "Stok tidak tersedia" 
        })
    }                
    else{
        conn.release();
        conn = await getConn();
        let querykembar = `select * from cart where id_sneaker =${id_sneaker} and ukuran_sneaker = ${ukuran_sneaker}`;
        let hasilkembar = await executeQuery(conn,querykembar);
        conn.release();
        conn = await getConn();
        if(hasilkembar.length>0)
        {
            
            let totalsneaker = parseInt(hasilkembar[0].jumlah_sneaker) + parseInt(jumlah_sneaker);
            let subtotalsneaker = parseInt(totalsneaker * hasilkembar[0].harga_satuan); 
            try {
                let query2 = `update cart set jumlah_sneaker = ${totalsneaker}, subtotal = ${subtotalsneaker} where id_sneaker = ${id_sneaker} and ukuran_sneaker = ${ukuran_sneaker}`;
                let hasil2 = await executeQuery(conn,query2);                    
            } catch (error) {
                console.log(error);
            }
            conn.release();
            conn = await getConn();
            if(req.query.from)
            {
                let query2 = `delete from wishlist where id_wishlist =${from}`;
                let hasil2 = await executeQuery(conn,query2);
                conn.release();
                if(hasil2.affectedRows>0)
                {
                    return res.status(200).json({
                        "msg" : "Item masuk cart" 
                    })
                }
            }
            else{
                return res.status(200).json({
                    "msg" : "Item masuk cart" 
                })
            }
            
        }
        else{
            let query = `insert into cart values (null,${id_user},${id_sneaker},${jumlah_sneaker},${ukuran_sneaker},${harga_satuan},${subtotal},null,null)`;
            let hasil = await executeQuery(conn,query);
            conn.release();
            conn = await getConn();
            if(hasil.affectedRows>0)
            {
                if(req.query.from)
                {
                    let query2 = `delete from wishlist where id_wishlist =${from}`;
                    let hasil2 = await executeQuery(conn,query2);
                    conn.release();
                    if(hasil2.affectedRows>0)
                    {
                        return res.status(200).json({
                            "msg" : "Item masuk cart" 
                        })
                    }
                }
                else{
                    return res.status(200).json({
                        "msg" : "Item masuk cart" 
                    })
                }
    
            }
            else
            {
                return res.status(404).json({
                    "msg" : "Item gagal masuk cart" 
                })
            }
        }
        
    }
    
})

router.get('/getKota',async function(req,res){
    let conn = await getConn();
    let query = `select * from kota`;
    let hasil = await executeQuery(conn,query);
    //console.log(hasil);
    let result = [];
    if(hasil.length>0)
    {
        for (let index = 0; index < hasil.length; index++) {
            result.push({
                "id_provinsi" : hasil[index].id_provinsi,
                "id_kota" : hasil[index].id_kota,
                "nama_kota" : hasil[index].nama_kota
            });            
        }
        conn.release();
        conn = await getConn();
        let query2 =`select id_provinsi,nama_provinsi,harga_shipping from provinsi`;
        let hasil2 = await executeQuery(conn,query2);
        return res.status(200).json({"result":result,"provinsi":hasil2});
    }
});

router.post("/pay",async function(req,res){
    let conn = await getConn();
    var id_user = req.body.id_user;
    var nama_penerima = req.body.nama_penerima;
    var saldo = req.body.saldo;
    var id_kota= req.body.id_kota;
    var no_telp = req.body.no_telp;
    var alamat = req.body.alamat;
    var kode_pos = req.body.kode_pos;
    let query = `insert into shipment values(null,${id_kota},'${nama_penerima}','${alamat}',${kode_pos},'${no_telp}',null,null)`;
    let hasil = await executeQuery(conn,query);
    if(hasil.affectedRows>0)
    {
        conn.release();
        conn = await getConn();
        let query2 = `select * from cart where id_user =${id_user}`;
        let hasil2 = await executeQuery(conn,query2);
        conn.release();
        conn = await getConn();
        let query3 = `select * from shipment order by 1 desc limit 1`;
        let hasil3 = await executeQuery(conn,query3);
        var tanggalku = new Date();
        //var tanggal = tanggalku.toDateString();
        var tanggal = tanggalku.getFullYear()+"-"+parseInt(tanggalku.getMonth()+1)+"-"+parseInt(tanggalku.getDate()+1);
        console.log(tanggal);

        var total=0;
        var jumlahbarang = 0;
        for (let index = 0; index < hasil2.length; index++) {
            total+= hasil2[index].subtotal;
            jumlahbarang += hasil2[index].jumlah_sneaker;
        }
        conn.release();
        conn = await getConn();
        
        let querycek = `select p.harga_shipping from provinsi p,kota k,shipment s where 
        s.id_shipment =  ${hasil3[0].id_shipment} and
        s.id_kota = k.id_kota 
        and p.id_provinsi = k.id_provinsi `;
        let hasilcek = await executeQuery(conn,querycek);
        console.log("TOTAL SEBELUM "+total);
        total += parseInt(hasilcek[0].harga_shipping);
        console.log(total);
        console.log(saldo);
        conn.release();
        conn = await getConn();
        
        if(saldo>total)
        {
            let booljumlah = false;
            var namasneakerku = "";
            for (let index = 0; index < hasil2.length; index++) {
                let querysneaker = `select * from ukuran where id_sneaker = ${hasil2[index].id_sneaker} and ukuran_sneaker = ${hasil2[index].ukuran_sneaker}`;
                let hasilsneaker = await executeQuery(conn,querysneaker);
                conn.release();
                conn = await getConn();
                if(hasilsneaker[0].stok_sneaker<=hasil2[index].jumlah_sneaker)
                {
                    booljumlah = true;
                    namasneakerku = hasil2[index].id_sneaker;
                    break;
                }            
            }
            if(booljumlah==true)
            {
                let queryhapusshipment = `delete from shipment
                order by id_shipment desc limit 1`;
                let hasilshipment = await executeQuery(conn,queryhapusshipment);
                conn.release();
                conn = await getConn();
                let querynamasneaker = `select nama_sneaker from sneaker where id_sneaker = ${namasneakerku}`;
                let hasilnamasneaker = await executeQuery(conn,querynamasneaker);
                conn.release();
                return res.status(404).json({
                    "msg":"Stok Sneaker "+ hasilnamasneaker[0].nama_sneaker +" Tidak Tersedia",
                    "user":"stok"
                })
            }
            else{
                saldo = parseInt(saldo-total);
            
                conn.release();
                conn = await getConn();
                
                let querysaldo = `update user set saldo =${saldo} where id_user = ${id_user}`;
                let hasilsaldo = await executeQuery(conn,querysaldo);
                conn.release();
                conn = await getConn();
                
                for (let index = 0; index < hasil2.length; index++) {
                    let queryupdatestok = `update ukuran set stok_sneaker = stok_sneaker - ${hasil2[index].jumlah_sneaker} where id_sneaker = ${hasil2[index].id_sneaker} and ukuran_sneaker = ${hasil2[index].ukuran_sneaker}`;
                    let hasilqueryupdatestok = await executeQuery(conn,queryupdatestok); 
                    conn.release();
                    conn = await getConn();
                }
                conn.release();
                conn = await getConn();
                let query4 = `insert into htrans values(null,${id_user},${hasil3[0].id_shipment},'${tanggal}',${jumlahbarang},${total},null,null)`;
                let hasil4 = await executeQuery(conn,query4);
                conn.release();
                conn = await getConn();
                let query6 = `select id_trans from htrans order by 1 desc limit 1`;
                let hasil6 = await executeQuery(conn,query6);
                if(hasil2.length>0)
                {
                    for (let index = 0; index < hasil2.length; index++) {
                        conn.release();
                        conn = await getConn();
                        let query5 = `insert into dtrans values(${hasil6[0].id_trans},${hasil2[index].id_sneaker},
                            ${hasil2[index].jumlah_sneaker},
                            ${hasil2[index].ukuran_sneaker},
                            ${hasil2[index].harga_satuan},
                            ${hasil2[index].subtotal},null,null)`;
                        let hasil5 = await executeQuery(conn,query5);
                    }
                }
                conn.release();
                conn = await getConn();
                let query7 = `delete from cart where id_user = ${id_user}`;
                let hasil7 = await executeQuery(conn,query7);
                conn.release();
                conn = await getConn();
                let query8 = `select * from user where id_user=${id_user}`;
                let hasil8 = await executeQuery(conn,query8);
                return res.status(200).json({
                    "msg":"Struk Berhasil Terkirim ke Email Anda",
                    "user" :hasil8
                })
            }
        }
        else{
            return res.status(404).json({
                "msg":"Saldo Tidak Cukup",
                "user":""
            })
        }
    }
});
router.get("/getHarga/:id_user",async function(req,res){
    let result = {};
    let conn = await getConn();
    let id_user = req.params.id_user;
    var subtotal = 0;
    let query = `select * from cart where id_user = ${id_user}`;
    let hasil = await executeQuery(conn,query);
    if(hasil.length)
    {
        for (let index = 0; index < hasil.length; index++) {
            subtotal+=hasil[index].subtotal;
        }
        return res.status(200).send({
            "msg" : subtotal
        })
    }
    else{
        return res.status(404).send({
            "msg" : ""
        })
    }
    
    // let query2 = `select * from htrans h,user u where h.id_user = ${id_user} and u.id_user =${id_user} order by h.id_trans desc limit 1`;
    // let hasil2 = await executeQuery(conn,query2);
    // //result.headers = hasil2[0];
    // conn.release();
    // conn = await getConn();
   
    // let query = `select * from sneaker s,dtrans d where s.id_sneaker = d.id_sneaker and d.id_trans = ${hasil2[0].id_trans}`;
    // let hasil = await executeQuery(conn,query);
    // conn.release();
    // //result.details = hasil;
    // conn = await getConn();
    // console.log(result);
    
    // let query3 = `select * from provinsi p,shipment s,kota k where s.id_shipment = ${hasil2[0].id_shipment} and s.id_kota = k.id_kota and p.id_provinsi = k.id_provinsi`;
    // let hasil3 = await executeQuery(conn,query3);
    // conn.release();
    // //result.shipment = hasil3[0];

    // var totalsemua = hasil2[0].total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    // var totaltanpaship = parseInt(hasil2[0].total - hasil3[0].harga_shipping).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    // var totalshipping = hasil3[0].harga_shipping.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');

    


})
router.get('/getStruk/:id_user',async function(req,res){
    let result = {};
    let conn = await getConn();
    let id_user = req.params.id_user;
    let query2 = `select * from htrans h,user u where h.id_user = ${id_user} and u.id_user =${id_user} order by h.id_trans desc limit 1`;
    let hasil2 = await executeQuery(conn,query2);

    let date = new Date(hasil2[0].tgl_beli);

    date.setDate(date.getDate()-1);

    hasil2[0].tgl_beli= date.toDateString();

    result.headers = hasil2[0];
    conn.release();
    conn = await getConn();
   
    let query = `select * from sneaker s,dtrans d where s.id_sneaker = d.id_sneaker and d.id_trans = ${hasil2[0].id_trans}`;
    let hasil = await executeQuery(conn,query);
    conn.release();
    result.details = hasil;
    conn = await getConn();
    console.log(result);
    
    let query3 = `select * from provinsi p,shipment s,kota k where s.id_shipment = ${hasil2[0].id_shipment} and s.id_kota = k.id_kota and p.id_provinsi = k.id_provinsi`;
    let hasil3 = await executeQuery(conn,query3);
    conn.release();
    result.shipment = hasil3[0];
    
    var detailbarang = "";
    for (let index = 0; index < hasil.length; index++) {
        detailbarang += `<tr style="border: none;">
        <td style="margin-right:100px;text-align:center">${hasil[index].nama_sneaker}</td>
        <td style="margin-right:100px;text-align:center">${hasil[index].brand_sneaker}</td>
        <td style="text-align: right;margin-right:100px">${hasil[index].ukuran_sneaker}</td>
        <td style="text-align: right;margin-right:100px">${hasil[index].jumlah_sneaker} Item</td>
        <td style="text-align: right;margin-right:100px">Rp.${hasil[index].harga_satuan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},- </td>
        <td style="text-align: right;margin-right:100px">Rp.${hasil[index].subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')},-</td>
    </tr>`
    }
    var totalsemua = hasil2[0].total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    var totaltanpaship = parseInt(hasil2[0].total - hasil3[0].harga_shipping).toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    var totalshipping = hasil3[0].harga_shipping.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');

    result.totalsemua  = totalsemua;
    result.totaltanpaship = totaltanpaship;
    result.totalshipping = totalshipping;


    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'isosepatuqu@gmail.com',
          pass: 'sepatuqu2020'
        }
    });

    var mailOptions = {
        from: 'isosepatuqu@gmail.com',
        to: hasil2[0].email,
        subject: 'Payment Bill Sepatuqu.com',
        html: `
        <div class="container font" style="font-size: 12pt; padding: 20px 0 20px 0;">
        <div class="row">
            <div class="col s12" style="padding: 20px; border: 2px solid black;">
                <div class="row">
                    <table style="width:100%;text-align:center">
                    <div class="col s6">
                        <h1 >PURCHASE RECEIPT</h1>
                    </div>

                    <div class="col s6" style="margin-top: 20px;">
                        <div class="right">
                            <img src="https://img.techpowerup.org/200530/sepatuqu.png" class="img-responsive" width="250">
                        </div>
                    </div>

                    <div class="col s12" style="margin-top: 10px;">
                        <em>SNEAKY</em> - Sneaker Online Store <br>
                        Jl. Ngagel Jaya Tengah No.73-77 <br>
                        Surabaya, Jawa Timur 60284
                    </div>
                    </table>
                    <br>
                    <table style="width:100%;border:1px solid black;padding: 2.5% 2.5% 2.5% 2.5%">
                    <thead>
                        <td>BILL TO</td>
                        <td>SHIP TO</td>
                        <td>RECEIPT NUMBER#${hasil2[0].id_trans}</td>
                        <td>SHIPPING TIME ESTIMATED</td>
                    </thead>
                    <tbody>
                        <tr>
                            <td> ${hasil2[0].nama}</td>
                            <td>${hasil3[0].nama_penerima}</td>
                            <td>RECEIPT DATE</td>
                            <td> ${hasil3[0].estimasi_shipping}</td>
                        </tr>
                        <tr>
                            <td><em>${hasil2[0].email}</em></td>
                            <td>${hasil3[0].alamat}</td>
                            <td>${hasil2[0].tgl_beli}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td> ${hasil3[0].nama_kota},${hasil3[0].nama_provinsi},${hasil3[0].kode_pos}</td>
                            <td> </td>
                            <td></td>
                        </tr>
                    


                    </tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="col s12" style="margin-top: 10px;">
                        <h5 style="text-align:center">ORDER DETAILS</h5>
                        <table style="width:100%;border:1px solid black;padding: 2.5% 2.5% 2.5% 2.5%">
                            <thead>
                                <tr style="border-top: 2px solid #3949AB; border-bottom: 2px solid #3949AB;">
                                    <th>SNEAKER</th>
                                    <th>BRAND</th>
                                    <th style="text-align: right;">SIZE</th>
                                    <th style="text-align: right;">QUANTITY</th>
                                    <th style="text-align: right;">PRICE</th>
                                    <th style="text-align: right;">SUBTOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                ${detailbarang}
                            </tbody>
                            <tfoot>
                                <br><br>
                                <tr style="border: none;">
                                    <td colspan="4"></td>
                                    <td style="text-align: right;">SUBTOTAL</td>
                                    <td style="text-align: right;">Rp.${totaltanpaship},-</td>
                                </tr>
                                <tr style="border: none;">
                                    <td colspan="4"></td>
                                    <td style="text-align: right;">SHIPPING COST (JNE - REGULER)</td>
                                    <td style="text-align: right;">Rp.${totalshipping},-</td>
                                </tr>
                                <br>
                                <tr style="border-top: 2px solid #3949AB; border-bottom: 2px solid #3949AB;">
                                    <td colspan="4"></td>
                                    <td style="text-align: right;">GRAND TOTAL</td>
                                    <td style="text-align: right;">Rp.${totalsemua},-</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <div class="row" style="text-align:center">
                    <div class="col s12" style="margin-top: 100px;">
                        <div class="center">
                            <h5>
                                - - THANK YOU FOR PURCHASING OUR ITEMS - -
                            </h5>
                        </div>
                    </div>

                    <div class="col s12" style="margin-top: 20px;">
                        <div class="center">
                            <img src="https://img.techpowerup.org/200530/sepatuqu.png" class="img-responsive" width="250">
                        </div>
                    </div>

                    <div class="col s12" style="margin-top: 20px;">
                        <div class="center">
                            <h6>
                                <em>SNEAKY</em> - SNEAKER ONLINE STORE
                            </h6>
                            ©2019 <em>SNEAKY</em>. <br>
                            All Rights Reserved.
                        </div>
                    </div>
                </div>
            </div>
        </div>`
        // <html>
        // <div style="font-size: 14pt;">PURCHASE RECEIPT</div>
        //     <div style="float: right;">
        //         <img src="https://i.imgur.com/ewCxgqx.png" width="150">
        //     </div>
        // <div style="clear: both;"></div>
        
        

        // <header>
        //     <div>
        //     <p>
        //         <i>SNEAKY</i> - Sneaker Online Store <br/>
        //         Jl. Ngagel Jaya Tengah No.73-77      <br/>
        //         Surabaya, Jawa Timur 60284
        //     </p>
        //     </div>
        // </header>
        // <body>

        // </body>
        // <footer>
        // <div>
        // <center>
        //     <h5>--THANK YOU FOR PURCHASING OUR ITEMS--</h5>
        //     <img src='https://cdn.discordapp.com/attachments/699999930189611121/716146525348298752/unknown.png' style={{width:"50px",height:"50px" marginLeft:"auto", marginRight:"auto"}}></img><br/>
        //     <i>SNEAKY</i> - Sneaker Online Store <br/>
        //     @2019 <i>SNEAKY</i> <br/>
        //     All Rights Reserved.
        // </center><br/>
        // </div>
        // </footer>
        // </html>
        

    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });

    return res.status(200).json({
        "msg" : result
    })
    

})

router.get('/transactions/:id_user', async (req, res) => {
    let conn= await getConn();
    let query= await executeQuery(conn, `
        SELECT *
        FROM htrans
        WHERE id_user = ${req.params.id_user} 
    `);

    return res.status(200).json({
        status: 200,
        transactions: query
    });
});

router.get('/transactions/:id_trans/:id_shipment', async (req, res) => {
    let conn= await getConn();
    let query= await executeQuery(conn, `
        SELECT *
        FROM dtrans dt, sneaker sn, kategori ka, shipment sh, kota ko
        WHERE dt.id_trans = ${req.params.id_trans} AND
              dt.id_sneaker = sn.id_sneaker AND
              sn.id_kategori = ka.id_kategori AND
              sh.id_shipment = ${req.params.id_shipment} AND
              sh.id_kota = ko.id_kota
    `);

    return res.status(200).json({
        status: 200,
        transaction: {
            id_trans: query[0].id_trans,
            id_shipment: query[0].id_shipment,
            nama_penerima: query[0].nama_penerima,
            alamat: query[0].alamat,
            kode_pos: query[0].kode_pos,
            no_telp: query[0].no_telp,
            nama_kota: query[0].nama_kota,
            sneakers: query.map(item => ({
                nama_sneaker: item.nama_sneaker,
                brand_sneaker: item.brand_sneaker,
                kategori_sneaker: item.nama_kategori,
                gambar_sneaker: item.gambar,
                jumlah_sneaker: item.jumlah_sneaker,
                harga_satuan: item.harga_satuan,
                subtotal: item.subtotal
            }))
        }
    });
});

module.exports= router;