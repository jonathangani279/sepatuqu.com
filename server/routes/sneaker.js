const express = require("express");
const config = require('./config');
const mysql= require('mysql');
const router= express.Router();
const pool= mysql.createPool(config.database);
const multer=require("multer");
const path = require("path");

const storage=multer.diskStorage({
    destination:function(req,file,callback){
        callback(null,"./public/images/sneakers");
    },
    filename:function(req,file,callback) {
        callback(null,file.originalname);
    }
});

const uploads=multer({
    storage:storage
});

const getConn= () => {
    try {   
        return new Promise((resolve, reject) => {
            pool.getConnection((err, conn) => {
                if (err) reject(err);
                else resolve(conn);
            });
        });
    } catch (error) {   
        console.log(error);
    }   
};

const executeQuery= (conn, query) => {
    try {
        return new Promise((resolve, reject) => {
            conn.query(query, (err, res) => {
                if (err) reject(err);
                else resolve(res);
            });
        });
    } catch (error) {   
        console.log(error);
    }
};

router.get('/', async (req, res) => {
    let kategori= 0;
    let keyword= req.query.keyword;

    if (req.query.kategori === 'men') kategori= 1;
    else if (req.query.kategori === 'women') kategori= 2;
    else if (req.query.kategori === 'kid') kategori= 3;
    /*else {
        let conn= await getConn();
        let query= await executeQuery(conn, `SELECT * FROM sneaker`);
        
        conn.release();
        
        return res.status(200).json({
            status: 200,
            sneakers: query
        });
    }*/

    let conn= await getConn();
    let query= await executeQuery(conn, `
        SELECT *
        FROM sneaker s, kategori k
        WHERE s.id_kategori = k.id_kategori
        ${kategori > 0 ? `AND k.id_kategori = ${kategori}` : ''}
        ${keyword ? `AND s.nama_sneaker LIKE '%${keyword}%'` : ''}
    `);

    conn.release();

    return res.status(200).json({
        status: 200,
        sneakers: query
    });
});

router.get('/:id_sneaker', async (req, res) => {
    let conn= await getConn();
    let query= await executeQuery(conn, `
        SELECT *
        FROM sneaker
        WHERE id_sneaker = ${req.params.id_sneaker}
    `);

    conn.release();

    let result= query[0];

    conn= await getConn();
    query= await executeQuery(conn, `
        SELECT *
        FROM ukuran
        WHERE id_sneaker = ${req.params.id_sneaker}
    `);

    conn.release();

    result.ukurans= query;
    
    return res.status(200).json({
        status: 200,
        detail: result
    });
});

//ADMIN
router.get('/AdminGetDetailSneaker/:id_sneaker', async (req, res) => {
    let conn= await getConn();
    let query= await executeQuery(conn, `
        SELECT *
        FROM sneaker
        WHERE id_sneaker = ${req.params.id_sneaker}
    `);

    conn.release();
    return res.status(200).json({
        status: 200,
        detail: query
    });
});

router.put('/AdminGetEditSneaker/', async (req, res) => {
    let id=req.body.id;
    let name=req.body.name;
    let brand=req.body.brand;
    let category=req.body.category;
    let price=req.body.price;
    let status=req.body.status;
    let conn= await getConn();
    let query= await executeQuery(conn, `
        update sneaker 
        set nama_sneaker='${name}',
        brand_sneaker='${brand}',
        id_kategori=${category},
        harga_sneaker=${price},
        status_sneaker='${status}'
        where id_sneaker='${id}'`);

    conn.release();

    if (query.affetedRows === 0) {
        return res.status(500).json({
            status: 500,
            message: 'Internal server error. Please try again.'
        });
    }
    else{
        return res.status(200).json({
            status: 200,
            message: 'Save Edit Sneaker Success.'
        });
    }
});
router.post('/AdminInsertSneaker/', async (req, res) => {

    let name=req.body.name;
    let kategori=req.body.kategori;
    let brand=req.body.brand;
    let harga=req.body.harga;
    let Status=req.body.Status;
    let nameImage=req.body.nameImage;

    let conn= await getConn();
    let query= await executeQuery(conn, `insert into sneaker values(
        ${null},
        ${kategori},
        '${name}',
        '${brand}',
        ${harga},
        ${Status},
        ${null},
        ${null},
        '${nameImage}'
    )`);

    conn.release();

    if (query.affetedRows === 0) {
        return res.status(500).json({
            status: 500,
            message: 'Internal server error. Please try again.'
        });
    }
    else{
        return res.status(200).json({
            status: 200,
            message: 'Insert Sneaker Success.'
        });
    }
});

router.post("/upload",uploads.single("myImage"), function(req,res){});

 router.get('/AdminGetSneaker/:cari/:kategori', async (req, res) => {
    let conn= await getConn();
    let nama=req.params.cari;
    let kategori=req.params.kategori;
    let query="";
    if(nama=="semua"){
        if(kategori=="semua"){
            query= await executeQuery(conn, `
                SELECT *
                FROM sneaker
            `);
        }
        else{
            query= await executeQuery(conn, `
                SELECT *
                FROM sneaker
                WHERE id_kategori = '${kategori}'
            `);
        }
    }
    else{
        if(kategori=="semua"){
            query= await executeQuery(conn, `
                SELECT *
                FROM sneaker
                WHERE nama_sneaker Like '%${nama}%'
            `);
        }
        else{
            query= await executeQuery(conn, `
                SELECT *
                FROM sneaker
                WHERE nama_sneaker Like '%${nama}%'
                AND id_kategori = '${kategori}'
            `);
        }
    }

    conn.release();
    return res.status(200).json({
        status: 200,
        sneakers: query
    });
});
module.exports= router;