const config = { 
    database: {
        host: 'localhost',
        user: 'root',
        password: '',
        port: 3306,
        database: 'sneaky'
    },

    server: {
        host: '127.0.0.1',
        port: 3000
    }
};

module.exports = config;