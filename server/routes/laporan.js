const express = require("express");
const config = require('./config');
const mysql= require('mysql');
const app= express();
const pool= mysql.createPool(config.database);

const router = express.Router();


const getConnection= () => {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, conn) => {
            if (err) reject(err);
            else resolve(conn);
        });
    });
};

const executeQuery= (conn, query) => {
    return new Promise((resolve, reject) => {
        conn.query(query, (err, res) => {
            if (err) reject(err);
            else resolve(res);
        });
    });
};

router.get("/getLaporan", async (req, res) => {
    let conn= await getConnection();
    result = await executeQuery(conn, //UNTUK HEADER LAPORAN
        `SELECT h.id_trans "id_trans", DATE_FORMAT(CAST(h.tgl_beli AS CHAR), '%d %M %Y %H:%i') "tanggal_beli", h.total "total_harga", h.jumlah "jumlah_beli",
        u.nama "nama_pembeli", u.email "email_pembeli", s.nama_penerima "nama_penerima", s.alamat "alamat_pengiriman", s.kode_pos "kode_pos", s.no_telp "no_telp",
        k.nama_kota "nama_kota", p.nama_provinsi "nama_provinsi", p.estimasi_shipping "estimasi_shipping", p.harga_shipping "harga_shipping"
        FROM htrans h, user u, shipment s, kota k, provinsi p
        where h.id_user = u.id_user and h.id_shipment = s.id_shipment and s.id_kota = k.id_kota and k.id_provinsi = p.id_provinsi`
    );
    let result2 = await executeQuery(conn, //UNTUK DETAIL LAPORAN
        `SELECT d.id_trans "id_trans", d.ukuran_sneaker "ukuran_item", d.jumlah_sneaker "jumlah_item", d.harga_satuan "harga_item", d.subtotal "subtotal",
        s.nama_sneaker "nama_item", s.brand_sneaker "brand_item", k.nama_kategori "kategori_item"
        FROM dtrans d, sneaker s, kategori k
        where d.id_sneaker = s.id_sneaker and s.id_kategori = k.id_kategori`
    );

    let kembalian = [
        {header:result,detail:result2}
    ];

    //console.log(result);
    res.json(kembalian);
});
router.get("/getLaporanFilter", async (req, res) => {
    //console.log("sesuatu");
    let tglFrom = "";
    let tglUntil = "";
    //console.log(req.query.tglFrom);
    let temp = req.query.tglFrom.split("-");
    let temp2 = req.query.tglUntil.split("-");
    
    // temp[0] = parseInt(temp[0]+1);
    // console.log("Sebelum" + temp2);
    // temp2[0] = (parseInt(temp2[0])+1);
    
    // if(temp2[0]<10)
    // {
    //     temp2[0] = "0" + temp2[0];
    // }

    // temp2[0] = temp2[0].toString();
    // console.log("Sesudah"+ temp2);

     
    if(req.query.tglFrom != "") tglFrom = new Date (temp[1] + "-" + temp[2] + "-" + temp[0]);
    else tglFrom = new Date ("1900-01-01");
    if(req.query.tglUntil != "") tglUntil = new Date (temp2[1] + "-" + temp2[2] + "-" + temp2[0]);
    else tglUntil = new Date ("2100-01-01");

    // console.log("tglFrom" + tglFrom);
    // console.log("tglUntil" + tglUntil);

    let sortPrice = req.query.sortPrice;
    //console.log(tglFrom + " | " + tglUntil + " | " + sortPrice);

    let conn= await getConnection();
    result = await executeQuery(conn, //UNTUK HEADER LAPORAN
        `SELECT h.id_trans "id_trans", DATE_FORMAT(CAST(h.tgl_beli AS CHAR), '%d %m %Y %H:%i') "tanggal_beli_filter", DATE_FORMAT(CAST(h.tgl_beli AS CHAR), '%d %M %Y %H:%i') "tanggal_beli", h.total "total_harga",h.jumlah "jumlah_beli",
        u.nama "nama_pembeli", u.email "email_pembeli", s.nama_penerima "nama_penerima", s.alamat "alamat_pengiriman", s.kode_pos "kode_pos", s.no_telp "no_telp",
        k.nama_kota "nama_kota", p.nama_provinsi "nama_provinsi", p.estimasi_shipping "estimasi_shipping", p.harga_shipping "harga_shipping"
        FROM htrans h, user u, shipment s, kota k, provinsi p
        where h.id_user = u.id_user and h.id_shipment = s.id_shipment and s.id_kota = k.id_kota and k.id_provinsi = p.id_provinsi
        order by h.total ${sortPrice}`
    );
    //console.log(result);
    let header = [];
    let detail = [];
    
    result.forEach(function(element) {
        //let tanggalTemp = element.tanggal.getDate().toString() + "/" + ((parseInt(element2.tanggal.getMonth()) + 1).toString()).padStart(2,'0') + "/" + element2.tanggal.getFullYear().toString();
        let dataTgl = element.tanggal_beli_filter.split(" ");
        console.log(dataTgl);
        //let tglDB = new Date((parseInt(element.tanggal_beli.getMonth()) + 1).toString() + "-" + element.tanggal_beli.getDate().toString() + "-" + element.tanggal_beli.getFullYear().toString());
        dataTgl[0] = parseInt(dataTgl[0])-1;
        if(dataTgl[0]<10)
        {
            dataTgl[0] = "0" + dataTgl[0];
        }
        dataTgl[0] = dataTgl[0].toString();
        
        console.log("setelah diubah"+dataTgl);
        
        if(dataTgl[0]=="00")
        {
            dataTgl[0] = "01";
        }
        let tglDB = new Date(dataTgl[1] + "-" + dataTgl[0] + "-" + dataTgl[2]);
        
        //console.log(tglFrom + " | " + tglDB + " | " + tglUntil);

        //console.log(tanggalTemp + " - " + a);

        var tglDB1 = (parseInt(tglDB.getMonth())+1) + "/" + tglDB.getDate() + "/" + tglDB.getFullYear();
        var tglfrom1 = (parseInt(tglFrom.getMonth())+1) + "/" + tglFrom.getDate() + "/" + tglFrom.getFullYear();
        var tgluntil1 =(parseInt(tglUntil.getMonth())+1) + "/" + tglUntil.getDate() + "/" + tglUntil.getFullYear();
        

        console.log("tgldb "+tglDB1);
        console.log("tglfrom "+tglfrom1);
        console.log("tgluntil "+tgluntil1);
        
        var d1 = tglfrom1.split("/");
        var d2 = tgluntil1.split("/");
        var c = tglDB1.split("/");

        var from = new Date(d1[2],parseInt(d1[0])-1,  d1[1]);  // -1 because months are from 0 to 11
        var to   = new Date(d2[2],parseInt(d2[0])-1, d2[1]);
        var check = new Date(c[2],parseInt(c[0])-1 , c[1]);

        console.log("ini bug "+ d1[2] + "-" + parseInt(d1[0])-1 + "-" + d1[1]);
        

        console.log("from "+from);
        console.log("to "+to);
        console.log("check "+check);
        

        if(check >= from && check <= to){
            console.log(element);
            header.push(element);
        }
    });

    let result2 = await executeQuery(conn, //UNTUK DETAIL LAPORAN
        `SELECT d.id_trans "id_trans", d.ukuran_sneaker "ukuran_item", d.jumlah_sneaker "jumlah_item", d.harga_satuan "harga_item", d.subtotal "subtotal",
        s.nama_sneaker "nama_item", s.brand_sneaker "brand_item", k.nama_kategori "kategori_item"
        FROM dtrans d, sneaker s, kategori k
        where d.id_sneaker = s.id_sneaker and s.id_kategori = k.id_kategori`
    );
    result2.forEach(function(element1) {
        header.forEach(function(element2) {
            if(element1.id_trans == element2.id_trans) detail.push(element1);
        });
    });
    
    let kembalian = [
        {header:header,detail:detail}
    ];
    //console.log(result);
    res.json(kembalian);
});

router.get("/getMostSold", async (req, res) => {
    let conn= await getConnection();
    result = await executeQuery(conn, //UNTUK HEADER LAPORAN
        `select sum(jumlah_sneaker) "jumlah", s.nama_sneaker "nama" from dtrans d,sneaker s where d.id_sneaker=s.id_sneaker group by d.id_sneaker`
    );

    let jumlah = [];
    let nama = [];
    result.forEach(function(element) {
        jumlah.push(parseInt(element.jumlah));
        nama.push(element.nama);
    });
    let kembalian = [
        {jumlah:jumlah,nama:nama}
    ];
    //console.log(result);
    res.json(kembalian);
});

router.get("/getInvoice", async (req, res) => {
    let conn= await getConnection();
    let result = await executeQuery(conn, //UNTUK HEADER LAPORAN
        `SELECT h.id_trans "id_trans", h.tgl_beli "tanggal_beli", h.total "total_harga",
        u.nama "nama_pembeli", u.email "email_pembeli", s.nama_penerima "nama_penerima", s.alamat "alamat_pengiriman", s.kode_pos "kode_pos", s.no_telp "no_telp",
        k.nama_kota "nama_kota", p.nama_provinsi "nama_provinsi", p.estimasi_shipping "estimasi_shipping", p.harga_shipping "harga_shipping"
        FROM htrans h, user u, shipment s, kota k, provinsi p
        where h.id_user = u.id_user and h.id_shipment = s.id_shipment and s.id_kota = k.id_kota and k.id_provinsi = p.id_provinsi`
    );
    let result2 = await executeQuery(conn, //UNTUK DETAIL LAPORAN
        `SELECT d.id_trans "id_trans", d.ukuran_sneaker "ukuran_item", d.jumlah_sneaker "jumlah_item", d.harga_satuan "harga_item", d.subtotal "subtotal",
        s.nama_sneaker "nama_item", s.brand_sneaker "brand_item", k.nama_kategori "kategori_item"
        FROM dtrans d, sneaker s, kategori k
        where d.id_sneaker = s.id_sneaker and s.id_kategori = k.id_kategori`
    );

    let kembalian = [
        {header:result,detail:result2}
    ];

    //console.log(result);
    res.json(kembalian);
});

router.get("/getEmail", async (req, res) => {
    let conn= await getConnection();
    let result = await executeQuery(conn, //UNTUK HEADER LAPORAN
        `SELECT email "email" from user`
    );

    let kembalian = [
        {email:result}
    ];

    //console.log(result);
    res.json(kembalian);
});

router.post("/getInvoiceFilter", async (req, res) => {
    let temp = req.body.emailFilter;
    let header = [];
    let detail = [];
    //console.log(temp);

    let conn= await getConnection();
    result = await executeQuery(conn, //UNTUK HEADER LAPORAN
        `SELECT h.id_trans "id_trans", h.tgl_beli "tanggal_beli", h.total "total_harga",
        u.nama "nama_pembeli", u.email "email_pembeli", s.nama_penerima "nama_penerima", s.alamat "alamat_pengiriman", s.kode_pos "kode_pos", s.no_telp "no_telp",
        k.nama_kota "nama_kota", p.nama_provinsi "nama_provinsi", p.estimasi_shipping "estimasi_shipping", p.harga_shipping "harga_shipping"
        FROM htrans h, user u, shipment s, kota k, provinsi p
        where h.id_user = u.id_user and h.id_shipment = s.id_shipment and s.id_kota = k.id_kota and k.id_provinsi = p.id_provinsi and u.email='${temp}'`
    );
    
    let result2 = await executeQuery(conn, //UNTUK DETAIL LAPORAN
        `SELECT d.id_trans "id_trans", d.ukuran_sneaker "ukuran_item", d.jumlah_sneaker "jumlah_item", d.harga_satuan "harga_item", d.subtotal "subtotal",
        s.nama_sneaker "nama_item", s.brand_sneaker "brand_item", k.nama_kategori "kategori_item"
        FROM dtrans d, sneaker s, kategori k
        where d.id_sneaker = s.id_sneaker and s.id_kategori = k.id_kategori`
    );
    result2.forEach(function(element1) {
        result.forEach(function(element2) {
            if(element1.id_trans == element2.id_trans) detail.push(element1);
        });
    });
    
    let kembalian = [
        {header:result,detail:detail}
    ];
    //console.log(result);
    res.json(kembalian);
});

module.exports = router;
